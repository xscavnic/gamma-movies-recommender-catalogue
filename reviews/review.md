# Gamma Eval - Movie Recommender Catalogue

### Boring checklist stuff first:

- Accessible gitlab repo - **CHECK**
- Description and diagrams - **CHECK**
- 4+ entity classes - **CHECK**
- DAO interface structure - **CHECK** but see comments below
- JPA DAO Impl - **CHECK**
- Tests with different author - **(35) CHECK**
- PRs - **CHECK**
- Runnable via console + instructions - **CHECK**
- Everyone contributed ~equally - **CHECK**

## Commit Contribution

    29  Jakub Kuchár
    23  Kristian Malak
    21  Šárka Ščavnická
    17  xbock

- Fairly Balanced

## Things to note:

Overall you fulfilled all the requirements set for this milestone but we have some notes below that we would like to see reflected somehow in the next milestone (even just comments why, if you decide not to implement them)

**One admin**

With so many movies being released every week, is one person enough? Maybe if they did not need to add the movies in manually then yes. Take a look at this: https://www.themoviedb.org/ they offer access to their APIs for free with attribution only. Maybe the administrator could just press a button on their "dashboard" and do manual filtering? In any case this could be a good source of data for populating your database.

The [`Admin`](https://gitlab.fi.muni.cz/xscavnic/gamma-movies-recommender-catalogue/-/blob/main/src/main/java/cz/fi/muni/pa165/sem6/gamma/Entity/Admin.java) class looks and feels like a `MovieAdministrationDAO` to us. 

**Naming and consistency**

You have a folder `test` which is standard, then each class is named `*DAOTest` and each method inside the class is annotated with `@Test` and the method name goes like this `*Test` in `GenreDAOTest` and `UserDAOTest` and `test*` in `MovieDAOTest`. The style in `RatingDAOTest` is what I would expect everywhere. Because it is consistent, and not redundant. (If you had/plan to have Software Quality then there they expect this as well.)

```java

    /**
     * Persists passed object to database
     *
     * @param obj - object to be persisted
     */
    void create(Genre obj);

    /**
     * Creates a new movie which has parameter as entity movie.
     *
     * @param movie to be created
     */
    void create(Movie movie);
  
    /**
     * Creates a rating passed from parameters.
     *
     * @param r the rating to be created
     */
    void create(Rating r);

    /**
     *
     * @param p Person to persist in DB
     */
    void create(Person p);

```

The only thing missing to be completely inconsistent is the `create` method name and the `p` parameter of `Person` class. It things like these that set apart "ok projects" and "excellent projects".

Java packages should be lowercase, so no `DAO` and `Entity` [Source](https://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html).

**Plaintext passwords**

By looking at the `findAdmin()` you are using `getResultList()` while querying the DB so maybe there was initially a plan to have multiple Admins, why did you settle with one?\
By the name `findAdmin()` I would expect a pure function that does not modify the database. If you want to keep it this way it should be noted somewhere that the call can actually create one if it does not find one.\
But the point of this section is plaintext passwords. You are hard-coding "nbuSR123" as the default password in production code. In M3 we have a security requirement that passwords are not stored in plaintext in the database. (Which should be the standard in any serious application). Which means this method will have to be reworked in M3 at the latest.

**DAO Structure - 0.5** 

You have DAO interface and DAO implementations good.
Why is `Admin` class holding a list of `Genre`? Genres are tied to movies so should be handled by movies directly.\
Why does `Person` contain functions such as `rateMovie()` and `removeRating()` these are supposed to be DAO interface/impl. (They are not being used right now so I do not see the intent/cannot tell you what to do with them), but entities are supposed to map to Database entities (hence the name) and in a well designed project should not change themselves. \
Why is the abstract `User` holding a list of `Movie`s and why is the list `static`? (Did they produce/like/see them? Which one is it. (None of them really work since it is `static`...)) 


**Database choice**

This is beyond the scope of the project but using a relational database for recommender systems is not good, because the recommendation is computed from various attributes of the movie and preferences of the user you have to fetch everything for every recommendation (or do some black magic caching). For this purpose Graph databases are ideal as the "recommendation process" happens in the database. I can recommend Neo4J https://neo4j.com/. You can probably see why and how this is helpful.

**Tests - 0.5**

The CRUD operations are tested, dao coverage is 96% with the only method `getRecommendedMovies()` untested (that being the core method of the entire project but not being CRUD I have no problem with that for now), the entity coverage is "only" 61%  as you are using the `.equals()` in tests which verifies only a minimum of properties that should uniquely identify the entity in the system. (Business equivalence is done properly).\
The test for the `static List<Movie> movies` is missing (if you had it then maybe you would notice that it should not be static, in any case we cannot do much about it since the context is unknown...)\
There are some `//TODO add check` comments, mainly in the `Admin` add/update movie we are not sure what you want to check, the existence of the movie perhaps?\
The same goes for `editGenre(genre, edited)` that is not tested as well.

**Post peer review**

There are a lot of interesting observations about the `equals/hashCode`.\
It is difficult to say if they are right as we do not know all the business details that you are planing to implement. I would like to see more tests that cover the `Admin`/`Person` distinction (especially in `hashCode`). So that you can also verify it behaves as expected. As `Admin` and `Person` use the same hash based on login only...

A note about *Test dependencies are an excellent addition.* from one of the reviews. Unit tests should be as independent as possible (hence the name). For example looking at `UserDAOTest`::`removeTest()` I do not see why it needs to run `updateTest()` first. I think I see what the intent was, but, running `GenreDAOTest`::`findAllTest()` while `findByIdTest` fails, the `findAllTest()` is actually skipped even though it would be successful (as the Ids are not checked in the test). 

## With all that:

## *4

