package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

@Controller
public class HomeController {

	private final HttpClient httpClient = HttpClientBuilder.create().build();

	@GetMapping(value = "/")
	public String getLogin(Model model, HttpSession session, UriComponentsBuilder uriBuilder, HttpSession httpSession) {
		model.addAttribute("user", new UserAuthenticateDTO());
		if (session.getAttribute("login") != null && session.getAttribute("hash") != null && session.getAttribute("timestamp") != null) {
			JSONObject request = new JSONObject();
			request.put("login", session.getAttribute("login").toString());
			request.put("hash", session.getAttribute("hash").toString());
			request.put("timestamp", Integer.parseInt(session.getAttribute("timestamp").toString()));
			HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/validate_hash");
			httpPost.addHeader("content-type", "application/json");
			JSONObject response;
			try {
				httpPost.setEntity(new StringEntity(request.toString()));
				HttpResponse result = httpClient.execute(httpPost);
				response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
			} catch (IOException e) {
				return "redirect:" + uriBuilder.path("/security/login").encode().toUriString();
			}
			if (Objects.equals(response.getString("Status"), "OK")) {
				return "home";
			}
		}
		Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
		model.addAttribute("isAdmin", isAdmin);
		return "redirect:" + uriBuilder.path("/security/login").encode().toUriString();
	}
}
