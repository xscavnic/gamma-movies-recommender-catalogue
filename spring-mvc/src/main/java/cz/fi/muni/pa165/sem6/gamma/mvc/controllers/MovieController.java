package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserDTO;
import cz.fi.muni.pa165.sem6.gamma.sampledata.SampleDataLoadingImpl;
import cz.fi.muni.pa165.sem6.gamma.service.GenreService;
import cz.fi.muni.pa165.sem6.gamma.service.MovieService;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("/movie")
public class MovieController {

    private final RestTemplate restTemplate = new RestTemplate();

    final static Logger log = LoggerFactory.getLogger(GenreController.class);

    @Autowired
    private MovieService movieService;

    @Autowired
    private GenreService genreService;

    /**
     * Shows a list of movies
     *
     * @param model data to display
     * @return JSP page name
     */
    @GetMapping(value = "/list")
    public String list(Model model,HttpSession httpSession) {

        model.addAttribute("movies", movieService.findAll());
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "movie/list";
    }

    /**
     * Shows a movies detail by the corresponding id
     *
     * @param id    id of showing movie
     * @param model data to display
     * @return JSP page name
     */
    @GetMapping(value = "/detail/{id}")
    public String getMovieById(@PathVariable("id") Long id, Model model,
                               UriComponentsBuilder uriBuilder,
                               RedirectAttributes redirectAttributes, HttpSession httpSession
                               ) {

        try {
            model.addAttribute("movie", movieService.findById(id));
        } catch (Exception ex) {
            log.error("Exception={}", ex.getCause(), ex);
            redirectAttributes.addFlashAttribute("alert_danger", "Invalid id passed.");
            return "redirect:" + uriBuilder.path("/movie/list").encode().toUriString();
        }

        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "movie/detail";
    }

    /**
     * Created form for creating a movie
     *
     * @param model data to display
     * @return JSP page name
     */
    @GetMapping(value = "/new")
    public String newMovieForm(Model model, HttpSession httpSession) {
        MovieCreateDTO movieCreateDto = new MovieCreateDTO();

        model.addAttribute("movieCreateDto", movieCreateDto);
        model.addAttribute("allGenres", genreService.findAll());

        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "movie/new";
    }

    @PostMapping("/create")
//@RequestParam(value = "file") MultipartFile image,
    public String create(
                        @RequestParam(value = "movieGenres", required = false) String[] genreDtoSet,
                         @RequestParam(value = "actorsString", required = false) String actorsString,
                         @Valid @ModelAttribute("movieCreateDto") MovieCreateDTO formBean,
                         BindingResult bindingResult,
                         Model model, RedirectAttributes redirectAttributes,
                         UriComponentsBuilder uriComponentsBuilder) throws IOException {
        log.debug("create(formBean={})", formBean);
/*
       if (image != null && !image.isEmpty()) {
            formBean.setImage(image.getBytes());
        } else {
            redirectAttributes.addFlashAttribute("alert_danger", "Image not uploaded");
            return "redirect:" + uriComponentsBuilder.path("/movie/new").encode().toUriString();
        }
*/
        if (genreDtoSet != null && genreDtoSet.length > 0) {
            Set<GenreDTO> genres = new HashSet<>();
            for (String genreId : genreDtoSet) {
                genres.add(genreService.findById(Long.valueOf(genreId)));
            }

            if (!genres.isEmpty()) {
                formBean.setGenres(genres);
            }
        }

        if (!actorsString.isEmpty()) {
            List<String> actors = Arrays.asList(actorsString.split(", "));
            formBean.setActors(actors);
        }

        try {
            movieService.create(formBean);
            redirectAttributes.addFlashAttribute("alert_success", "Movie has been created");
        } catch (Exception ex) {
            log.error("Exception={}", ex.getCause(), ex);
            redirectAttributes.addFlashAttribute("alert_danger", "Movie create failed.");
        }
        return "redirect:" + uriComponentsBuilder.path("/movie/list").encode().toUriString();
    }

    /**
     * Returns model -- movie list but deletes movie with given id.
     *
     * @param id                 the id of movie to be deleted
     * @param model              to which is updated movieList
     * @param redirectAttributes to redirect in case of alert success
     * @return model -- movie list but deletes movie with given id
     */
    @GetMapping(value = "/delete/{id}")
    public String deleteById(@PathVariable("id") Long id,
                             Model model,
                             RedirectAttributes redirectAttributes,
                             UriComponentsBuilder uriBuilder,
                             HttpSession httpSession) {

        try {
            movieService.remove(movieService.findById(id));
            redirectAttributes.addFlashAttribute("alert_success", "Movie was removed");
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute("alert_danger", "Movie remove didn't work, " +
                    "probably Movie is still used in some genre or review." + ex.toString());
        }

        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "redirect:" + uriBuilder.path("/movie/list").encode().toUriString();
    }

    /**
     * Returns model -- edit with filled movieDto.
     *
     * @param id    of the movie to be updated
     * @param model to which is filled movieDto
     * @return model -- edit with filled movieDto
     */
    @GetMapping(value = "/edit/{id}")
    public String getEditMovieDtoView(@PathVariable("id") Long id,
                                      Model model,
                                      RedirectAttributes redirectAttributes,
                                      UriComponentsBuilder uriBuilder,
                                      HttpSession httpSession) {
        MovieDTO movieDto;
        try {
            movieDto = movieService.findById(id);
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute("alert_danger", "Movie id not found");
            return "redirect:" + uriBuilder.path("/movie/list").encode().toUriString();
        }

        String actorsString;
        if (movieDto.getActors() != null && movieDto.getActors().size() > 0) {
            actorsString = String.join(", ", movieDto.getActors());
        } else {
            actorsString = "";
        }

        model.addAttribute("movieDto", movieDto);
        model.addAttribute("actorsString", actorsString);
        model.addAttribute("allGenres", genreService.findAll());
        model.addAttribute("selectedGenres", movieDto.getGenres());
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "movie/edit";
    }

    @RequestMapping("/movieImage/{id}")
    public void getMovieImage(@PathVariable long id, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        MovieDTO movieDto = movieService.findById(id);
        byte[] image = movieDto.getImage(0);
        if (image == null) {
            response.sendRedirect(request.getContextPath() + "/no-image.png");
        } else {
            response.setContentType("image/jpeg");
            ServletOutputStream out = response.getOutputStream();
            out.write(image);
            out.flush();
        }
    }


    @PostMapping(value = "/edit")
    public String create(@RequestParam(value = "movieGenres", required = false) String[] genreDtoSet,
                         @RequestParam(value = "actorsString", required = false) String actorsString,
                         @Valid @ModelAttribute("movieDto") MovieDTO formBean,
                         BindingResult bindingResult,
                         Model model, RedirectAttributes redirectAttributes,
                         UriComponentsBuilder uriComponentsBuilder)  {
        log.debug("create(formBean={})", formBean);

        if (genreDtoSet != null && genreDtoSet.length > 0) {
            Set<GenreDTO> genres = new HashSet<>();
            for (String genreId : genreDtoSet) {
                genres.add(genreService.findById(Long.valueOf(genreId)));
            }

            if (!genres.isEmpty()) {
                formBean.setGenres(genres);
            }
        }

        if (!actorsString.isEmpty()) {
            List<String> actors = Arrays.asList(actorsString.split(", "));
            formBean.setActors(actors);
        }

        try {
            movieService.update(formBean);
            redirectAttributes.addFlashAttribute("alert_success", "Movie has been created");
        } catch (Exception ex) {
            log.error("Exception={}", ex.getCause(), ex);
            redirectAttributes.addFlashAttribute("alert_danger", "Movie create failed.");
        }
        return "redirect:" + uriComponentsBuilder.path("/movie/list").encode().toUriString();
    }

}
