package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Objects;


@Controller
@RequestMapping("/security")
public class LoginController {

	private final HttpClient httpClient = HttpClientBuilder.create().build();

	@GetMapping(value = "login")
	public String getLogin(Model model, HttpSession session) {
		model.addAttribute("user", new UserAuthenticateDTO());
		if (session.getAttribute("login") != null && session.getAttribute("hash") != null && session.getAttribute("timestamp") != null) {
			JSONObject request = new JSONObject();
			request.put("login", session.getAttribute("login").toString());
			request.put("hash", session.getAttribute("hash").toString());
			request.put("timestamp", Integer.parseInt(session.getAttribute("timestamp").toString()));
			HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/validate_hash");
			httpPost.addHeader("content-type", "application/json");
			JSONObject response;
			try {
				httpPost.setEntity(new StringEntity(request.toString()));
				HttpResponse result = httpClient.execute(httpPost);
				response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
			} catch (IOException e) {
				return "security/login";
			}
			if (Objects.equals(response.getString("Status"), "OK")) {
				return "redirect:/";
			}
		}
		return "security/login";
	}

	@PostMapping(value = "login")
	public String postLogin(Model model, HttpSession session, UriComponentsBuilder uriBuilder, @Valid @ModelAttribute("user") UserAuthenticateDTO userAuthenticateDTO,
							BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		JSONObject request = new JSONObject();
		request.put("login", userAuthenticateDTO.getLogin());
		request.put("password", userAuthenticateDTO.getPassword());
		HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/person_login");
		httpPost.addHeader("content-type", "application/json");
		JSONObject response;
		try {
			httpPost.setEntity(new StringEntity(request.toString()));
			HttpResponse result = httpClient.execute(httpPost);
			response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
		} catch (IOException e) {
			redirectAttributes.addFlashAttribute("alert_danger", "Unexpected error occurred while trying to login");
			return "security/login";
		}
		if (Objects.equals(response.getString("Status"), "ERROR")) {
			redirectAttributes.addFlashAttribute("alert_danger", "Invalid login or password");
			return "security/login";
		}
		JSONObject authenticatedUserHash = response.getJSONObject("authenticatedUserHash");
		session.setAttribute("login", authenticatedUserHash.getString("login"));
		session.setAttribute("hash", authenticatedUserHash.getString("hash"));
		session.setAttribute("timestamp", Long.toString(authenticatedUserHash.getLong("timestamp")));
		session.setAttribute("isAdmin", 1);
		return "redirect:/";
	}

	@GetMapping(value = "/logout")
	public String logout(Model model, HttpSession session, UriComponentsBuilder uriBuilder) {
		session.removeAttribute("login");
		session.removeAttribute("hash");
		session.removeAttribute("timestamp");
		session.removeAttribute("isAdmin");
		return "redirect:" + uriBuilder.path("/security/login").encode().toUriString();
	}

	@GetMapping(value = "wrongAccess")
	public String wrongAccess(Model model, HttpSession httpSession) {
//        UserDto userDto = (UserDto) httpSession.getAttribute("authenticatedUser");
//        model.addAttribute("authenticatedUser", userDto);
		return "/security/wrongAccess";
	}
}
