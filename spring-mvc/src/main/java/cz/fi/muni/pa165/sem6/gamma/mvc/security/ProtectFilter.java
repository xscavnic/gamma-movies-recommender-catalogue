//package cz.fi.muni.pa165.sem6.gamma.mvc.security;
//
//import cz.fi.muni.pa165.sem6.gamma.dao.AdminDAO;
//import cz.fi.muni.pa165.sem6.gamma.dao.AdminDAOImpl;
//import cz.fi.muni.pa165.sem6.gamma.dto.AdminDTO;
//import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
//import cz.fi.muni.pa165.sem6.gamma.dto.UserDTO;
//
//import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
//import cz.fi.muni.pa165.sem6.gamma.service.AdminServiceImpl;
//import cz.fi.muni.pa165.sem6.gamma.service.PersonServiceImpl;
//import cz.fi.muni.pa165.sem6.gamma.service.UserServiceImpl;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.context.support.WebApplicationContextUtils;
//
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import javax.xml.bind.DatatypeConverter;
//import java.io.IOException;
//import java.util.Optional;
//
//import static org.springframework.web.context.support.WebApplicationContextUtils.*;
//
///**
// * Protects administrative part of application.
// *
// * @author Šárka Ščavnická
// */
//
//@WebFilter(urlPatterns = {"/genre/*", "/user/*", "/rating/*", "/movie/*"})
//public class ProtectFilter implements Filter {
//    final static Logger log = LoggerFactory.getLogger(ProtectFilter.class);
//
//    /**
//     * Set AuthenticatedUser attribute for management of parts of the websites. Handles errors that occurs.
//     */
//    @Override
//    public void doFilter(ServletRequest r, ServletResponse s, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) r;
//        HttpServletResponse response = (HttpServletResponse) s;
//
//        String auth = request.getHeader("Authorization");
//        if (auth == null) {
//            response401(response);
//            return;
//        }
//
//        String[] creds = parseAuthHeader(auth);
//        String logname = creds[0];
//        String password = creds[1];
//
//        PersonServiceImpl userService = WebApplicationContextUtils.getWebApplicationContext(r.getServletContext())
//                .getBean(PersonServiceImpl.class);
//
//        AdminServiceImpl adminService = WebApplicationContextUtils.getWebApplicationContext(r.getServletContext())
//                .getBean(AdminServiceImpl.class);
//
//        // Authenticate that there is such user
//        UserDTO matchingUser = userService.findByLogin(logname);
//        if(matchingUser==null) {
//            log.warn("no user with login {}", logname);
//            response401(response);
//            return;
//        }
//
//        UserAuthenticateDTO userAuthenticateDTO = new UserAuthenticateDTO();
//        userAuthenticateDTO.setId(matchingUser.getId());
//        userAuthenticateDTO.setPassword(password);
//
//        // Proof that it is an admin
//        try {AdminDTO admin = adminService.findById(matchingUser.getId());}
//        catch (DaoException e){
//            log.warn("user not admin {}", matchingUser);
//            response401(response);
//            return;
//        }
//
//        if (userService.authenticate(userAuthenticateDTO).isEmpty()) {
//            log.warn("wrong credentials: user={} password={}", creds[0], creds[1]);
//            response401(response);
//            return;
//        }
//
//        request.setAttribute("authenticatedUser", matchingUser);
//        chain.doFilter(request, response);
//    }
//
//    private String[] parseAuthHeader(String auth) {
//        return new String(DatatypeConverter.parseBase64Binary(auth.split(" ")[1])).split(":", 2);
//    }
//
//    private void response401(HttpServletResponse response) throws IOException {
//        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//        response.setHeader("WWW-Authenticate", "Basic realm=\"type email and password\"");
//        response.getWriter().println("<html><body><h1>401 Unauthorized</h1> Go away ...</body></html>");
//    }
//
//    @Override
//    public void init(FilterConfig filterConfig){}
//
//    @Override
//    public void destroy() {
//
//    }
//
//}
