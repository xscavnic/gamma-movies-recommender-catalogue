package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingDTO;
import cz.fi.muni.pa165.sem6.gamma.service.MovieService;
import cz.fi.muni.pa165.sem6.gamma.service.PersonService;
import cz.fi.muni.pa165.sem6.gamma.service.RatingService;
import cz.fi.muni.pa165.sem6.gamma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

@Controller
@RequestMapping("/rating")
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private PersonService userService;

    @GetMapping("/list")
    public String listAll(Model model,HttpSession httpSession) {
        model.addAttribute("ratings", ratingService.findAll());
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "rating/listAll";
    }

    /**
     * Created form for creating a review
     * @param movieId movie that is being reviewed
     * @param model data to display
     * @param httpSession request to determine logged user creating the review
     * @return JSP page name
     */
    @GetMapping(value = "/new/{id}")
    public String newReviewForm(@PathVariable("id") Long movieId, Model model,
                                UriComponentsBuilder uriBuilder, HttpSession httpSession) {
        try {
            MovieDTO movieDto = movieService.findById(movieId);
            RatingCreateDTO ratingCreateDTO = new RatingCreateDTO();
            ratingCreateDTO.setMovie(movieDto);
            PersonDTO userDto = (PersonDTO) httpSession.getAttribute("authenticatedUser");
            if (userDto != null) {
                ratingCreateDTO.setPerson(userDto);
            }
            ratingCreateDTO.setReportedRating(false);
            ratingCreateDTO.setTimeAdded(LocalDate.now());
            model.addAttribute("ratingCreateDTO", ratingCreateDTO);
            model.addAttribute("authenticatedUser", userDto);
            model.addAttribute("movieDto", movieDto);
        } catch (Exception ex) {    //Service Layer Exception??
            model.addAttribute("alert_danger", "No such movie");
            return "redirect:" + uriBuilder.path("/rating/movie/{id}").buildAndExpand(movieId).encode().toUriString();
        }
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "rating/new";
    }

    /**
     *  Creates new review from information submitted to a form
     * @param ratingDTO review data
     * @param model data to display
     * @param bindingResult related result
     * @param uriBuilder uri builder
     * @param redirectAttributes related attributes
     * @param httpSession request to determine logged user
     * @return page to display
     */
    @PostMapping(value = "/create")
    public String createNewReview(@Valid @ModelAttribute("ratingCreateDTO") RatingCreateDTO ratingDTO,
                                  Model model,
                                  BindingResult bindingResult,
                                  UriComponentsBuilder uriBuilder,
                                  RedirectAttributes redirectAttributes,
                                  HttpSession httpSession) {
        PersonDTO userDto = (PersonDTO) httpSession.getAttribute("authenticatedUser");
        if (bindingResult.hasErrors()) {
            for (ObjectError ge : bindingResult.getGlobalErrors()) {
                System.err.println("ObjectError: " + ge);
            }
            for (FieldError fe : bindingResult.getFieldErrors()) {
                model.addAttribute(fe.getField() + "_error", true);
            }
            model.addAttribute("authenticatedUser", userDto);
            return "rating/new";
        }
        ratingService.create(ratingDTO);
        redirectAttributes.addFlashAttribute("alert_success", "Review was created");
        Long id = ratingDTO.getId();
        model.addAttribute("movieId", ratingDTO.getId());
        model.addAttribute("reviewList", ratingService.findAllByMovie(movieService.findById(id).getId()));
        model.addAttribute("authenticatedUser", userDto);
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "redirect:" + uriBuilder.path("/review/movie/{id}").buildAndExpand(id).encode().toUriString();
    }

    /**
     *  Gets all reviews for a given movie
     * @param id movie
     * @param model data to display
     * @param httpSession request to determine logged user
     * @return JSP page name
     */
    @GetMapping(value = "/movie/{id}")
    public String findAllForMovie(@PathVariable("id") Long id, Model model, HttpSession httpSession) {
        try {
            MovieDTO movieDto = movieService.findById(id);
            List<RatingDTO> reviews = ratingService.findAllByMovie(movieDto.getId());
            Map<Long, String> userLogins = new HashMap<>();
            for (RatingDTO review : reviews) {
                userLogins.put(review.getPerson().getId(), userService.findById(review.getPerson().getId()).getLogin());
            }
            model.addAttribute("movie", movieDto);
            model.addAttribute("reviewList", reviews);
            model.addAttribute("userLogins", userLogins);
        } catch (Exception e) { // Service layer exception ??
            model.addAttribute("alert_danger", "No such movie");
        }
        PersonDTO userDto = (PersonDTO) httpSession.getAttribute("authenticatedUser");
        model.addAttribute("authenticatedUser", userDto);
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "rating/list";
    }

    /**
     *  Deletes a review
     * @param id review to remove
     * @param model data to display
     * @param redirectAttributes related attributes
     * @param httpSession request to determine logged user
     * @return JSP page name
     */
    @GetMapping(value = "/delete/{id}")
    public String deleteById(@PathVariable("id") Long id,
                             Model model,
                             RedirectAttributes redirectAttributes,
                             UriComponentsBuilder uriBuilder,
                             HttpSession httpSession) {
        try {
            Long movieId = ratingService.findById(id).getMovie().getId();
            PersonDTO userDto = (PersonDTO) httpSession.getAttribute("authenticatedUser");
            if(userDto.getId().equals(ratingService.findById(id).getPerson().getId())){
                try {
                    ratingService.remove(ratingService.findById(id));
                    redirectAttributes.addFlashAttribute("alert_success", "Review was removed");
                } catch (Exception ex) {
                    redirectAttributes.addFlashAttribute("alert_danger", "Review delete failed");
                }
                return "redirect:" + uriBuilder.path("/review/movie/" + movieId).encode().toUriString();
            }
        } catch (Exception ex) { // Service layer exception ??
            redirectAttributes.addFlashAttribute("alert_danger", "No such movie");
            //model.addAttribute("alert_danger", "No such movie");
            return "redirect:" + uriBuilder.path("/movie/list").buildAndExpand().encode().toUriString();
        }
        model.addAttribute("alert_danger", "No such movie");
        return "redirect:" + uriBuilder.path("/movie/list").buildAndExpand().encode().toUriString();
    }

    /**
     * Creates form to edit a review
     * @param id review id
     * @param model data to display
     * @param httpSession request to determine logged user
     * @return JSP page name
     */
    @GetMapping(value = "/edit/{id}")
    public String updateReviewForm(@PathVariable("id") Long id, Model model,
                                   RedirectAttributes redirectAttributes,
                                   UriComponentsBuilder uriBuilder, HttpSession httpSession) {
        try {
            RatingDTO ratingDTO = ratingService.findById(id);
            model.addAttribute("reviewDto", ratingDTO);

            PersonDTO userDto = (PersonDTO) httpSession.getAttribute("authenticatedUser");
            model.addAttribute("authenticatedUser", userDto);
            model.addAttribute("movieDto",  movieService.findById(ratingDTO.getMovie().getId()));
        } catch (Exception ex) { // ServiceLayerException ??
            redirectAttributes.addFlashAttribute("alert_danger", "No such movie");
            return "redirect:" + uriBuilder.path("/movie/list").buildAndExpand().encode().toUriString();
        }
        return "rating/edit";
    }

    /**
     * Updates review from data from a form
     * @param ratingDTO review data
     * @param model data to display
     * @param bindingResult related result
     * @param uriBuilder uri builder
     * @param redirectAttributes related attributes
     * @param httpSession request to determine logged user
     * @return page to display
     */
    @PostMapping(value = "/edit")
    public String updateReview(@Valid @ModelAttribute("reviewDto") RatingDTO ratingDTO,
                               Model model,
                               BindingResult bindingResult,
                               UriComponentsBuilder uriBuilder,
                               RedirectAttributes redirectAttributes,
                               HttpSession httpSession) {
        PersonDTO userDto = (PersonDTO) httpSession.getAttribute("authenticatedUser");
        if (bindingResult.hasErrors()) {
            for (ObjectError ge : bindingResult.getGlobalErrors()) {
                System.err.println("ObjectError: " + ge);
            }
            for (FieldError fe : bindingResult.getFieldErrors()) {
                model.addAttribute(fe.getField() + "_error", true);
            }
            model.addAttribute("authenticatedUser", userDto);
            return "rating/edit";
        }
        ratingService.update(ratingDTO);
        redirectAttributes.addFlashAttribute("alert_success", "review was updated");
        Long id = ratingDTO.getMovie().getId();
        model.addAttribute("movie", ratingDTO.getMovie().getId());
        model.addAttribute("reviewList", ratingService.findAllByMovie(movieService.findById(ratingDTO.getMovie().getId()).getId()));
        model.addAttribute("authenticatedUser", userDto);
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "redirect:" + uriBuilder.path("/review/movie/{id}").buildAndExpand(id).encode().toUriString();
    }

}
