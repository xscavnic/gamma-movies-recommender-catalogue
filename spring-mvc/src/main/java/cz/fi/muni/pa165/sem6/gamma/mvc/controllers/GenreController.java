package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
import cz.fi.muni.pa165.sem6.gamma.service.GenreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author Jakub Kuchar
 */
@Controller
@RequestMapping("/genre")
public class GenreController {

    final static Logger log = LoggerFactory.getLogger(GenreController.class);

    @Autowired
    private GenreService genreService;

    private final RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/list")
    public String list(Model model,HttpSession httpSession) {
        model.addAttribute("genres", genreService.findAll());
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "genre/list";
    }

    /**
     * Shows a genres detail by the corresponding id
     *
     * @param id    id of showing movie
     * @param model data to display
     * @return JSP page name
     */
    @GetMapping(value = "/detail/{id}")
    public String getMovieById(@PathVariable("id") Long id, Model model,
                               UriComponentsBuilder uriBuilder,
                               RedirectAttributes redirectAttributes,
                               HttpSession httpSession) {

        try {
            model.addAttribute("genre", genreService.findById(id));
        } catch (Exception ex) {
            log.error("Exception={}", ex.getCause(), ex);
            redirectAttributes.addFlashAttribute("alert_danger", "Invalid id passed.");
            return "redirect:" + uriBuilder.path("/genre/list").encode().toUriString();
        }

        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "genre/detail";
    }

    /**
     * Prepares an empty form.
     *
     * @param model data to be displayed
     * @return JSP page
     */
    @RequestMapping(value = "/new")
    public String newCategory(Model model, HttpSession httpSession) {
        log.debug("new()");
        model.addAttribute("genreCreate", new GenreCreateDTO());

        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "genre/new";
    }

    @PostMapping("/create")
    public String create(@Valid @ModelAttribute("genreCreate") GenreCreateDTO formBean, BindingResult bindingResult,
                         Model model, RedirectAttributes redirectAttributes, UriComponentsBuilder uriComponentsBuilder) {
        log.debug("create(formBean={})", formBean);
        //in case of validation error forward back to the the form
        if (bindingResult.hasErrors()) {
            for (ObjectError ge : bindingResult.getGlobalErrors()) {
                log.trace("ObjectError: {}", ge);
            }
            for (FieldError fe : bindingResult.getFieldErrors()) {
                model.addAttribute(fe.getField() + "_error", true);
                log.trace("FieldError: {}", fe);
            }
            return "genre/new";
        }
        try {
            genreService.create(formBean);
            redirectAttributes.addFlashAttribute("alert_success", "Genre " + formBean.getName() + " was created");
        } catch (Exception e) {
            return "genre/name_create_error";
        }


        return "redirect:" + uriComponentsBuilder.path("/genre/list").toUriString();
    }

    /**
     * Returns model -- edit with filled genreDto.
     *
     * @param id of the genre to be updated
     * @param model to which is filled genreDto
     * @return model -- edit with filled genreDto
     */
    @GetMapping(value = "/edit/{id}")
    public String getEditGenreCreateDtoView(@PathVariable("id") Long id,
                                            Model model,
                                            RedirectAttributes redirectAttributes,
                                            UriComponentsBuilder uriBuilder,
                                            HttpSession httpSession) {

            GenreDTO genreDto;
            try {
                genreDto = genreService.findById(id);
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("alert_danger", "Genre id not found");
                model.addAttribute("genreList", genreService.findAll());
                return "redirect:" + uriBuilder.path("/genre/list").encode().toUriString();
            }
            Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
            model.addAttribute("isAdmin", isAdmin);
            model.addAttribute("genreEdit", genreDto);
            return "genre/edit";

    }

    /**
     * Returns list of all genres in case of successful update of new genre.
     *
     * @param genreDto to be updated
     * @param bindingResult to be updated in case of error
     * @param model to which is updated in case error
     * @param redirectAttributes to redirect in case alert success
     * @param uriBuilder to build uri
     * @return list of all genres in case of successful update of genre.
     */
    @PostMapping("/edit")
    public String editGenre(@Valid @ModelAttribute("genreEdit") GenreDTO genreDto,
                            BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes,
                            UriComponentsBuilder uriBuilder, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            for (ObjectError ge : bindingResult.getGlobalErrors()) {
                System.err.println("ObjectError: " + ge);
            }
            for (FieldError fe : bindingResult.getFieldErrors()) {
                model.addAttribute(fe.getField() + "_error", true);
            }

            return "genre/edit";
        }

        try {
            genreService.update(genreDto);
            redirectAttributes.addFlashAttribute("alert_success", "Genre was updated");
        } catch (Exception e) {
            model.addAttribute("genreError", genreDto);
            return "genre/name_edit_error";
        }

        return "redirect:" + uriBuilder.path("/genre/list").encode().toUriString();
    }

}
