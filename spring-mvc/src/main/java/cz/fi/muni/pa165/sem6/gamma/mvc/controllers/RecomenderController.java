package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/recommender")
public class RecomenderController {

    @Autowired
    private MovieService movieService;

    @GetMapping(value = "/topByRatings")
    public String recommend(Model model, HttpSession session,
                            UriComponentsBuilder uriBuilder,
                            HttpSession httpSession,
                            RedirectAttributes redirectAttributes) {
        try {
            model.addAttribute("movies", movieService.sortByRating());
        }
        catch (Exception ex) {
            redirectAttributes.addFlashAttribute("alert_danger", "Invalid id passed.");
            return "redirect:" + uriBuilder.path("/genre/list").encode().toUriString();
        }
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "/recommender/topByRatings";
    }

    @GetMapping(value = "/list")
    public String list(Model model,HttpSession httpSession) {

        model.addAttribute("movies", movieService.findAll());
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "recommender/list";
    }

    @GetMapping(value = "/byRatings/{id}")
    public String recommendbyRatings(@PathVariable("id") Long id, Model model, HttpSession session,
                                     UriComponentsBuilder uriBuilder,
                                     HttpSession httpSession,
                                     RedirectAttributes redirectAttributes) {
        try {
            model.addAttribute("movies", movieService.similarByUserRatings(movieService.findById(id),5 ));
        }
        catch (Exception ex) {
            redirectAttributes.addFlashAttribute("alert_danger", "Invalid id passed.");
            return "redirect:" + uriBuilder.path("/genre/list").encode().toUriString();
        }
        Integer isAdmin = (Integer) httpSession.getAttribute("isAdmin");
        model.addAttribute("isAdmin", isAdmin);
        return "/recommender/byRatings";
    }
}
