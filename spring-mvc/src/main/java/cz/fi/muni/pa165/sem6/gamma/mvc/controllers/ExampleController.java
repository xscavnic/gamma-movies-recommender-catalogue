package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Example controller showing as many features as possible.
 *
 * @author Martin Kuba makub@ics.muni.cz
 */
@Controller
@RequestMapping("/example")
public class ExampleController {

    final static Logger log = LoggerFactory.getLogger(ExampleController.class);

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/example")
    public String showPage() {
        return "example";
    }

}

