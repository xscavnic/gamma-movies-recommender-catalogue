package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class AdminController {

	private final RestTemplate restTemplate = new RestTemplate();

	private final HttpClient httpClient = HttpClientBuilder.create().build();

	@RequestMapping("/list_reported_ratings")
	public String reportedRatings(Model model, RedirectAttributes redirectAttributes) {
		JSONObject request = new JSONObject();
		request.put("get_reported_ratings", "");
		HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/admin");
		httpPost.addHeader("content-type", "application/json");
		JSONObject response;
		try {
			httpPost.setEntity(new StringEntity(request.toString()));
			HttpResponse result = httpClient.execute(httpPost);
			response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
		} catch (IOException e) {
			redirectAttributes.addFlashAttribute("alert_danger", "Unexpected error occurred while trying to list reported ratings");
			return "admin/listReportedRatings";
		}
		JSONArray jsonArray = response.getJSONArray("Payload");
		List<JSONObject> ratings = new ArrayList<>();
		for (int i = 0; i < jsonArray.length(); i++) {
			ratings.add(jsonArray.getJSONObject(i));
		}
		model.addAttribute("ratings", ratings);
		return "admin/listReportedRatings";
	}

	@RequestMapping("/login")
	public String login(Model model, HttpSession session) {
		model.addAttribute("user", new UserAuthenticateDTO());
		if (session.getAttribute("login") != null && session.getAttribute("hash") != null && session.getAttribute("timestamp") != null) {
			JSONObject request = new JSONObject();
			request.put("login", session.getAttribute("login").toString());
			request.put("hash", session.getAttribute("hash").toString());
			request.put("timestamp", Integer.parseInt(session.getAttribute("timestamp").toString()));
			HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/admin/validate_hash");
			httpPost.addHeader("content-type", "application/json");
			JSONObject response;
			try {
				httpPost.setEntity(new StringEntity(request.toString()));
				HttpResponse result = httpClient.execute(httpPost);
				response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
			} catch (IOException e) {
				return "admin/login";
			}
			if (Objects.equals(response.getString("Status"), "OK")) {
				return "redirect:/admin/dashboard";
			}
		}
		return "admin/login";
	}

	@PostMapping("/logon")
	public String logon(Model model, HttpSession session, @Valid @ModelAttribute("user") UserAuthenticateDTO userAuthenticateDTO,
						BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		JSONObject request = new JSONObject();
		request.put("login", userAuthenticateDTO.getLogin());
		request.put("password", userAuthenticateDTO.getPassword());
		HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/admin/login");
		httpPost.addHeader("content-type", "application/json");
		JSONObject response;
		try {
			httpPost.setEntity(new StringEntity(request.toString()));
			HttpResponse result = httpClient.execute(httpPost);
			response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
		} catch (IOException e) {
			redirectAttributes.addFlashAttribute("alert_danger", "Unexpected error occurred while trying to login");
			return "admin/login";
		}
		if (Objects.equals(response.getString("Status"), "ERROR")) {
			redirectAttributes.addFlashAttribute("alert_danger", "Invalid login or password");
			return "admin/login";
		}
		JSONObject authenticatedUserHash = response.getJSONObject("authenticatedUserHash");
		session.setAttribute("login", authenticatedUserHash.getString("login"));
		session.setAttribute("hash", authenticatedUserHash.getString("hash"));
		session.setAttribute("timestamp", Long.toString(authenticatedUserHash.getLong("timestamp")));
		session.setAttribute("isAdmin", 2);
		return "redirect:/admin/dashboard";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession httpSession) {
		httpSession.removeAttribute("login");
		httpSession.removeAttribute("hash");
		httpSession.removeAttribute("timestamp");
		httpSession.removeAttribute("isAdmin");
		return "redirect:/admin/login";
	}

	@RequestMapping("/dashboard")
	public String dashboard(Model model, HttpSession session) {
		String x = checkAuthenticatedUserHash(session);
		if (x != null) return x;
		return "admin/dashboard";
	}

	@RequestMapping(value = {"", "/"})
	public String home(Model model) {
		return "redirect:/admin/dashboard";
	}

	private String checkAuthenticatedUserHash(HttpSession session) {
		if (session.getAttribute("login") == null || session.getAttribute("hash") == null || session.getAttribute("timestamp") == null) {
			System.out.println("Missing");
			return "redirect:/admin/login";
		}
		JSONObject request = new JSONObject();
		request.put("login", session.getAttribute("login").toString());
		request.put("hash", session.getAttribute("hash").toString());
		request.put("timestamp", Integer.parseInt(session.getAttribute("timestamp").toString()));
		HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/admin/validate_hash");
		httpPost.addHeader("content-type", "application/json");
		JSONObject response;
		try {
			httpPost.setEntity(new StringEntity(request.toString()));
			HttpResponse result = httpClient.execute(httpPost);
			response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
		} catch (IOException e) {
			System.out.println("Exception");
			return "redirect:/admin/login";
		}
		if (Objects.equals(response.getString("Status"), "ERROR")) {
			System.out.println("Error");
			return "redirect:/admin/login";
		}
		return null;
	}
}
