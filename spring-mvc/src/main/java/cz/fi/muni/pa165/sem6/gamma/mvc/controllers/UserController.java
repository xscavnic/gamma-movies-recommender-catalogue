package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.PersonCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserChangePasswordDTO;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpResponse;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Controller
@RequestMapping("/user")
public class UserController {

	private final RestTemplate restTemplate = new RestTemplate();

	private final HttpClient httpClient = HttpClientBuilder.create().build();


	/**
	 * Prepares an empty form.
	 *
	 * @param model data to be displayed
	 * @return JSP page
	 */
	@RequestMapping(value = "/chpasswd")
	public String newPassword(Model model) {
		model.addAttribute("passwordChange", new UserChangePasswordDTO());
		return "user/changePassword";
	}

	@PostMapping("/chpasswd/save")
	public String changePassword(@Valid @ModelAttribute("passwordChange") UserChangePasswordDTO formBean, BindingResult bindingResult,
								 Model model, RedirectAttributes redirectAttributes, UriComponentsBuilder uriComponentsBuilder, HttpSession httpSession) {
		//in case of validation error forward back to the form
		if (bindingResult.hasErrors()) {
//            for (ObjectError ge : bindingResult.getGlobalErrors()) {
//            }
			for (FieldError fe : bindingResult.getFieldErrors()) {
				model.addAttribute(fe.getField() + "_error", true);
			}
			return "user/changePassword";
		}
		//Change password
		JSONObject request = new JSONObject();
		JSONObject authenticatedUserHash = new JSONObject();
		authenticatedUserHash.put("login", httpSession.getAttribute("login").toString());
		authenticatedUserHash.put("hash", httpSession.getAttribute("hash").toString());
		authenticatedUserHash.put("timestamp", Integer.parseInt(httpSession.getAttribute("timestamp").toString()));
		request.put("authenticatedUserHash", authenticatedUserHash);
		JSONObject changePassword = new JSONObject();
		changePassword.put("login", formBean.getLogin());
		changePassword.put("old_password", formBean.getOldPassword());
		changePassword.put("new_password", formBean.getNewPassword());
		request.put("change_password", changePassword);
		HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest");
		httpPost.addHeader("content-type", "application/json");
		JSONObject response;
		try {
			httpPost.setEntity(new StringEntity(request.toString()));
			HttpResponse result = httpClient.execute(httpPost);
			response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
		} catch (IOException e) {
			redirectAttributes.addFlashAttribute("alert_danger", "Unexpected error occurred while trying change your password");
			return "user/changePassword";
		}
		if (Objects.equals(response.getString("Status"), "ERROR")) {
			if (response.getString("Error").contains("Old password")) {
				model.addAttribute("old_password_error", true);
			} else {
				model.addAttribute("login_error", true);
			}
			return "user/changePassword";
		}
		httpSession.setAttribute("login", response.getJSONObject("authenticatedUserHash").getString("login"));
		httpSession.setAttribute("hash", response.getJSONObject("authenticatedUserHash").getString("hash"));
		httpSession.setAttribute("timestamp", Long.toString(response.getJSONObject("authenticatedUserHash").getLong("timestamp")));
		//report success
		redirectAttributes.addFlashAttribute("alert_success", "Password changed!");
		return "redirect:/";
	}

	@RequestMapping("/register")
	public String register(Model model) {
		model.addAttribute("newPerson", new PersonCreateDTO());
		return "user/register";
	}

	@PostMapping("/create")
	public String create(@Valid @ModelAttribute("userRegister") PersonCreateDTO formBean, BindingResult bindingResult,
						 Model model, RedirectAttributes redirectAttributes, UriComponentsBuilder uriComponentsBuilder) {
		if (bindingResult.hasErrors()) {
//            for (ObjectError ge : bindingResult.getGlobalErrors()) {
//            }
			for (FieldError fe : bindingResult.getFieldErrors()) {
				model.addAttribute(fe.getField() + "_error", true);
			}
			return "user/register";
		}
		JSONObject request = new JSONObject();
		request.put("login", formBean.getLogin());
		request.put("password", formBean.getPassword());
		request.put("firstName", formBean.getFirstName());
		request.put("lastName", formBean.getLastName());
		request.put("country", formBean.getCountry());
		request.put("birthdate", formBean.getBirthDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
		HttpPost httpPost = new HttpPost("http://localhost:8080/pa165/rest/person_register");
		httpPost.addHeader("content-type", "application/json");
		JSONObject response;
		try {
			httpPost.setEntity(new StringEntity(request.toString()));
			HttpResponse result = httpClient.execute(httpPost);
			response = new JSONObject(EntityUtils.toString(result.getEntity(), "UTF-8"));
		} catch (IOException e) {
			redirectAttributes.addFlashAttribute("alert_danger", "Unexpected error occurred while trying to register new user");
			return "user/register";
		}
		if (Objects.equals(response.getString("Status"), "ERROR")) {
			model.addAttribute("login_error", true);
			return "user/register";
		}
		redirectAttributes.addFlashAttribute("alert_success", "Registration successful");
		return "redirect:/security/login";
	}


}
