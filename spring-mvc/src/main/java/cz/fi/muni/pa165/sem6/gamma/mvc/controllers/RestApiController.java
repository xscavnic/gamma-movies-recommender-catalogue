package cz.fi.muni.pa165.sem6.gamma.mvc.controllers;

import cz.fi.muni.pa165.sem6.gamma.dto.AdminCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.AdminDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserChangePasswordDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserDTO;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import cz.fi.muni.pa165.sem6.gamma.service.AdminService;
import cz.fi.muni.pa165.sem6.gamma.service.GenreService;
import cz.fi.muni.pa165.sem6.gamma.service.MovieService;
import cz.fi.muni.pa165.sem6.gamma.service.PersonService;
import cz.fi.muni.pa165.sem6.gamma.service.RatingService;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * REST API for both person and admin
 *
 * @author Kristián Malák
 */

@RestController
@RequestMapping("/rest")
class RestApiController {

	@Inject
	AdminService adminService;

	@Inject
	PersonService personService;

	@Inject
	GenreService genreService;

	@Inject
	MovieService movieService;

	@Inject
	RatingService ratingService;

	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String main(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		PersonDTO personDTO = checkAuthenticatedUserHashAndReturnPerson(request, response);
		if (personDTO == null) return response.toString();
		long paramCount = Stream.of("change_password", "list_movies", "topByGenre", "similarByGenre", "similarByRatings", "rate_movie", "edit_user", "list_ratings", "remove_rating", "report_rating").map(request::has).count();
		if (paramCount != 1) {
			response.put("Status", "ERROR");
			response.put("Error", "Please provide exactly one argument.");
			return response.toString();
		}
		if (request.has("change_password")) {
			JSONObject changePassword = request.getJSONObject("change_password");
			if (!Objects.equals(personDTO.getLogin(), changePassword.getString("login"))) {
				response.put("Status", "ERROR");
				response.put("Error", "Cannot change password of other user than the logged in one.");
				return response.toString();
			}
			try {
				personService.changePassword(new UserChangePasswordDTO(changePassword.getString("login"), changePassword.getString("old_password"), changePassword.getString("new_password")));
				response.put("Status", "OK");
				response.put("Message", "Password successfully changed");
				PersonDTO person = personService.findByLogin(personDTO.getLogin());
				JSONObject authenticatedUserHash = new JSONObject();
				Instant timestamp = Instant.now();
				authenticatedUserHash.put("login", person.getLogin());
				authenticatedUserHash.put("hash", Integer.toHexString(createAuthenticatedUserHash(person.getLogin(), timestamp, person)));
				authenticatedUserHash.put("timestamp", timestamp.getEpochSecond());
				response.put("authenticatedUserHash", authenticatedUserHash);
			} catch (SecurityException e) {
				response.put("Status", "ERROR");
				response.put("Error", "Old password does not match.");
			}
		} else if (request.has("list_movies")) {
			response.put("Status", "OK");
			List<MovieDTO> movieList;
			if (request.getJSONObject("list_movies").has("genre_ids")) {
				JSONArray genreIdsArray = request.getJSONObject("list_movies").getJSONArray("genre_ids");
				Set<Long> genreIdsSet = new HashSet<>();
				for (int i = 0; i < genreIdsArray.length(); i++) {
					genreIdsSet.add(genreIdsArray.getLong(i));
				}
				movieList = new ArrayList<>(movieService.getByGenres(genreIdsSet));
			} else {
				movieList = movieService.findAll();
			}
			preparePayload(response, movieList);
		} else if (request.has("topByGenre")) {
			response.put("Status", "OK");
			preparePayload(response, movieService.topMoviesByGenre(movieService.findById(request.getJSONObject("topByGenre").getLong("movie_id")), request.getJSONObject("topByGenre").getInt("max")));
		} else if (request.has("similarByGenre")) {
			response.put("Status", "OK");
			preparePayload(response, movieService.similarByGenre(movieService.findById(request.getJSONObject("similarByGenre").getLong("movie_id")), request.getJSONObject("similarByGenre").getInt("max")));
		} else if (request.has("similarByRatings")) {
			response.put("Status", "OK");
			preparePayload(response, movieService.similarByUserRatings(movieService.findById(request.getJSONObject("similarByRatings").getLong("movie_id")), request.getJSONObject("similarByRatings").getInt("max")));
		} else if (request.has("rate_movie")) {
			RatingCreateDTO ratingCreateDTO = new RatingCreateDTO();
			ratingCreateDTO.setRatingDescription(request.getJSONObject("rate_movie").getString("description"));
			ratingCreateDTO.setMovie(movieService.findById(request.getJSONObject("rate_movie").getLong("movie_id")));
			ratingCreateDTO.setTotalStars(request.getJSONObject("rate_movie").getInt("total_stars"));
			ratingCreateDTO.setPerson(personDTO);
			ratingCreateDTO.setTimeAdded(LocalDate.now());
			ratingService.create(ratingCreateDTO);
			response.put("Status", "OK");
		} else if (request.has("edit_user")) {
			if (request.getJSONObject("edit_user").has("login")) {
				try {
					personService.findByLogin(request.getJSONObject("edit_user").getString("login"));
					response.put("Status", "ERROR");
					response.put("Error", "Login already in use.");
					return response.toString();
				} catch (EntityNotFoundException e) {
					personDTO.setLogin(request.getJSONObject("edit_user").getString("login"));
				}
			}
			if (request.getJSONObject("edit_user").has("firstName")) {
				personDTO.setFirstName(request.getJSONObject("edit_user").getString("firstName"));
			}
			if (request.getJSONObject("edit_user").has("lastName")) {
				personDTO.setLastName(request.getJSONObject("edit_user").getString("lastName"));
			}
			if (request.getJSONObject("edit_user").has("country")) {
				personDTO.setCountry(request.getJSONObject("edit_user").getString("country"));
			}
			if (request.getJSONObject("edit_user").has("birthDate")) {
				personDTO.setBirthDate(LocalDate.parse(request.getJSONObject("edit_user").getString("birthDate")));
			}
			personService.update(personDTO);
			response.put("Status", "OK");
		} else if (request.has("list_ratings")) {
			response.put("Status", "OK");
			response.put("payload", new JSONArray(ratingService.findAllByMovie(request.getJSONObject("list_ratings").getLong("movie_id")).stream().map(ratingDTOToJSON()).collect(Collectors.toList())));
		} else if (request.has("remove_rating")) {
			RatingDTO ratingDTO = ratingService.findById(request.getJSONObject("remove_rating").getLong("id"));
			if (Objects.equals(ratingDTO.getPerson().getId(), personDTO.getId())) {
				ratingService.remove(ratingDTO);
				response.put("Status", "OK");
				response.put("Message", "Rating successfully removed.");
			} else {
				response.put("Status", "ERROR");
				response.put("Error", "Cannot remove rating of another user.");
			}
		} else if (request.has("report_rating")) {
			ratingService.reportRating(ratingService.findById(request.getJSONObject("report_rating").getLong("id")));
			response.put("Status", "OK");
			response.put("Message", "Rating successfully reported.");
		}
		return response.toString();
	}

	@RequestMapping(value = "/person_info", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String getPersonData(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		PersonDTO personDTO = checkAuthenticatedUserHashAndReturnPerson(request, response);
		if (personDTO == null) return response.toString();
		response.put("Status", "OK");
		JSONObject personInfo = new JSONObject();
		personInfo.put("login", personDTO.getLogin());
		personInfo.put("firstName", personDTO.getFirstName());
		personInfo.put("lastName", personDTO.getLastName());
		personInfo.put("birthdate", personDTO.getBirthDate());
		personInfo.put("country", personDTO.getCountry());
		personInfo.put("ratings_num", personDTO.getRatings().size());
		response.put("payload", personInfo);
		return response.toString();
	}

	@PostMapping(value = "/person_ratings", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String getPersonRatings(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		PersonDTO personDTO = checkAuthenticatedUserHashAndReturnPerson(request, response);
		if (personDTO == null) return response.toString();
		response.put("Status", "OK");
		response.put("payload", new JSONArray(ratingService.getRatingByUser(personDTO.getId()).stream().map(ratingDTOToJSON()).collect(Collectors.toList())));
		return response.toString();
	}

	@PostMapping(value = "/person_register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerPerson(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		try {
			personService.findByLogin(request.getString("login"));
			response.put("Status", "ERROR");
			response.put("Error", "Login already in use.");
		} catch (DaoException e) {
			personService.register(new PersonCreateDTO(request.getString("login"), request.getString("password"), request.getString("firstName"), request.getString("lastName"), request.getString("country"), LocalDate.parse(request.getString("birthdate"))));
			response.put("Status", "OK");
		}
		System.out.println(response.toString());
		return response.toString();
	}

	@PostMapping(value = "/person_login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String loginPerson(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		Optional<PersonDTO> person = personService.authenticate(new UserAuthenticateDTO(request.getString("login"), request.getString("password")));
		if (person.isPresent()) {
			JSONObject authenticatedUserHash = new JSONObject();
			Instant timestamp = Instant.now();
			authenticatedUserHash.put("login", person.get().getLogin());
			authenticatedUserHash.put("hash", Integer.toHexString(createAuthenticatedUserHash(person.get().getLogin(), timestamp, person.get())));
			authenticatedUserHash.put("timestamp", timestamp.getEpochSecond());
			response.put("Status", "OK");
			response.put("authenticatedUserHash", authenticatedUserHash);
		} else {
			response.put("Status", "ERROR");
			response.put("Error", "Login unsuccessful.");
		}
		return response.toString();
	}

	private int createAuthenticatedUserHash(String login, Instant timestamp, UserDTO userDTO) {
		System.out.printf("login: %s, timestamp: %d, id: %d, password: %s%n", login, timestamp.getEpochSecond(), userDTO.getId(), userDTO.getPassword());
		int hash = Objects.hash(userDTO.getPassword(), login, userDTO.getId(), timestamp.getEpochSecond());
		System.out.println(hash);
		return hash;
	}

	private Function<RatingDTO, JSONObject> ratingDTOToJSON() {
		return ratingDTO -> {
			JSONObject ratingJSON = new JSONObject();
			ratingJSON.put("id", ratingDTO.getId());
			ratingJSON.put("description", ratingDTO.getRatingDescription());
			ratingJSON.put("total_stars", ratingDTO.getTotalStars());
			ratingJSON.put("added", ratingDTO.getTimeAdded());
			ratingJSON.put("person_name", ratingDTO.getPerson().getFirstName() + " " + ratingDTO.getPerson().getLastName());
			return ratingJSON;
		};
	}

	private void preparePayload(JSONObject response, List<MovieDTO> movieList) {
		response.put("payload", new JSONArray(movieList.stream().map(movieDTOToJSON()).collect(Collectors.toList())));
	}

	private Function<MovieDTO, JSONObject> movieDTOToJSON() {
		return movieDTO -> {
			JSONObject m = new JSONObject();
			m.put("id", movieDTO.getId());
			m.put("title", movieDTO.getTitle());
			m.put("description", movieDTO.getDescription());
			m.put("director", movieDTO.getDirector());
			m.put("genres", movieDTO.getGenres().stream().map(genreDTO -> JSONObject.stringToValue(String.format("{name: %s, description: %s}", genreDTO.getName(), genreDTO.getDescription()))).collect(Collectors.toList()));
			m.put("actors", movieDTO.getActors());
			m.put("rating", movieService.getMovieRatings(movieDTO));
			return m;
		};
	}

	@PostMapping(value = "/validate_hash", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String checkPersonHash(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		String login = request.getString("login");
		String userHash = request.getString("hash");
		Instant timestamp = Instant.ofEpochSecond(request.getLong("timestamp"));
		if (ChronoUnit.HOURS.between(timestamp, Instant.now()) > 24) {
			response.put("Status", "ERROR");
			response.put("Error", "Hash validation unsuccessful.");
			return response.toString();
		}
		PersonDTO personDTO;
		try {
			personDTO = personService.findByLogin(login);
		} catch (DaoException e) {
			response.put("Status", "ERROR");
			response.put("Error", "Hash validation unsuccessful.");
			return response.toString();
		}
		if (!Integer.toHexString(createAuthenticatedUserHash(login, timestamp, personDTO)).equals(userHash)) {
			response.put("Status", "ERROR");
			response.put("Error", "Hash validation unsuccessful.");
			return response.toString();
		}
		response.put("Status", "OK");
		response.put("Message", "Hash valid.");
		return response.toString();
	}

	private PersonDTO checkAuthenticatedUserHashAndReturnPerson(JSONObject request, JSONObject response) {
		if (!request.has("authenticatedUserHash")) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return null;
		}
		String login = request.getJSONObject("authenticatedUserHash").getString("login");
		String userHash = request.getJSONObject("authenticatedUserHash").getString("hash");
		Instant timestamp = Instant.ofEpochSecond(request.getJSONObject("authenticatedUserHash").getLong("timestamp"));
		if (timestamp.isAfter(Instant.now())) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return null;
		}
		PersonDTO personDTO;
		try {
			personDTO = personService.findByLogin(login);
		} catch (EntityNotFoundException e) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return null;
		}
		if (createAuthenticatedUserHash(login, timestamp, personDTO) != Integer.parseInt(userHash, 16)) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return null;
		}
		if (ChronoUnit.HOURS.between(timestamp, Instant.now()) > 1) {
			if (ChronoUnit.HOURS.between(timestamp, Instant.now()) > 24) {
				response.put("Status", "ERROR");
				response.put("Error", "User needs to be logged in.");
				return null;
			}
			JSONObject authenticatedUserHash = new JSONObject();
			Instant newTimestamp = Instant.now();
			authenticatedUserHash.put("login", login);
			authenticatedUserHash.put("hash", Integer.toHexString(createAuthenticatedUserHash(login, newTimestamp, personDTO)));
			authenticatedUserHash.put("timestamp", newTimestamp.getEpochSecond());
			response.put("authenticatedUserHash", authenticatedUserHash);
		}
		return personDTO;
	}

	@PostMapping(value = "/admin", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String mainForAdmin(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		if (!request.has("authenticatedUserHash")) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return response.toString();
		}
		String login = request.getJSONObject("authenticatedUserHash").getString("login");
		String userHash = request.getJSONObject("authenticatedUserHash").getString("hash");
		Instant timestamp = Instant.ofEpochSecond(request.getJSONObject("authenticatedUserHash").getLong("timestamp"));
		if (timestamp.isAfter(Instant.now())) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return response.toString();
		}
		AdminDTO adminDTO;
		try {
			adminDTO = adminService.findByLogin(login);
		} catch (EntityNotFoundException e) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return response.toString();
		}
		if (createAuthenticatedUserHash(login, timestamp, adminDTO) != Integer.parseInt(userHash, 16)) {
			response.put("Status", "ERROR");
			response.put("Error", "User needs to be logged in.");
			return response.toString();
		}
		if (ChronoUnit.HOURS.between(timestamp, Instant.now()) > 1) {
			if (ChronoUnit.HOURS.between(timestamp, Instant.now()) > 24) {
				response.put("Status", "ERROR");
				response.put("Error", "User needs to be logged in.");
				return response.toString();
			}
			JSONObject authenticatedUserHash = new JSONObject();
			Instant newTimestamp = Instant.now();
			authenticatedUserHash.put("login", login);
			authenticatedUserHash.put("hash", Integer.toHexString(createAuthenticatedUserHash(login, newTimestamp, adminDTO)));
			authenticatedUserHash.put("timestamp", newTimestamp.getEpochSecond());
			response.put("authenticatedUserHash", authenticatedUserHash);
		}
		long paramCount = Stream.of("change_password", "list_movies", "list_users", "edit_user", "list_ratings", "remove_rating", "add_movie", "create_genre", "remove_movie", "update_movie", "edit_genre", "get_reported_ratings", "blacklist").map(request::has).count();
		if (paramCount != 1) {
			response.put("Status", "ERROR");
			response.put("Error", "Please provide exactly one argument.");
			return response.toString();
		}
		if (request.has("change_password")) {
			JSONObject changePassword = request.getJSONObject("change_password");
			if (!Objects.equals(adminDTO.getLogin(), changePassword.getString("login"))) {
				response.put("Status", "ERROR");
				response.put("Error", "Cannot change password of other user than the logged in one.");
				return response.toString();
			}
			try {
				adminService.changePassword(new UserChangePasswordDTO(changePassword.getString("login"), changePassword.getString("old_password"), changePassword.getString("new_password")));
				response.put("Status", "OK");
				response.put("Message", "Password successfully changed");
				AdminDTO admin = adminService.findByLogin(adminDTO.getLogin());
				JSONObject authenticatedUserHash = new JSONObject();
				timestamp = Instant.now();
				authenticatedUserHash.put("login", admin.getLogin());
				authenticatedUserHash.put("hash", Integer.toHexString(createAuthenticatedUserHash(admin.getLogin(), timestamp, admin)));
				authenticatedUserHash.put("timestamp", timestamp.getEpochSecond());
				response.put("authenticatedUserHash", authenticatedUserHash);
			} catch (SecurityException e) {
				response.put("Status", "ERROR");
				response.put("Error", "Old password does not match.");
			}
		} else if (request.has("list_movies")) {
			response.put("Status", "OK");
			List<MovieDTO> movieList;
			if (request.getJSONObject("list_movies").has("genre_ids")) {
				JSONArray genreIdsArray = request.getJSONObject("list_movies").getJSONArray("genre_ids");
				Set<Long> genreIdsSet = new HashSet<>();
				for (int i = 0; i < genreIdsArray.length(); i++) {
					genreIdsSet.add(genreIdsArray.getLong(i));
				}
				movieList = new ArrayList<>(movieService.getByGenres(genreIdsSet));
			} else {
				movieList = movieService.findAll();
			}
			preparePayload(response, movieList);
		} else if (request.has("edit_user")) {
			try {
				adminService.findByLogin(request.getJSONObject("edit_user").getString("login"));
				response.put("Status", "ERROR");
				response.put("Error", "Login already in use.");
				return response.toString();
			} catch (EntityNotFoundException e) {
				adminDTO.setLogin(request.getJSONObject("edit_user").getString("login"));
			}
			adminService.update(adminDTO);
		} else if (request.has("list_users")) {
			List<PersonDTO> personDTOS;
			if (request.getJSONObject("list_users").has("blacklisted")) {
				personDTOS = personService.findAll(request.getJSONObject("list_users").getBoolean("blacklisted"));
			} else {
				personDTOS = personService.findAll();
			}
			response.put("Status", "OK");
			response.put("Payload", new JSONArray(personDTOS.stream().map(personDTO -> {
				JSONObject person = new JSONObject();
				person.put("login", personDTO.getLogin());
				person.put("name", personDTO.getFirstName() + " " + personDTO.getLastName());
				person.put("blacklisted", personDTO.getBlacklisted());
				person.put("ratings_num", personDTO.getRatings().size());
				return person;
			})));
		} else if (request.has("remove_rating")) {
			RatingDTO ratingDTO = ratingService.findById(request.getJSONObject("remove_rating").getLong("id"));
			if (ratingDTO.getReportedRating()) {
				ratingService.remove(ratingDTO);
				response.put("Status", "OK");
				response.put("Message", "Rating successfully removed.");
			} else {
				response.put("Status", "ERROR");
				response.put("Error", "Cannot remove rating which has not been reported.");
			}
		} else if (request.has("add_movie")) {
			MovieCreateDTO movieCreateDTO = new MovieCreateDTO();
			movieCreateDTO.setTitle(request.getJSONObject("add_movie").getString("title"));
			movieCreateDTO.setDescription(request.getJSONObject("add_movie").getString("description"));
			movieCreateDTO.setDirector(request.getJSONObject("add_movie").getString("director"));
			JSONArray genres = request.getJSONObject("add_movie").getJSONArray("genre_ids");
			Set<GenreDTO> genreDTOS = new HashSet<>();
			for (int i = 0; i < genres.length(); i++) {
				genreDTOS.add(genreService.findById(genres.getLong(i)));
			}
			movieCreateDTO.setGenres(genreDTOS);
			JSONArray actorsJson = request.getJSONObject("add_movie").getJSONArray("actors");
			List<String> actors = new ArrayList<>();
			for (int i = 0; i < actorsJson.length(); i++) {
				actors.add(actorsJson.getString(i));
			}
			movieCreateDTO.setActors(actors);
/*			if (request.getJSONObject("add_movie").has("images")) {
				JSONArray imageJsonArray = request.getJSONObject("add_movie").getJSONArray("images");
				byte[] image = new byte[1024];
				for (int i = 0; i < imageJsonArray.length(); i++) {
					image.add(ArrayUtils.toObject(Base64.getDecoder().decode(imageJsonArray.getString(i))));
				}
				movieCreateDTO.setImage(image);
			}*/
			movieService.create(movieCreateDTO);
			response.put("Status", "OK");
			response.put("Message", "Movie successfully created.");
		} else if (request.has("create_genre")) {
			GenreCreateDTO genreCreateDTO = new GenreCreateDTO();
			genreCreateDTO.setName(request.getJSONObject("create_genre").getString("name"));
			genreCreateDTO.setDescription(request.getJSONObject("create_genre").getString("description"));
			genreService.create(genreCreateDTO);
			response.put("Status", "OK");
			response.put("Message", "Genre successfully created.");
		} else if (request.has("remove_movie")) {
			movieService.remove(movieService.findById(request.getJSONObject("remove_movie").getLong("id")));
			response.put("Status", "OK");
			response.put("Message", "Movie successfully removed.");
		} else if (request.has("update_movie")) {
			MovieDTO movieDTO = movieService.findById(request.getJSONObject("update_movie").getLong("id"));
			if (request.getJSONObject("update_movie").has("title")) {
				movieDTO.setTitle(request.getJSONObject("update_movie").getString("title"));
			}
			if (request.getJSONObject("update_movie").has("description")) {
				movieDTO.setDescription(request.getJSONObject("update_movie").getString("description"));
			}
			if (request.getJSONObject("update_movie").has("director")) {
				movieDTO.setDirector(request.getJSONObject("update_movie").getString("director"));
			}
			if (request.getJSONObject("update_movie").has("genre_ids")) {
				JSONArray genres = request.getJSONObject("update_movie").getJSONArray("genre_ids");
				Set<GenreDTO> genreDTOS = new HashSet<>();
				for (int i = 0; i < genres.length(); i++) {
					genreDTOS.add(genreService.findById(genres.getLong(i)));
				}
				movieDTO.setGenres(genreDTOS);
			}
			if (request.getJSONObject("update_movie").has("actors")) {
				JSONArray actorsJson = request.getJSONObject("update_movie").getJSONArray("actors");
				List<String> actors = new ArrayList<>();
				for (int i = 0; i < actorsJson.length(); i++) {
					actors.add(actorsJson.getString(i));
				}
				movieDTO.setActors(actors);
			}
			/*if (request.getJSONObject("update_movie").has("images")) {
				JSONArray imageJsonArray = request.getJSONObject("update_movie").getJSONArray("images");
				byte[] image = new byte[1024];
				for (int i = 0; i < imageJsonArray.length(); i++) {
					image.add(ArrayUtils.toObject(Base64.getDecoder().decode(imageJsonArray.getString(i))));
				}
				movieDTO.setImage(image);
			}*/
			movieService.update(movieDTO);
			response.put("Status", "OK");
			response.put("Message", "Movie successfully updated.");
		} else if (request.has("edit_genre")) {
			GenreDTO genreDTO = genreService.findById(request.getJSONObject("edit_genre").getLong("id"));
			if (request.getJSONObject("edit_genre").has("name")) {
				genreDTO.setName(request.getJSONObject("edit_genre").getString("name"));
			}
			if (request.getJSONObject("edit_genre").has("description")) {
				genreDTO.setDescription(request.getJSONObject("edit_genre").getString("description"));
			}
			genreService.update(genreDTO);
			response.put("Status", "OK");
			response.put("Message", "Genre successfully edited.");
		} else if (request.has("get_reported_ratings")) {
			response.put("Status", "OK");
			response.put("Payload", new JSONArray(ratingService.findAll().stream().filter(RatingDTO::getReportedRating).map(ratingDTO -> {
				JSONObject rating = new JSONObject();
				rating.put("id", ratingDTO.getId());
				rating.put("movie_id", ratingDTO.getMovie().getId());
				rating.put("description", ratingDTO.getRatingDescription());
				rating.put("total_stars", ratingDTO.getTotalStars());
				rating.put("user_login", ratingDTO.getPerson().getLogin());
				rating.put("added", ratingDTO.getTimeAdded().format(DateTimeFormatter.ISO_LOCAL_DATE));
				return rating;
			}).collect(Collectors.toList())));
		} else if (request.has("blacklist")) {
			PersonDTO personDTO = personService.findByLogin(request.getJSONObject("blacklist").getString("person_login"));
			if (personDTO.getRatings().stream().anyMatch(RatingDTO::getReportedRating)) {
				personService.ban(personDTO, true);
				response.put("Status", "OK");
				response.put("Message", "Person successfully banned.");
			}
			response.put("Status", "ERROR");
			response.put("Error", "Cannot ban person with no reported ratings.");
		}
		return response.toString();
	}

	@PostMapping(value = "/admin/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerAdmin(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		if (!request.has("authenticatedUserHash")) {
			response.put("Status", "ERROR");
			response.put("Error", "A new admin needs to be registered by an existing, already logged in, admin.");
			return response.toString();
		}
		String login = request.getJSONObject("authenticatedUserHash").getString("login");
		String userHash = request.getJSONObject("authenticatedUserHash").getString("hash");
		Instant timestamp = Instant.ofEpochSecond(request.getJSONObject("authenticatedUserHash").getLong("timestamp"));
		AdminDTO adminDTO;
		try {
			adminDTO = adminService.findByLogin(login);
		} catch (DaoException e) {
			response.put("Status", "ERROR");
			response.put("Error", "A new admin needs to be registered by an existing, already logged in, admin.");
			return response.toString();
		}
		if (createAuthenticatedUserHash(login, timestamp, adminDTO) != Integer.parseInt(userHash, 16)) {
			response.put("Status", "ERROR");
			response.put("Error", "A new admin needs to be registered by an existing, already logged in, admin.");
			return response.toString();
		}
		try {
			adminService.findByLogin(request.getString("login"));
			response.put("Status", "ERROR");
			response.put("Error", "Login already in use.");
		} catch (EntityNotFoundException e) {
			adminService.register(new AdminCreateDTO(request.getString("login"), request.getString("password")));
			response.put("Status", "OK");
		}
		return response.toString();
	}

	@PostMapping(value = "/admin/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String loginAdmin(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		Optional<AdminDTO> admin = adminService.authenticate(new UserAuthenticateDTO(request.getString("login"), request.getString("password")));
		if (admin.isPresent()) {
			JSONObject authenticatedUserHash = new JSONObject();
			Instant timestamp = Instant.now();
			authenticatedUserHash.put("login", admin.get().getLogin());
			authenticatedUserHash.put("hash", Integer.toHexString(createAuthenticatedUserHash(admin.get().getLogin(), timestamp, admin.get())));
			authenticatedUserHash.put("timestamp", timestamp.getEpochSecond());
			response.put("Status", "OK");
			response.put("authenticatedUserHash", authenticatedUserHash);
		} else {
			response.put("Status", "ERROR");
			response.put("Error", "Login unsuccessful.");
		}
		System.out.println(response.toString());
		return response.toString();
	}

	@PostMapping(value = "/admin/validate_hash", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String validateAdminHash(@RequestBody String json) {
		JSONObject request = new JSONObject(json);
		JSONObject response = new JSONObject();
		String login = request.getString("login");
		String userHash = request.getString("hash");
		Instant timestamp = Instant.ofEpochSecond(request.getLong("timestamp"));
		if (ChronoUnit.HOURS.between(timestamp, Instant.now()) > 24) {
			response.put("Status", "ERROR");
			response.put("Error", "Hash validation unsuccessful A.");
			return response.toString();
		}
		AdminDTO adminDTO;
		try {
			adminDTO = adminService.findByLogin(login);
		} catch (DaoException e) {
			response.put("Status", "ERROR");
			response.put("Error", "Hash validation unsuccessful B.");
			return response.toString();
		}
		if (!Integer.toHexString(createAuthenticatedUserHash(login, timestamp, adminDTO)).equals(userHash)) {
			response.put("Status", "ERROR");
			response.put("Error", "Hash validation unsuccessful C.");
			return response.toString();
		}
		response.put("Status", "OK");
		response.put("Message", "Hash valid.");
		System.out.println(response.toString());
		return response.toString();
	}
}
