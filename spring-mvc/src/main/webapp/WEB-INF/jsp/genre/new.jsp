<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <meta charset="utf-8">
</head>
<my:pagetemplate title="${title}">
<jsp:attribute name="body">
    <c:if test="${isAdmin == 2}">
     <form:form method="post" action="${pageContext.request.contextPath}/genre/create"
                modelAttribute="genreCreate" cssClass="form-horizontal">


                <div class="form-group ${name_error?'has-error':''}">
                    <form:label path="name" cssClass="col-sm-2 control-label">Name</form:label>
                    <div class="col-sm-10">
                        <form:input path="name" cssClass="form-control"/>
                        <form:errors path="name" cssClass="help-block"/>
                    </div>
                </div>

                <div class="form-group ${description_error?'has-error':''}">
                    <form:label path="description" cssClass="col-sm-2 control-label">Description</form:label>
                    <div class="col-sm-10">
                        <form:input path="description" cssClass="form-control"/>
                        <form:errors path="description" cssClass="help-block"/>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Create genre</button>
            </form:form>

        <form:form method="get" action="${pageContext.request.contextPath}/genre/list"
                       cssClass="form-horizontal">

            <button class="btn btn-primary" type="submit">Back</button>

    </form:form>
    </c:if>

</jsp:attribute>
</my:pagetemplate>
