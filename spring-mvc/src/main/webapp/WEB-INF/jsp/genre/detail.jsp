<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<my:pagetemplate title="${genre.name}">
<jsp:attribute name="body">

    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><c:out value="${genre.name}"/></td>
            <td><c:out value="${genre.description}"/></td>
        </tr>
        </tbody>
    </table>

    <form:form method="get" action="${pageContext.request.contextPath}/genre/list"
               cssClass="form-horizontal">

        <button class="btn btn-primary" type="submit">Back</button>

    </form:form>

</jsp:attribute>
</my:pagetemplate>
