<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<my:pagetemplate title="Error - Another genre with this name already exists!">
<jsp:attribute name="body">

    <form:form method="get" action="${pageContext.request.contextPath}/genre/edit/${genreError.id}"
               cssClass="form-horizontal">

        <button class="btn btn-primary" type="submit">Back</button>

    </form:form>

</jsp:attribute>
</my:pagetemplate>
