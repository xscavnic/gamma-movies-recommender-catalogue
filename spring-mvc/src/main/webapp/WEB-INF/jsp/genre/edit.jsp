<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<my:pagetemplate title="Edit genre">
<jsp:attribute name="body">
    <c:if test="${isAdmin == 2}">
 <form:form method="post" action="${pageContext.request.contextPath}/genre/edit"
            modelAttribute="genreEdit" cssClass="form-horizontal">

       <div class="form-group ${id_error?'has-error':''}">
           <form:label path="id" cssClass="col-sm-2 control-label">Id</form:label>
           <div class="col-sm-10">
               <form:input path="id" readonly="true" cssClass="form-control"/>
               <form:errors path="id" cssClass="help-block"/>
           </div>
       </div>


    <div class="form-group ${name_error?'has-error':''}">
        <form:label path="Name" cssClass="col-sm-2 control-label">Title</form:label>
        <div class="col-sm-10">
            <form:input path="name" cssClass="form-control"/>
            <form:errors path="name" cssClass="help-block"/>
        </div>
    </div>
    <div class="form-group ${description_error?'has-error':''}">
        <form:label path="Description" cssClass="col-sm-2 control-label">Description</form:label>
        <div class="col-sm-10">
            <form:textarea cols="80" rows="20" path="description" cssClass="form-control"/>
            <form:errors path="description" cssClass="help-block"/>
        </div>
    </div>
    <button class="btn btn-primary" type="submit">Edit genre</button>
</form:form>

<form:form method="get" action="${pageContext.request.contextPath}/genre/list"
           cssClass="form-horizontal">

    <button class="btn btn-primary" type="submit">Back</button>

</form:form>
    </c:if>

</jsp:attribute>
</my:pagetemplate>
