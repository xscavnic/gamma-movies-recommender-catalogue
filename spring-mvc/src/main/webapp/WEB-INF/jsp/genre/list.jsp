<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<my:pagetemplate title="Genres">
<jsp:attribute name="body">

    <c:if test="${isAdmin == 2}">
        <my:a href="/genre/new" class="btn btn-primary">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            New genre
        </my:a>
     </c:if>

    <table class="table">
        <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${genres}" var="genre">
            <tr>
                <td><c:out value="${genre.name}"/></td>
                <td><c:out value="${genre.description}"/></td>
                <td><my:a href="/genre/detail/${genre.id}" class="btn btn-primary">View</my:a></td>
                <c:if test="${isAdmin == 2}">
                    <td><my:a href="/genre/edit/${genre.id}" class="btn btn-secondary">Edit</my:a></td>
                </c:if>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</jsp:attribute>
</my:pagetemplate>
