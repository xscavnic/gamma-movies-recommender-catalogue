<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<my:pagetemplate title="Recommended by Ratings">
<jsp:attribute name="body">

    <c:if test="${isAdmin > 0}">
    <table class="table">
      <thead>
      <tr>
        <th>Title</th>
        <th>Description</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${movies}" var="movie">
            <tr>
              <td><c:out value="${movie.title}"/></td>
              <td><c:out value="${movie.description}"/></td>
              <td><my:a href="/recommender/byRatings/${movie.id}" class="btn btn-primary">Recommend Similar</my:a></td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    </c:if>

</jsp:attribute>
</my:pagetemplate>
