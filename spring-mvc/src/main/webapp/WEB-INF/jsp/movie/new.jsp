<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <meta charset="utf-8">
</head>
<my:pagetemplate title="Create Movie">
<jsp:attribute name="body">
    <c:if test="${isAdmin == 2}">

        <form:form method="post" action="${pageContext.request.contextPath}/movie/create"
                   modelAttribute="movieCreateDto" cssClass="form-horizontal" >

        <div class="form-group ${title_error?'has-error':''}">
            <form:label path="title" cssClass="col-sm-2 control-label">Title (required)</form:label>
            <div class="col-sm-10">
                <form:input path="title" cssClass="form-control" required="required"/>
                <form:errors path="title" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${director_error?'has-error':''}">
            <form:label path="director" cssClass="col-sm-2 control-label">Director (required)</form:label>
            <div class="col-sm-10">
                <form:input path="director" cssClass="form-control" required="required"/>
                <form:errors path="director" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${actors_error?'has-error':''}">
            <div class="col-sm-2 control-label">
                <label>Actors</label>
            </div>
            <div class="col-sm-10">
                <textarea name="actorsString" cols="80" rows="5" class="form-control"></textarea>
            </div>
        </div>
        <div class="form-group ${description_error?'has-error':''}">
            <form:label path="description" cssClass="col-sm-2 control-label">Description</form:label>
            <div class="col-sm-10">
                <form:textarea cols="80" rows="10" path="description" cssClass="form-control"/>
                <form:errors path="description" cssClass="help-block"/>
            </div>
        </div>

        <div class="form-group ${genres_error?'has-error':''}">
            <div class="col-sm-2 control-label">
                <label>Genres</label>
            </div>
            <div class="col-sm-10">
                <c:forEach items="${allGenres}" var="genre">
                    <div class="row">
                        <label><input type="checkbox" name="movieGenres" value="${genre.id}"/> ${genre.name}</label>
                    </div>
                </c:forEach>
            </div>
        </div>
            <%--
                    <div class="form-group ${file_error?'has-error':''}">
                        <div class="col-sm-2 control-label">
                            <label>Image</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" name="file" required/>
                        </div>
                    </div>
            --%>
            <div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>

        </form:form>
    </c:if>


</jsp:attribute>
</my:pagetemplate>
