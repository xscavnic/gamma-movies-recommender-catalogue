<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <meta charset="utf-8">
</head>
<my:pagetemplate title="${movie.title}">
<jsp:attribute name="body">

    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Director</th>
            <th>Genres</th>
            <th>Actors</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
    <tr>
        <td><c:out value="${movie.id}"/></td>
        <td><c:out value="${movie.title}"/></td>
        <td><c:out value="${movie.director}"/></td>
        <td>
            <c:forEach items="${movie.genres}" var="genres">
            <c:out value="${genres.name}, "/>
            </c:forEach>
        </td>
        <td><c:out value="${movie.actors}"/></td>
        <td><c:out value="${movie.description}"/></td>
        <c:if test="${isAdmin > 0}">
            <td>
                <my:a href="/rating/new/${movie.id}" class="btn btn-primary">Rate movie</my:a>
            </td>
        </c:if>
    </tr>
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <img class="img-responsive img-rounded"
                         src="${pageContext.request.contextPath}/movie/movieImage/${movie.id}">
                </div>
            </div>
        </div>
    </div>


</jsp:attribute>
</my:pagetemplate>
