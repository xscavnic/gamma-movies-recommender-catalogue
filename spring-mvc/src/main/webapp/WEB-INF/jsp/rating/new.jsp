<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<my:pagetemplate title="Rate movie">
<jsp:attribute name="body">

        <form:form method="post" action="${pageContext.request.contextPath}/rating/create"
                   modelAttribute="ratingCreateDTO" cssClass="form-horizontal">

             <div class="form-group ${movie_id_error?'has-error':''}" hidden>
                 <form:label path="id" cssClass="col-sm-2 control-label">Id</form:label>
                 <div class="col-sm-10">
                     <form:input path="id" hidden="true" cssClass="form-control"/>
                     <form:errors path="id" cssClass="help-block"/>
                 </div>
             </div>

            <div class="form-group ${id_error?'has-error':''}" hidden>
                <form:label path="person" cssClass="col-sm-2 control-label">Person</form:label>
                <div class="col-sm-10">
                    <form:input path="person" readonly="true" cssClass="form-control"/>
                    <form:errors path="person" cssClass="help-block"/>
                </div>
            </div>

             <div class="form-group ${id_error?'has-error':''}">
                 <form:label path="movie" cssClass="col-sm-2 control-label">Movie</form:label>
                 <div class="col-sm-10">
                     <form:input path="movie.title" readonly="true" cssClass="form-control"/>
                     <form:errors path="movie.title" cssClass="help-block"/>
                 </div>
             </div>

             <div class="form-group ${description_error?'has-error':''}">
                 <form:label path="ratingDescription" cssClass="col-sm-2 control-label">Your rating</form:label>
                 <div class="col-sm-10">
                     <form:textarea cols="80" rows="20" path="ratingDescription" cssClass="form-control"/>
                     <form:errors path="ratingDescription" cssClass="help-block"/>
                 </div>
             </div>

            <div class="form-group ${score_error?'has-error':''}" >
                <form:label path="totalStars" cssClass="col-sm-2 control-label">Total stars</form:label>
                <div class="col-sm-10">
                    <form:input path="totalStars" cssClass="form-control" form:required="true" form:type="number" form:min="0" form:max="5" />
                    <form:errors path="totalStars" cssClass="help-block"/>
                </div>
            </div>

            <div class="form-group ${time_added_error?'has-error':''}" hidden>
                <form:label path="timeAdded" cssClass="col-sm-2 control-label">Time added</form:label>
                <div class="col-sm-10">
                    <form:input path="timeAdded" cssClass="form-control"/>
                    <form:errors path="timeAdded" cssClass="help-block"/>
                </div>
            </div>

            <div class="form-group ${reported_error?'has-error':''}" hidden>
                <form:label path="reportedRating" cssClass="col-sm-2 control-label">Reported rating</form:label>
                <div class="col-sm-10">
                    <form:input path="reportedRating" cssClass="form-control"/>
                    <form:errors path="reportedRating" cssClass="help-block"/>
                </div>
            </div>

            <button class="btn btn-primary" type="submit" style="float:right;">Submit rating</button>
        </form:form>


</jsp:attribute>
</my:pagetemplate>