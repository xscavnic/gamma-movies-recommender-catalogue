<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<my:pagetemplate title="Ratings">
<jsp:attribute name="body">

    <table class="table">
        <thead>
        <tr>
            <th>Movie</th>
            <th>Rating description</th>
            <th>User</th>
            <th>Total stars</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${ratings}" var="rating">
            <tr>
                <td><c:out value="${rating.movie.title}"/></td>
                <td><c:out value="${rating.ratingDescription}"/></td>
                <td><c:out value="${rating.person.firstName} ${rating.person.lastName}"/></td>
                <td><c:out value="${rating.totalStars}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</jsp:attribute>
</my:pagetemplate>
