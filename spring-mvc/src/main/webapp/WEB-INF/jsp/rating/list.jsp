<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <meta charset="utf-8">
</head>
<my:pagetemplate title="${title}">
<jsp:attribute name="body">
    <h2>${movie.title}</h2>
    <div class="row">
        <div class="col-xs-3">
            <div class="panel-body">
                <img class="img-responsive img-rounded"
                     src="${pageContext.request.contextPath}/movie/movieImage/${movie.id}">
            </div>
        </div>
    </div>
    <my:a href="/movie/detail/${movie.id}" class="btn btn-primary">View Movie</my:a>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Criterion</th>
            <th scope="col">Score</th>
            <th scope="col">Author</th>
            <th scope="col">Description</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${reviewList}" var="review">
                <tr>
                    <td><c:out value="${review.criterion}"/></td>
                    <td><c:out value="${review.score}/10"/></td>
                    <td><c:out value="${userLogins.get(review.userId)}"/></td>
                    <td><c:out value="${review.description}"/></td>
                    <td><c:if test="${authenticatedUser.getId() == review.userId || authenticatedUser.isAdmin()}">
                            <my:a href="/review/delete/${review.id}" class="btn btn-danger">Delete</my:a>
                            <my:a href="/review/edit/${review.id}" class="btn btn-secondary">Edit</my:a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</jsp:attribute>
</my:pagetemplate>

