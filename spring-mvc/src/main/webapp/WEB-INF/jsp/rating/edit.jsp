<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="false" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <meta charset="utf-8">
</head>
<my:pagetemplate title="${title}">
<jsp:attribute name="body">
    <h2>${movieDto.title}</h2>
    <div class="row">
        <div class="col-xs-3">
            <div class="panel-body">
                <img class="img-responsive img-rounded"
                     src="${pageContext.request.contextPath}/movie/movieImage/${movieDto.id}">
            </div>
        </div>
        <div class="col-xs-6">
    <form:form method="post" action="${pageContext.request.contextPath}/review/edit"
               modelAttribute="reviewDto" cssClass="form-horizontal">
         <div class="form-group ${id_error?'has-error':''}" hidden>
             <form:label path="id" cssClass="col-sm-2 control-label">Id</form:label>
             <div class="col-sm-10">
                 <form:input path="id" readonly="true" cssClass="form-control"/>
                 <form:errors path="id" cssClass="help-block"/>
             </div>
         </div>
        <div class="form-group ${criterion_error?'has-error':''}">
            <form:label path="criterion" cssClass="col-sm-2 control-label">Criterion</form:label>
            <div class="col-sm-10">
                <form:input path="criterion" cssClass="form-control"  readonly="true" />
                <form:errors path="criterion" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${score_error?'has-error':''}" >
            <form:label path="score" cssClass="col-sm-2 control-label">Score</form:label>
            <div class="col-sm-10">
                <form:input path="score" cssClass="form-control" required="true" form:type="number" form:min="0" form:max="10" />
                <form:errors path="score" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${movie_error?'has-error':''}" hidden>
            <form:label path="movieId"   cssClass="col-sm-2 control-label">Movie</form:label>
            <div class="col-sm-10">
                <form:input path="movieId"   cssClass="form-control"/>
                <form:errors path="movieId"  cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${user_error?'has-error':''}" hidden>
            <form:label path="userId"  hidden="true" cssClass="col-sm-2 control-label">UserId</form:label>
            <div class="col-sm-10">
                <form:input path="userId"  cssClass="form-control"/>
                <form:errors path="userId" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${description_error?'has-error':''}">
            <form:label path="description" cssClass="col-sm-2 control-label">Description</form:label>
            <div class="col-sm-10">
                <form:textarea cols="80" rows="20" path="description" cssClass="form-control" form:value="reviewDto.description"/>
                <form:errors path="description" cssClass="help-block"/>
            </div>
        </div>
        <button class="btn btn-primary" type="submit" style="float: right;">Edit review</button>
    </form:form>
        </div>
    </div>
    <%--@elvariable id="reviewDto" type="cz.muni.fi.pa165.dto.ReviewDto"--%>

</jsp:attribute>
</my:pagetemplate>
