<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<my:pagetemplate title="Register">
<jsp:attribute name="body">
    <form:form method="post" action="${pageContext.request.contextPath}/user/create"
               modelAttribute="newPerson" cssClass="form-horizontal">
        <div class="form-group ${login_error?'has-error':''}" >
            <form:label path="login" cssClass="col-sm-2 control-label">Login</form:label>
            <div class="col-sm-10">
                <form:input path="login" cssClass="form-control"/>
                <form:errors path="login" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${password_error?'has-error':''}" >
            <form:label path="password" cssClass="col-sm-2 control-label">Password</form:label>
            <div class="col-sm-10">
                <form:input path="password" cssClass="form-control"/>
                <form:errors path="password" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${firstName_error?'has-error':''}" >
            <form:label path="firstName" cssClass="col-sm-2 control-label">First name</form:label>
            <div class="col-sm-10">
                <form:input path="firstName" cssClass="form-control"/>
                <form:errors path="firstName" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${lastName_error?'has-error':''}" >
            <form:label path="lastName" cssClass="col-sm-2 control-label">Last name</form:label>
            <div class="col-sm-10">
                <form:input path="lastName" cssClass="form-control"/>
                <form:errors path="lastName" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${country_error?'has-error':''}" >
            <form:label path="country" cssClass="col-sm-2 control-label">Country</form:label>
            <div class="col-sm-10">
                <form:input path="country" cssClass="form-control"/>
                <form:errors path="country" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${birthDate_error?'has-error':''}" >
            <form:label path="birthDate" cssClass="col-sm-2 control-label">Birth date (yyyy-mm-dd)</form:label>
            <div class="col-sm-10">
                <form:input path="birthDate" cssClass="form-control"/>
                <form:errors path="birthDate" cssClass="help-block"/>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Register</button>
    </form:form>
</jsp:attribute>
</my:pagetemplate>