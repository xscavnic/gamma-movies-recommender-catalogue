<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<my:pagetemplate title="Change password">
<jsp:attribute name="body">
    <form:form method="post" action="${pageContext.request.contextPath}/user/chpasswd/save"
          modelAttribute="passwordChange" cssClass="form-horizontal">
        <div class="form-group ${login_error?'has-error':''}" >
            <form:label path="login" cssClass="col-sm-2 control-label">Login</form:label>
            <div class="col-sm-10">
                <form:input path="login" cssClass="form-control"/>
                <form:errors path="login" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${old_password_error?'has-error':''}" >
            <form:label path="oldPassword" cssClass="col-sm-2 control-label">Old password</form:label>
            <div class="col-sm-10">
                <form:input path="oldPassword" cssClass="form-control"/>
                <form:errors path="oldPassword" cssClass="help-block"/>
            </div>
        </div>
        <div class="form-group ${new_password_error?'has-error':''}" >
            <form:label path="newPassword" cssClass="col-sm-2 control-label">New password</form:label>
            <div class="col-sm-10">
                <form:input path="newPassword" cssClass="form-control"/>
                <form:errors path="newPassword" cssClass="help-block"/>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Change password</button>
    </form:form>
</jsp:attribute>
</my:pagetemplate>
