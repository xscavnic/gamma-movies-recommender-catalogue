package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Rating;
import java.util.List;

/**
 * RatingDAO class
 *
 * @author Šárka Ščavnická
 */
public interface RatingDAO {

    /**
     * Creates a rating passed from parameters.
     *
     * @param r the rating to be created
     */
    void create(Rating r);

    /**
     * Finding all ratings.
     *
     * @return all ratings
     */
    List<Rating> findAll();

    /**
     * Finds a rating by id which was set as a parameter.
     *
     * @param id the id of the searched rating
     * @return rating with passed id from parameters
     */
    Rating findById(Long id);

    /**
     * Removes existing rating which is set in the parameters.
     *
     * @param r the rating to be removed
     */
    void remove(Rating r);

    /**
     * Get list of ratings from one user who is specified in a parameter.
     *
     * @param userId user id of the user in whose ratings are searched
     * @return list of ratings which have same user
     */
    List<Rating> getRatingByUser(Long userId);

    /**
     * Returns all rating for the movie which is specified by parameter.
     *
     * @param movieId the movie selected for listing of ratings
     * @return all ratings assigned to the selected movie
     */
    List<Rating> findAllByMovie(Long movieId);

    /**
     * Update existing rating
     *
     * @param r the rating to be updated
     */
    void update(Rating r);
}
