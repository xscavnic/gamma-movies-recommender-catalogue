package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.User;

import java.util.List;

/**
 * User Data Access Object layer Interface (for both person and admin subclasses)
 *
 * @author Kristián Malák
 */
public interface UserDAO<U extends User> {
    /**
     * @param u User to persist in DB
     */
    void create(U u);

    /**
     * @param u User to update in the DB
     */
    void update(U u);

    /**
     * @param u User to delete from DB
     */
    void remove(U u);

    /**
     * @param id of User to find in DB (person or admin)
     * @return User with given ID
     */
    User findById(Long id);

    /**
     * @param login to find User by
     * @return User with given login
     */
    U findByLogin(String login);

    /**
     * @return all users of specific type (either all users or all admins) of the system
     */
    List<U> findAll();
}
