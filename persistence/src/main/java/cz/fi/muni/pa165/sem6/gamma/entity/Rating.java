package cz.fi.muni.pa165.sem6.gamma.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Rating entity class
 *
 * @author Šárka Ščavnická
 */
@Entity(name = "Rating")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String ratingDescription;

    @Column(nullable = false)
    @Min(0)
    @Max(5)
    private Integer totalStars;

    @Column(nullable = false)
    private LocalDate timeAdded;

    @Column(nullable = false)
    private Boolean reportedRating;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRatingDescription() {
        return ratingDescription;
    }

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }

    public Integer getTotalStars() {
        return totalStars;
    }

    public void setTotalStars(Integer totalStars) {
        this.totalStars = totalStars;
    }

    public LocalDate getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(LocalDate timeAdded) {
        this.timeAdded = timeAdded;
    }

    public Boolean getReportedRating() {
        return reportedRating;
    }

    public void setReportedRating(Boolean reportedRating) {
        this.reportedRating = reportedRating;
    }

    @ManyToOne
    @NotNull
    private Movie movie;

    @ManyToOne
    @NotNull
    private Person person;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie m) {
        this.movie = m;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person p) {
        this.person = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rating)) return false;

        Rating r = (Rating) o;

        if (getTotalStars() != null ? !getTotalStars().equals(r.getTotalStars()) : r.getTotalStars() != null)
            return false;
        if (getTimeAdded() != null ? !getTimeAdded().equals(r.getTimeAdded()) : r.getTimeAdded() != null)
            return false;
        if (getMovie() != null ? !getMovie().equals(r.getMovie()) : r.getMovie() != null) return false;
        return getPerson() != null ? getPerson().equals(r.getPerson()) : r.getPerson() == null;
    }

    @Override
    public int hashCode() {
        int result = getTotalStars() != null ? getTotalStars().hashCode() : 0;
        result = 31 * result + (getTimeAdded() != null ? getTimeAdded().hashCode() : 0);
        result = 31 * result + (getMovie() != null ? getMovie().hashCode() : 0);
        result = 31 * result + (getPerson() != null ? getPerson().hashCode() : 0);
        return result;
    }
}
