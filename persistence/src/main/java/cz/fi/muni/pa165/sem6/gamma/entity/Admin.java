package cz.fi.muni.pa165.sem6.gamma.entity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the admin of the system, responsible for genres and movies.
 * There should be only one admin for the system.
 *
 * @author Kristián Malák
 */
@Entity
public class Admin extends User {
    @OneToMany
    @NotNull
    private List<Genre> genres;

    public Admin(String login, String password) {
        super(login, password);
        this.genres = new ArrayList<>();
    }

    public Admin(Long id, String login, String password, List<Genre> genres) {
        super(id, login, password);
        this.genres = genres;
    }

    public Admin() {
        this.genres = new ArrayList<>();
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Admin && super.equals(o);
    }
}
