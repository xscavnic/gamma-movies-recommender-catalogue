package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.User;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

/**
 * User Data Access Object layer Implementation
 *
 * @author Kristián Malák
 */
@NoRepositoryBean
@Transactional
public abstract class UserDAOImpl<U extends User> implements UserDAO<U> {

    @PersistenceContext
    protected EntityManager em;

    @Override
    public void create(@NotNull U u) {
        try {
            em.persist(u);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to create a new User", e);
        }
    }

    @Override
    public void update(@NotNull U u) {
        try {
            em.merge(u);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to update a User", e);
        }
    }

    @Override
    public void remove(@NotNull U u) {
        try {
            em.remove(em.contains(u) ? u : em.merge(u));
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to remove a User", e);
        }
    }
}
