package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Genre Data Access Object implementation
 *
 * @author - Jakub Kuchár, 484954
 **/
@Repository
@Transactional
public class GenreDAOImpl implements GenreDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Genre g) {
        try {
            em.persist(g);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to create a new Genre", e);
        }
    }

    @Override
    public Genre findById(Long id) {
        try {
            return em.find(Genre.class, id);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Genre by id", e);
        }
    }

    @Override
    public Genre findByName(String name) {
        try {
            return em.createQuery("select g from Genre g where g.name = :name", Genre.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Genre by name", e);
        }
    }

    @Override
    public List<Genre> findAll() {
        try {
            return em.createQuery("select g from Genre g", Genre.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find all Genres", e);
        }
    }

    @Override
    public void update(Genre g) {
        try {
            em.merge(g);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to update Genre", e);
        }
    }

    @Override
    public void remove(Genre g) {
        try {
            em.remove(em.contains(g) ? g : em.merge(g));
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to remove Genre", e);
        }
    }
}
