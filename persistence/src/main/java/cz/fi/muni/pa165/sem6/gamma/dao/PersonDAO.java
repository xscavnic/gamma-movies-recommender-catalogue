package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Person;

/**
 * User Data Access Object layer Interface for Person entity
 *
 * @author Kristián Malák
 */
public interface PersonDAO extends UserDAO<Person> {
}
