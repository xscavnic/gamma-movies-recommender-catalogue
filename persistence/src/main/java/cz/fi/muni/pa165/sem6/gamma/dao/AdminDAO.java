package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Admin;

/**
 * User Data Access Object layer Interface for Admin entity
 *
 * @author Kristián Malák
 */
public interface AdminDAO extends UserDAO<Admin> {
}
