package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Rating;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * RatingDAOImpl class
 *
 * @author Šárka Ščavnická
 */
@Repository
@Transactional
public class RatingDAOImpl implements RatingDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Rating r) {
        try {
            em.persist(r);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to create a new Rating", e);
        }
    }

    @Override
    public List<Rating> findAll() {
        try {
            return em.createQuery("select r from Rating r", Rating.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find all Ratings", e);
        }
    }

    @Override
    public Rating findById(Long id) {
        try {
            return em.find(Rating.class, id);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Rating by id", e);
        }
    }

    @Override
    public void remove(Rating r) {
        try {
            em.remove(em.contains(r) ? r : em.merge(r));
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to remove a Rating", e);
        }
    }

    @Override
    public List<Rating> getRatingByUser(Long userId) {
        try {
            return em.createQuery("select r from Rating r join r.person p where p.id in :r", Rating.class)
                    .setParameter("r", userId)
                    .getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Rating by User", e);
        }
    }

    @Override
    public List<Rating> findAllByMovie(Long movieId) {
        try {
            return em.createQuery("select r from Rating r join r.movie m where m.id in :r", Rating.class)
                    .setParameter("r", movieId)
                    .getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find all Ratings by Movie", e);
        }
    }

    @Override
    public void update(Rating r) {
        try {
            em.merge(r);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to update a Rating", e);
        }
    }
}
