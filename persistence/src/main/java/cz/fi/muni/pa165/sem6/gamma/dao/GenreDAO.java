package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;

import java.util.List;

/**
 * Genre Data Access Object layer Interface
 *
 * @author - Jakub Kuchár, 484954
 **/
public interface GenreDAO {

    /**
     * Persists passed object to database
     *
     * @param g - object to be persisted
     */
    void create(Genre g);

    /**
     * Finds and returns the object from the database which has id equal to the id parameter
     * @param id - id to be found in the table
     * @return - object from the table, which has id equal to the parameter
     */
    Genre findById(Long id);

    /**
     * Finds and returns the object from the database which has name equal to the name parameter
     * @param name - name to be found in the table
     * @return - object from the table, which has name equal to the parameter
     */
    Genre findByName(String name);

    /**
     * Finds all entries in the table.
     * @return - all entries in the table
     */
    List<Genre> findAll();

    /**
     * Updates object in the table in the database.
     * @param g - updated object.
     */
    void update(Genre g);

    /**
     * Deletes object from the table
     * @param g - object to be deleted
     */
    void remove(Genre g);
}
