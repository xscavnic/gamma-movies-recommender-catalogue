package cz.fi.muni.pa165.sem6.gamma.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Class representing a parent/superclass for all users of the system.
 *
 * @author Kristián Malák
 */
@MappedSuperclass
public abstract class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true, updatable = false, nullable = false)
    private String login;

    @NotNull
    private String password;

    protected User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    protected User(Long id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    protected User() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        if (this.login != null)
            throw new IllegalStateException("Cannot change login of user, a login is already set.");
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        return 31 + (getLogin() != null ? getLogin().hashCode() : 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (!(o instanceof User))
            return false;
        User u = (User) o;
        if (getLogin() == null) {
            return u.getLogin() == null;
        }
        return getLogin().equals(u.getLogin());
    }
}