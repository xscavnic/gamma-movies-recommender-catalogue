package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * MovieDAO interface
 *
 * @author Pavol Bock, 506490
 */

public interface MovieDAO {

    /**
     * Creates a new movie which has parameter as entity movie.
     *
     * @param m to be created
     */
    void create(Movie m);

    /**
     * Update existing movie with movie in parameter.
     *
     * @param m to be updated
     */
    void update(Movie m);

    /**
     * Delete existing movie which is set in parameter.
     *
     * @param m to be deleted
     */
    void remove(Movie m);

    /**
     * Find a movie with id which is set in parameter.
     *
     * @param id of movie to be find
     * @return movie with id set in parameter
     */
    Movie findById(Long id);

    /**
     * Show all existing movies.
     *
     * @return list of movies
     */
    List<Movie> findAll();

    /**
     * Get average rating of specific movie which is set in parameter.
     *
     * @param m to get average rating
     * @return average rating of the movie
     */
    Double getRating(Movie m);

    /**
     * Get list of moviesIDs with their average rating.
     *
     * @return map of long and double where long is id of movie and double is average number of stars.
     */
    Map<Long, Double> getMoviesRatings();

    /**
     * Get list of movies which has same genres based in parameter.
     *
     * @param genreIds set of genres ids to must have movies in the list
     * @return set of movies which has same genres
     */
    Set<Movie> getMoviesByGenres(Set<Long> genreIds);

    /**
     * Get list of movies with same genre.
     *
     * @return list of recommended movies
     */
    List<Movie> getSimilarByGenre(Movie movie);

    /**
     * Get list of movies with the biggest rating.
     *
     * @return list of recommended movies
     */
    List<Movie> getSimilarByRatings(Movie movie);

}
