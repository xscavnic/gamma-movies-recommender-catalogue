package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import cz.fi.muni.pa165.sem6.gamma.entity.User;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Person Data Access Object layer Implementation
 *
 * @author Kristián Malák
 */
@Repository
@Transactional
public class PersonDAOImpl extends UserDAOImpl<Person> implements PersonDAO {

    @Override
    public List<Person> findAll() {
        try {
            return em.createQuery("select p from Person p", Person.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find all Persons", e);
        }
    }

    @Override
    public Person findByLogin(String login) {
        try {
            return em.createQuery("select p from Person p where p.login=:login", Person.class).setParameter("login", login).getSingleResult();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Person by login", e);
        }
    }

    @Override
    public User findById(Long id) {
        try {
            return em.find(Person.class, id);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Person by id", e);
        }

    }
}
