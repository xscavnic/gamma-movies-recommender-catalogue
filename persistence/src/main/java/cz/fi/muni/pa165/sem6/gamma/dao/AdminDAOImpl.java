package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Admin;
import cz.fi.muni.pa165.sem6.gamma.entity.User;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Admin Data Access Object layer Implementation
 *
 * @author Kristián Malák
 */
@Repository
@Transactional
public class AdminDAOImpl extends UserDAOImpl<Admin> implements AdminDAO {
    @Override
    public List<Admin> findAll() {
        try {
            return em.createQuery("select a from Admin a", Admin.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find all Admins", e);
        }
    }

    @Override
    public Admin findByLogin(String login) {
        try {
            return em.createQuery("select a from Admin a where a.login=:login", Admin.class).setParameter("login", login).getSingleResult();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Admin by login", e);
        }
    }

    @Override
    public User findById(Long id) {
        try {
            return em.find(Admin.class, id);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find Admin by id", e);
        }
    }
}
