package cz.fi.muni.pa165.sem6.gamma.dao;
import javax.persistence.Tuple;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cz.fi.muni.pa165.sem6.gamma.entity.Movie;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * MovieDAOImpl
 *
 * @author Pavol Bock, 506490
 */
@Repository
@Transactional
public class MovieDAOImpl implements MovieDAO {

    @PersistenceContext
    private EntityManager EntityManager;

    @Override
    public void create(Movie movie) {
        try {
            EntityManager.persist(movie);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to create a new Movie", e);
        }
    }

    @Override
    public void update(@NotNull Movie m) {
        try {
            EntityManager.merge(m);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to update a Movie", e);
        }
    }

    @Override
    public void remove(@NotNull Movie m) {
        try {
            EntityManager.remove(EntityManager.contains(m) ? m : EntityManager.merge(m));
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to remove a Movie", e);
        }
    }

    @Override
    public Movie findById(Long id) {
        try {
            return EntityManager.find(Movie.class, id);
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find a Movie by id", e);
        }
    }

    @Override
    public List<Movie> findAll() {
        try {
            return EntityManager.createQuery("select m from Movie m", Movie.class).getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to find all Movies", e);
        }
    }

    @Override
    public Double getRating(Movie m) {
        try {
            return EntityManager.createQuery("select avg(r.totalStars) from Movie m, Rating r where m.id = :movie and r.movie = m ",
                    Double.class).setParameter("movie", m.getId()).getSingleResult();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to get the rating of a Movie", e);
        }
    }

    @Override
    public Map<Long, Double> getMoviesRatings() {
        try {
            List<Tuple> result = EntityManager.createQuery(
                    "select m.id as movieID ,avg(r.totalStars) as avgTotalStar from Movie m, Rating r where r.movie = m "+ "group by m.id order by avg(r.totalStars) desc",
                    Tuple.class
            ).getResultList();
            return result.stream().collect(Collectors.toMap(tuple -> ((Number) tuple.get("movieID")).longValue(), tuple -> ((Number) tuple.get("avgTotalStar")).doubleValue()));
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to get the ratings of a Movie", e);
        }
    }

    @Override
    public Set<Movie> getMoviesByGenres(Set<Long> GenreIds) {
        try {
            List <Movie> listMovies = EntityManager.createQuery(
                    "select m from Movie m join m.genres Genre where Genre.id in :genre",
                    Movie.class
            ).setParameter("genre", GenreIds).getResultList();
            return listMovies.stream().collect(Collectors.toSet());
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to get Movies by Genres", e);
        }
    }

    @Override
    public List<Movie> getSimilarByGenre(Movie movie) {
        try {
            Set<Genre> genres = movie.getGenres();
            return EntityManager.createQuery(
                    "select distinct m from Movie m join m.genres Genre where Genre in :genres "+ "order by m.id asc",
                    Movie.class
            ).setParameter("genres",genres).getResultList();
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to get similar Movies by Genres", e);
        }
    }


    @Override
    public List<Movie> getSimilarByRatings(Movie movie) {
        try {
            Double avg = getRating(movie);
            Map<Long, Double> resultAvg = getMoviesRatings();
            List<Movie> result = new ArrayList<>();
            for (Long key: resultAvg.keySet()){
                if (key != movie.getId()) {
                    if (resultAvg.get(key) >= avg) {
                        result.add(findById(key));
                    }
                }
            }
            return result;
        } catch (Exception e) {
            throw new DaoException("Exception occurred while trying to get similar Movies by Ratings", e);
        }
    }
}
