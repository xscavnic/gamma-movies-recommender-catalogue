package cz.fi.muni.pa165.sem6.gamma.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a regular of the system.
 *
 * @author Kristián Malák
 */
@Entity
public class Person extends User {
    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String country;

    @NotNull
    private LocalDate birthDate;

    @NotNull
    private boolean blacklisted = false;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Rating> ratings;

    public Person(String login, String password, String firstName, String lastName, String country, LocalDate birthDate) {
        super(login, password);
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.birthDate = birthDate;
        this.ratings = new ArrayList<>();
    }

    public Person(Long id, String login, String password, String firstName, String lastName, String country, LocalDate birthDate, List<Rating> ratings) {
        super(id, login, password);
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.birthDate = birthDate;
        this.ratings = ratings;
    }

    public Person() {
        this.ratings = new ArrayList<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public boolean isBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(boolean blacklisted) {
        this.blacklisted = blacklisted;
    }

    @Override
    public int hashCode() {
        return super.hashCode() * 31 + getBirthDate().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Person && super.equals(o);
    }
}
