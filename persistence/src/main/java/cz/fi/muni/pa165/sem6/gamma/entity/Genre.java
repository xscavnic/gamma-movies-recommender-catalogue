package cz.fi.muni.pa165.sem6.gamma.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity Genre
 *
 * @author - Jakub Kuchár, 484954
 **/
@Entity(name = "Genre")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String description;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinColumn(name = "Movie_id")
    private Set<Movie> movieSet = new HashSet<>();

    public void addMovie(Movie m) {
        movieSet.add(m);
    }

    public Set<Movie> getMovies() {
        return Collections.unmodifiableSet(movieSet);
    }

    public Genre() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getID() {
        return this.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;

        Genre g = (Genre) o;

        return getName() != null ? getName().equals(g.getName()) : g.getName() == null;
    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }
}
