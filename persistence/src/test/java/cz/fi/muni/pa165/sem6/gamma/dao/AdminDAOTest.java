package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.PersistenceSampleApplicationContext;
import cz.fi.muni.pa165.sem6.gamma.entity.Admin;
import cz.fi.muni.pa165.sem6.gamma.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for AdminDAO class.
 *
 * @author Pavol Bock, Kristián Malák
 */
@DirtiesContext
@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
@Transactional
public class AdminDAOTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private AdminDAO adminDAO;

    private Admin janko;
    private Admin ignac;


    @BeforeClass
    public void setup() {
        janko = new Admin();
        janko.setLogin("Janko");
        janko.setPassword("12345");
        adminDAO.create(janko);

        ignac = new Admin("Igi", "5555");
        adminDAO.create(ignac);
    }

    @AfterClass
    public void afterClass() {
        for (Admin u : adminDAO.findAll()) {
            adminDAO.remove(u);
        }
    }

    @Test
    public void create() {
        LocalDate today = LocalDate.now();
        Admin marienka = new Admin("Marienka", "98765");
        adminDAO.create(marienka);

        assertThat(adminDAO.findById(marienka.getId())).isEqualTo(marienka);
    }

    @Test
    public void update() {
        janko.setPassword("SuperBossJanko");
        adminDAO.update(janko);

        assertThat(adminDAO.findById(janko.getId())).isEqualTo(janko);
        assertThat(adminDAO.findById(janko.getId()).getPassword()).isEqualTo("SuperBossJanko");
    }

    @Test
    public void findById() {
        Admin retJanko = adminDAO.findByLogin(janko.getLogin());
        assertThat(janko).isEqualTo(retJanko);
    }

    @Test
    public void findAll() {
        List<Admin> returnedUsers = adminDAO.findAll();

        assertThat(returnedUsers.size()).isEqualTo(3);
        assertThat(returnedUsers.contains(janko)).isTrue();
    }

    @Test
    public void remove() {
        Long ignacID = ignac.getId();
        adminDAO.remove(ignac);

        assertThat(adminDAO.findById(ignacID)).isNull();
    }

    @Test
    public void findByLogin() {
        User ignacLogin = adminDAO.findByLogin(ignac.getLogin());
        assertThat(ignac).isEqualTo(ignacLogin);
    }
}
