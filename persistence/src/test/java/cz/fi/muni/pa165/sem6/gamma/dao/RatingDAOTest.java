package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;
import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import cz.fi.muni.pa165.sem6.gamma.entity.Rating;
import cz.fi.muni.pa165.sem6.gamma.PersistenceSampleApplicationContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Tests for RatingDAO class.
 *
 * @author Jakub Kuchár
 */
@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
@Transactional
@TestExecutionListeners(TransactionalTestExecutionListener.class)
public class RatingDAOTest extends AbstractTestNGSpringContextTests {

    private final LocalDate now = LocalDate.now();

    private Movie m1;
    private Movie m2;
    private Person p1;
    private Person p2;
    private Rating r1;
    private Rating r2;
    private Rating r3;
    private Rating r4;
    private Rating r5;

    @Autowired
    public RatingDAO ratingDAO;

    @Autowired
    public PersonDAO personDAO;

    @Autowired
    public MovieDAO movieDAO;

    @Autowired
    public GenreDAO genreDAO;

    private Movie createMovie(String title, String director, Set<Genre> genres) {
        Movie res = new Movie();
        res.setDirector(director);
        res.setTitle(title);
        res.setGenres(genres);
        return res;
    }

    private Rating createRating(String desc, Integer stars, Movie m, Person p, LocalDate timeAdded) {
        Rating res = new Rating();
        res.setMovie(m);
        res.setPerson(p);
        res.setRatingDescription(desc);
        res.setTotalStars(stars);
        res.setTimeAdded(timeAdded);
        res.setReportedRating(false);
        return res;
    }

    private Genre createGenre(String name, String desc) {
        Genre res = new Genre();
        res.setName(name);
        res.setDescription(desc);
        return res;
    }

    private void prepareDB() {
        Genre g1 = createGenre("Test", "");
        Genre g2 = createGenre("Test2", "");

        genreDAO.create(g1);
        genreDAO.create(g2);

        Set<Genre> g = new HashSet<>();
        g.add(g1);
        g.add(g2);

        m1 = createMovie("Test1", "Test1", g);
        m2 = createMovie("Test2", "Test2", g);

        p1 = new Person("abc", "123", "jakub", "kuchar", "slovakia", now.minusYears(23));
        p2 = new Person("abcd", "123", "pavol", "bock", "slovakia", now.minusYears(22));

        movieDAO.create(m1);
        movieDAO.create(m2);
        personDAO.create(p1);
        personDAO.create(p2);

        r1 = createRating("abc", 3, m1, p1, now.minusYears(2));
        r2 = createRating("def", 2, m2, p1, now.minusYears(1));
        r3 = createRating("adasda", 5, m2, p1, now);
        r4 = createRating("safsadf", 1, m1, p2, now);
        r5 = createRating("asdsdsf", 4, m1, p2, now);

        ratingDAO.create(r1);
        ratingDAO.create(r2);
        ratingDAO.create(r3);
        ratingDAO.create(r4);
        ratingDAO.create(r5);
    }

    @Test
    @DirtiesContext
    public void create() {
        Genre g1 = createGenre("Test", "");
        genreDAO.create(g1);
        Set<Genre> gs = new HashSet<>();
        gs.add(g1);
        Movie m = createMovie("Test1", "Quentino Tarantino", gs);
        Person p = new Person("abcd", "123", "jakub", "kuchar", "slovakia", now.minusYears(22));

        personDAO.create(p);
        movieDAO.create(m);

        Rating rating = createRating("Testing rating", 3, m, p, now);

        ratingDAO.create(rating);

        Rating rating1 = ratingDAO.findById(rating.getId());

        assertThat(rating1).isNotNull();
        assertThat(rating1).isEqualTo(rating);
        assertThat(ratingDAO.findById(rating.getId() + 1)).isNull();
        assertThat(rating1.getRatingDescription()).isEqualTo("Testing rating");
        assertThat(rating1.getTotalStars()).isEqualTo(3);
        assertThat(rating1.getTimeAdded()).isEqualTo(now);
        assertThat(rating1.getMovie()).isEqualTo(m);
        assertThat(rating1.getPerson()).isEqualTo(p);
    }

    @Test
    @DirtiesContext
    public void findAll() {
        prepareDB();
        assertThat(ratingDAO.findAll().size()).isEqualTo(5);
    }

    @Test
    @DirtiesContext
    public void findById() {
        prepareDB();
        assertThat(ratingDAO.findById(r1.getId())).isEqualTo(r1);
        assertThat(ratingDAO.findById(r2.getId())).isEqualTo(r2);
    }

    @Test
    @DirtiesContext
    public void remove() {
        prepareDB();

        assertThat(ratingDAO.findById(r1.getId())).isNotNull();

        ratingDAO.remove(r1);
        assertThat(ratingDAO.findAll().size()).isEqualTo(4);
        assertThat(ratingDAO.findById(r1.getId())).isNull();
    }

    @Test
    @DirtiesContext
    public void getRatingByUserFindAllByMovie() {
        prepareDB();

        // Get Rating By User
        assertThat(ratingDAO.getRatingByUser(p1.getId())).size().isEqualTo(3);
        assertThat(ratingDAO.getRatingByUser(p2.getId())).size().isEqualTo(2);

        // Find All By Movie
        assertThat(ratingDAO.findAllByMovie(m1.getId())).size().isEqualTo(3);
        assertThat(ratingDAO.findAllByMovie(m2.getId())).size().isEqualTo(2);
    }

    @Test
    @DirtiesContext
    public void update() {
        prepareDB();

        r1.setReportedRating(true);
        ratingDAO.update(r1);
        assertThat(ratingDAO.findById(r1.getId())).isEqualTo(r1);
        assertThat(ratingDAO.findById(r1.getId()).getReportedRating()).isEqualTo(true);
    }
}
