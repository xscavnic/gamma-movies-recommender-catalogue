package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.PersistenceSampleApplicationContext;
import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Tests for GenreDAOImpl class
 *
 * @author Kristián Malák
 */
@DirtiesContext
@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
@Transactional
public class GenreDAOTest extends AbstractTestNGSpringContextTests {

	@PersistenceUnit
	private EntityManagerFactory emf;

	private Genre genre;
	private Genre genre1;
	private Genre genre2;

	@Autowired
	private GenreDAO genreDAO;

	@BeforeClass
	public void setup() {
		genre = new Genre();
		genre.setName("Test4");
		genre.setDescription("Test4 genre create");
		genreDAO.create(genre);

		genre1 = new Genre();
		genre1.setName("Test5");
		genre1.setDescription("Test5 genre create");
		genreDAO.create(genre1);

		genre2 = new Genre();
		genre2.setName("Test6");
		genre2.setDescription("Test6 genre create");
		genreDAO.create(genre2);
	}

	@AfterClass
	public void afterClass() {
		for (Genre g : genreDAO.findAll()) {
			genreDAO.remove(g);
		}
	}

	@Test
	public void create() {
		Genre genre = new Genre();
		genre.setName("Test1");
		genre.setDescription("test genre create");
		genreDAO.create(genre);

		assertThat(genreDAO.findById(genre.getID())).isEqualTo(genre);
		assertThat(genreDAO.findByName(genre.getName())).isEqualTo(genre);
	}

	@Test
	public void findById() {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Genre genre = new Genre();
		genre.setName("Test2");
		genre.setDescription("test genre find by id");
		em.persist(genre);
		em.getTransaction().commit();
		em.close();

		Genre found = genreDAO.findById(genre.getID());
		assertThat(found.getName()).isEqualTo("Test2");
		assertThat(found.getDescription()).isEqualTo(genre.getDescription());
		assertThat(genre.equals(found)).isTrue();
		assertThat(found.equals(genre)).isTrue();
	}

	@Test
	public void findByName() {
		Genre genre = new Genre();
		genre.setName("Test3");
		genre.setDescription("test genre find by name");
		genreDAO.create(genre);

		Genre found = genreDAO.findByName("Test3");
		assertThat(found.getName()).isEqualTo("Test3");
		assertThat(found.getDescription()).isEqualTo(genre.getDescription());
		assertThat(genre.equals(found)).isTrue();
		assertThat(found.equals(genre)).isTrue();
	}

	@Test(priority = -1)
	public void findAll() {
		List<Genre> genres = genreDAO.findAll();
		assertThat(genres).hasSize(3);
		assertThat(genres).map(Genre::getName).containsExactlyElementsOf(List.of("Test4", "Test5", "Test6"));
	}

	@Test()
	public void update() {
		assertThat(genreDAO.findById(genre1.getID()).getName()).isEqualTo("Test5");
		genre1.setName("TestUpdate");
		genre1.setDescription("testUpdate genre updated");
		genreDAO.update(genre1);
		assertThat(genreDAO.findById(genre1.getID())).isEqualTo(genre1);
		assertThat(genreDAO.findById(genre1.getID()).getName()).isEqualTo("TestUpdate");

		EntityManager em = emf.createEntityManager();
		Genre found = em.find(genre1.getClass(), genre1.getID());
		assertThat(found).isEqualTo(genre1);
	}

	@Test()
	public void remove() {
		assertThat(genreDAO.findById(genre.getID())).isNotNull();

		genreDAO.remove(genre);
		assertThat(genreDAO.findById(genre.getID())).isNull();

		EntityManager em = emf.createEntityManager();
		Genre found = em.find(genre.getClass(), genre.getID());
		assertThat(found).isNull();
	}

	@Test()
	public void tryTwoGenresWithSameName() {
		Genre genre1 = new Genre();
		genre1.setName("Test8");
		genre1.setDescription("test genre");
		genreDAO.create(genre1);
		assertThat(genreDAO.findById(genre1.getID())).isNotNull();
		Genre genre2 = new Genre();
		genre2.setName("Test8");
		genre2.setDescription("test genre duplicate");
		assertThatThrownBy(() -> genreDAO.create(genre2))
				.isInstanceOf(DaoException.class);
	}

	@Test
	public void tryGenreWithoutName() {
		Genre genre = new Genre();
		genre.setDescription("test genre nameless");
		assertThatThrownBy(() -> genreDAO.create(genre))
				.isInstanceOf(DaoException.class);
	}

	@Test
	public void tryGenreWithoutDescription() {
		Genre genre = new Genre();
		genre.setName("Test8");
		assertThatThrownBy(() -> genreDAO.create(genre))
				.isInstanceOf(DaoException.class);
	}
}
