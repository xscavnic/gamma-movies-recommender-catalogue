package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;
import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import cz.fi.muni.pa165.sem6.gamma.entity.Rating;

import cz.fi.muni.pa165.sem6.gamma.PersistenceSampleApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for the MovieDAOImpl class
 *
 * @author Pavol Bock, Šárka Ščavnická
 */
@DirtiesContext
@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
@Transactional
public class MovieDAOTest extends AbstractTestNGSpringContextTests {

    @Autowired
    public MovieDAO movieDAO;
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    private RatingDAO ratingDAO;

    @Autowired
    private GenreDAO genreDAO;

    private Movie movie1;
    private Movie movie2;
    private Movie movie3;
    private Movie movie4;
    private Movie newMovie;

    private Set<Genre> genres1;
    private Set<Genre> genres2;
    private Set<Genre> genres3;
    private Genre g1;
    private Genre g2;

    private Rating rating1;
    private Rating rating2;
    private Rating rating3;

    @BeforeClass
    public void beforeClass() {
        g1 = new Genre();
        g1.setName("Movie genre 1");
        g1.setDescription("Description 1");
        genreDAO.create(g1);
        g2 = new Genre();
        g2.setName("Movie genre 2");
        g2.setDescription("Description 2");
        genreDAO.create(g2);
        genres1 = new HashSet<>();
        genres1.add(g1);
        genres2 = new HashSet<>();
        genres2.add(g2);
        genres3 = new HashSet<>();
        genres3.add(g1);
        genres3.add(g2);

        movie4 = new Movie();
        movie4.setTitle("Movie 18");
        movie4.setDirector("Director 18");
        movie4.setDescription("Description 18");
        movie4.setGenres(genres3);
        movieDAO.create(movie4);

        movie1 = new Movie();
        movie1.setTitle("Movie 1");
        movie1.setDirector("Director 1");
        movie1.setDescription("Description 1");
        Set<Rating> ratings1 = new HashSet<>();
        ratings1.add(rating1);
        movie1.setReviews(ratings1);
        movie1.setGenres(genres1);
        movieDAO.create(movie1);

        movie2 = new Movie();
        movie2.setTitle("New movie  2");
        movie2.setDirector("Director 2");
        movie2.setDescription("Description 2");
        Set<Rating> ratings2 = new HashSet<>();
        ratings2.add(rating2);
        movie2.setReviews(ratings2);
        movie2.setGenres(genres2);
        movieDAO.create(movie2);

        movie3 = new Movie();
        movie3.setTitle("New movie  3");
        movie3.setDirector("Director 3");
        movie3.setDescription("Description 3");
        Set<Rating> ratings3 = new HashSet<>();
        ratings3.add(rating3);
        movie3.setGenres(genres2);
        movieDAO.create(movie3);

        LocalDate today = LocalDate.now();
        Person person1 = new Person();
        person1.setLogin("login1");
        person1.setPassword("password1");
        person1.setFirstName("a");
        person1.setLastName("a");
        person1.setBirthDate(today);
        person1.setCountry("A");
        personDAO.create(person1);

        Rating rating1 = new Rating();
        rating1.setTotalStars(5);
        rating1.setMovie(movie1);
        rating1.setPerson(person1);
        rating1.setRatingDescription("Rating description 1");
        rating1.setTimeAdded(today);
        rating1.setReportedRating(false);
        ratingDAO.create(rating1);

        Rating rating2 = new Rating();
        rating2.setTotalStars(4);
        rating2.setMovie(movie2);
        rating2.setPerson(person1);
        rating2.setRatingDescription("Rating description 2");
        rating2.setTimeAdded(today);
        rating2.setReportedRating(false);
        ratingDAO.create(rating2);

        Rating rating3 = new Rating();
        rating3.setTotalStars(3);
        rating3.setMovie(movie3);
        rating3.setPerson(person1);
        rating3.setRatingDescription("Rating description 3");
        rating3.setTimeAdded(today);
        rating3.setReportedRating(false);
        ratingDAO.create(rating3);

        Rating rating4 = new Rating();
        rating4.setTotalStars(2);
        rating4.setMovie(movie3);
        rating4.setPerson(person1);
        rating4.setRatingDescription("Rating description 4");
        rating4.setTimeAdded(today);
        rating4.setReportedRating(false);
        ratingDAO.create(rating4);
    }

    @Test
    public void create() {
        Movie newMovie = new Movie();
        newMovie.setTitle("New Movie");
        newMovie.setDirector("New Director");

        Set<Rating> ratings1 = new HashSet<>();
        ratings1.add(rating1);
        newMovie.setReviews(ratings1);
        newMovie.setGenres(genres2);
        newMovie.setDescription("New Description");
        movieDAO.create(newMovie);
        Movie movie = movieDAO.findById(newMovie.getId());

        assertThat(movie).isNotNull();
        assertThat(movie.getId()).isNotNull();
        assertThat(ratingDAO.findAll().size()).isEqualTo(4);
        assertThat(movie).isEqualTo(newMovie);
    }

    @Test
    public void findById() {
        Movie movie_test2 = movieDAO.findById(movie2.getId());
        assertThat(movie_test2.getTitle()).isEqualTo(movie2.getTitle());
        assertThat(movie_test2.getDirector()).isEqualTo(movie2.getDirector());
        assertThat(movie_test2.getDescription()).isEqualTo(movie2.getDescription());
        assertThat(movie_test2).isEqualTo(movie2);
        assertThat(movie2).isEqualTo(movie_test2);
    }

    @Test(priority = -1)
    public void findAll() {
        List<Movie> movies = movieDAO.findAll();
        assertThat(movies).hasSize(4);
        assertThat(movies).map(Movie::getTitle).containsExactlyElementsOf(List.of("Movie 18","Movie 1", "New movie  2", "New movie  3"));
    }


    @Test
    public void update() {
        movie3.setTitle("New movie updated 3");
        movieDAO.update(movie3);
        assertThat(movieDAO.findById(movie3.getId())).isEqualTo(movie3);
        assertThat(movieDAO.findById(movie3.getId()).getTitle()).isEqualTo("New movie updated 3");
    }


    @Test
    public void remove() {
        Movie movie = new Movie();
        movie.setTitle("Test title");
        movie.setDescription("test movie delete");
        movie.setDirector("New Director 2");
        Set<Rating> ratings1 = new HashSet<>();
        ratings1.add(rating1);
        movie.setReviews(ratings1);
        movie.setGenres(genres1);
        movieDAO.create(movie);
        assertThat(movieDAO.findById(movie.getId())).isNotNull();

        movieDAO.remove(movie);
        assertThat(movieDAO.findById(movie.getId())).isNull();
    }

    @Test
    public void getRating() {
        Double result = movieDAO.getRating(movie2);
        assertThat(result).isEqualTo(4);
    }

    @Test
    public void getMoviesRatings() {
        Map<Long, Double> result = movieDAO.getMoviesRatings();

        assertThat(result.size()).isEqualTo(3);
        assertThat(result.get(movie1.getId())).isEqualTo(5);
        assertThat(result.get(movie2.getId())).isEqualTo(4);
        assertThat(result.get(movie3.getId())).isEqualTo(2.5);
        assertThat(result.get(movie3.getId()) < result.get(movie1.getId())).isTrue();
        assertThat(result.get(movie1.getId()) > result.get(movie2.getId())).isTrue();
    }

    @Test(priority = -1)
    public void getMoviesByGenres() {
        Set<Long> genres = new HashSet<>();
        genres.add(g1.getID());
        genres.add(g2.getID());
        Set<Movie> result = movieDAO.getMoviesByGenres(genres);

        assertThat(result.size()).isEqualTo(4);
        assertThat(result).map(Movie::getTitle).containsExactlyElementsOf(List.of("Movie 1", "New movie  2", "New movie  3", "Movie 18"));
    }

    @Test(priority = -1)
    public void getSimilarByGenre(){
        List<Movie> result = movieDAO.getSimilarByGenre(movie2);
        assertThat(result.size()).isEqualTo(3);
        assertThat(result).map(Movie::getTitle).containsExactlyElementsOf(List.of("Movie 18", "New movie  2", "New movie  3"));

        List<Movie> result1 = movieDAO.getSimilarByGenre(movie1);
        assertThat(result1.size()).isEqualTo(2);
        assertThat(result1).map(Movie::getTitle).containsExactlyElementsOf(List.of("Movie 18","Movie 1"));

        List<Movie> result2 = movieDAO.getSimilarByGenre(movie4);
        assertThat(result2.size()).isEqualTo(4);
        assertThat(result2).map(Movie::getTitle).containsExactlyElementsOf(List.of("Movie 18","Movie 1","New movie  2", "New movie  3"));
    }

    @Test
    public void getSimilarByRating(){
        List<Movie> result = movieDAO.getSimilarByRatings(movie2);
        assertThat(result.size()).isEqualTo(1);
        assertThat(result).map(Movie::getTitle).containsExactlyElementsOf(List.of("Movie 1"));
    }
}
