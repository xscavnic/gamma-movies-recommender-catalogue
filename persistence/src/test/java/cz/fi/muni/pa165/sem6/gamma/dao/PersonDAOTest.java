package cz.fi.muni.pa165.sem6.gamma.dao;

import cz.fi.muni.pa165.sem6.gamma.PersistenceSampleApplicationContext;
import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import cz.fi.muni.pa165.sem6.gamma.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Tests for PersonDAO class.
 *
 * @author Pavol Bock, 506490
 */
@DirtiesContext
@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
@Transactional
public class PersonDAOTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private PersonDAO personDAO;

    private Person janko;
    private Person ignac;

    @BeforeClass
    public void setup() {
        janko = new Person();
        LocalDate randomYear = LocalDate.ofYearDay(1992, 2);
        janko.setLogin("Janko");
        janko.setPassword("12345");
        janko.setFirstName("Jan");
        janko.setLastName("Drevo");
        janko.setCountry("Czechia");
        janko.setBirthDate(randomYear);
        personDAO.create(janko);

        LocalDate randomYear2 = LocalDate.ofYearDay(2000, 6);
        ignac = new Person("Igi", "5555", "Ignac", "Jahoda", "Slovakia", randomYear2);
        personDAO.create(ignac);
    }

    @AfterClass
    public void afterClass() {
        for (Person u : personDAO.findAll()) {
            personDAO.remove(u);
        }
    }

    @Test
    public void create() {
        LocalDate today = LocalDate.now();
        Person marienka = new Person("Marienka", "98765", "Maria", "Chalupkova", "Slovakia", today);
        personDAO.create(marienka);

        assertThat(personDAO.findById(marienka.getId())).isEqualTo(marienka);
    }

    @Test
    public void update() {
        janko.setPassword("SuperBossJanko");
        personDAO.update(janko);

        assertThat(personDAO.findById(janko.getId())).isEqualTo(janko);
        assertThat(personDAO.findById(janko.getId()).getPassword()).isEqualTo("SuperBossJanko");
    }

    @Test
    public void findById() {
        Person retJanko = personDAO.findByLogin(janko.getLogin());
        assertThat(janko).isEqualTo(retJanko);
    }

    @Test
    public void findAll() {
        List<Person> returnedUsers = personDAO.findAll();

        assertThat(returnedUsers.size()).isEqualTo(3);
        assertThat(returnedUsers.contains(janko)).isTrue();
    }

    @Test
    public void remove() {
        Long ignacID = ignac.getId();
        personDAO.remove(ignac);

        assertThat(personDAO.findById(ignacID)).isNull();
    }

    @Test
    public void findByLogin() {
        User ignacLogin = personDAO.findByLogin(ignac.getLogin());
        assertThat(ignac).isEqualTo(ignacLogin);
    }
}
