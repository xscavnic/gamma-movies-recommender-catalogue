package cz.fi.muni.pa165.sem6.gamma.sampledata;

import java.io.IOException;

/**
 * Populates database with sample data.
 *
 * @author Šárka Ščavnická
 */

public interface SampleDataLoading {

    void loadData() throws IOException;
}
