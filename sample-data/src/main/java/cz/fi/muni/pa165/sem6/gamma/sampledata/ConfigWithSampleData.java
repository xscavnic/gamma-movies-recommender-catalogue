package cz.fi.muni.pa165.sem6.gamma.sampledata;

import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Configuration
@Import(ServiceConfiguration.class)
@ComponentScan(basePackageClasses = {SampleDataLoadingImpl.class})
public class ConfigWithSampleData {

	@Autowired
	SampleDataLoading sampleDataLoading;

	@PostConstruct
	public void dataLoading() throws IOException {
		sampleDataLoading.loadData();
	}
}
