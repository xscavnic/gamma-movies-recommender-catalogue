package cz.fi.muni.pa165.sem6.gamma.sampledata;

import cz.fi.muni.pa165.sem6.gamma.dto.AdminCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.service.AdminService;
import cz.fi.muni.pa165.sem6.gamma.service.GenreService;
import cz.fi.muni.pa165.sem6.gamma.service.MovieService;
import cz.fi.muni.pa165.sem6.gamma.service.PersonService;
import cz.fi.muni.pa165.sem6.gamma.service.RatingService;

import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Loads some sample data to populate the movie recommender database.
 *
 * @author Šárka Ščavnická
 */

@Component
@Import(ServiceConfiguration.class)
@Transactional
public class SampleDataLoadingImpl implements SampleDataLoading {

    private List<String> fb_actors = new ArrayList<>();
    private List<String> ds_actors = new ArrayList<>();
    private List<String> av_actors = new ArrayList<>();
    private List<String> bm_actors = new ArrayList<>();
    private List<String> sh_actors = new ArrayList<>();

    final static Logger log = LoggerFactory.getLogger(SampleDataLoadingImpl.class);

    public static final String JPEG = "image/jpeg";


    @Autowired
    private AdminService adminService;

    @Autowired
    private PersonService personService;

    @Autowired
    private GenreService genreService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private RatingService ratingService;


    @Override
    @SuppressWarnings("unused")
    public void loadData() throws IOException {
        fb_actors.add("Jozko Vajda");
        fb_actors.add("Tom Cruise");
        fb_actors.add("Johny Depp");

        ds_actors.add("Benedict Cumberbatch");
        ds_actors.add("Chiwetel Ejiofor");
        ds_actors.add("Rachel McAdams");

        av_actors.add("Robeert Downey Jr.");
        av_actors.add("Chris Evans");
        av_actors.add("Scarlett Johansson");

        bm_actors.add("Christian Bale");
        bm_actors.add("Heath Ledger");
        bm_actors.add("Aaron Eckhart");

        sh_actors.add("Tim Robbins");
        sh_actors.add("Morgan Freeman");
        sh_actors.add("Bob Gunton");

        genre("Comedy","The comedy genre is defined by events that are...");
        genre("Drama", "The drama genre is defined by conflict and often...");
        genre("Action", "Movies in the action genre are defined by risk...");
        genre("Romance", "The romance genre is defined by intimate relatio...");
        genre("Fantasy", "The fantasy genre is defined by both circumst...");
        genre("Horror", "The horror genre is centered upon depicting ter...");

        Set<GenreDTO> genres_fb = new HashSet<>(genreService.findAll().subList(0, 3));
        Set<GenreDTO> genres_ds = new HashSet<>(genreService.findAll().subList(0, 5));
        Set<GenreDTO> genres_av = new HashSet<>(genreService.findAll().subList(0, 2));
        Set<GenreDTO> genres_bm = new HashSet<>(genreService.findAll().subList(2, 4));
        Set<GenreDTO> genres_sh = new HashSet<>(genreService.findAll().subList(3, 6));

        MovieCreateDTO fantasticBeasts1 = movie("Fantastic Beasts 1", "The adventures of writer Newt Scamander in New York's secret community of witches and wizards seventy years before Harry Potter reads his book in school.", "David Yates", genres_fb, fb_actors, readImage("1.jpg"));
        MovieCreateDTO fantasticBeasts2 = movie("Fantastic Beasts 2", "The second installment of the \"Fantastic Beasts\" series featuring the adventures of Magizoologist Newt Scamander.", "David Yates", genres_fb, fb_actors, readImage("2.jpg"));
        MovieCreateDTO doctorStrangeMulti = movie("Doctor Strange in the Multiverse of Madness", "Doctor Strange teams up with a mysterious teenage girl from his dreams who can travel across multiverses, to battle multiple threats, including other-universe versions of himself, which threaten to wipe out millions across the multiverse.", "Sam Raimi", genres_ds, ds_actors, readImage("3.jpg"));
        MovieCreateDTO doctorStrange = movie("Doctor Strange", "While on a journey of physical and spiritual healing, a brilliant neurosurgeon is drawn into the world of the mystic arts.", "Scott Derrickson", genres_ds, ds_actors, readImage("4.jpg"));
        MovieCreateDTO avangersEndGame = movie("Avengers: Endgame", "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.", "David Yates", genres_av, av_actors, readImage("5.jpg"));
        MovieCreateDTO avangers = movie("Avengers", "Earth's mightiest heroes must come together and learn to fight as a team if they are going to stop the mischievous Loki and his alien army from enslaving humanity.", "Joss Whedon", genres_av, av_actors, readImage("6.jpg"));
        MovieCreateDTO batmanBegins = movie("Batman Begins", "After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from corruption.", "Christopher Nolan", genres_bm, bm_actors, readImage("7.jpg"));
        MovieCreateDTO batmanDarkKnightRises = movie("The Dark Knight Rises", "Eight years after the Joker's reign of anarchy, Batman, with the help of the enigmatic Catwoman, is forced from his exile to save Gotham City from the brutal guerrilla terrorist Bane.", "Christopher Nolan", genres_bm, bm_actors, readImage("8.jpg"));
        MovieCreateDTO batmanDarkKnight = movie("The Dark Knight", "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.", "Christopher Nolan", genres_bm, bm_actors, readImage("9.jpg"));
        MovieCreateDTO shawshank = movie("The Shawshank Redemption", "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.", "Frank Darabont", genres_sh, sh_actors, readImage("10.jpg"));
        log.info("Loaded movie recommender movies.");

        AdminCreateDTO admin = admin("admin", "admin");
        AdminCreateDTO moderator = admin("moderator", "moderator");
        log.info("Loaded movie recommender with admin.");

        PersonCreateDTO adam = person("adam", "adam", "Adam", "Novak", "Czechia",  LocalDate.of(2000, 1, 13));
        PersonCreateDTO eva = person("eva", "eva", "Eva", "Blue", "Swiss",  LocalDate.of(1996, 5, 20));
        PersonCreateDTO jack = person("jack", "jack", "Jack", "Black", "USA",  LocalDate.of(1975, 9, 5));
        PersonCreateDTO donau = person("donau", "donau", "Donau", "Boney", "Germany",  LocalDate.of(2003, 2, 15));
        PersonCreateDTO berkley = person("berkley", "berkley", "Berkeley", "Mateer", "United Kingdom",  LocalDate.of(1993, 4, 5));
        PersonCreateDTO nikola = person("nikola", "nikola", "Nikola", "Grapp", "Croatia",  LocalDate.of(1977, 4, 15));
        PersonCreateDTO ernie = person("ernie", "ernie", "Ernie", "Kowalcyk", "Poland",  LocalDate.of(1969, 6, 17));
        PersonCreateDTO signy = person("signy", "signy", "Signy", "Forner", "Belgium",  LocalDate.of(1957, 8, 13));
        PersonCreateDTO abida = person("abida", "abida", "Abida", "Kassin", "Kongo",  LocalDate.of(2001, 12, 22));
        PersonCreateDTO ilean = person("ilean", "ilean", "Ilean", "Witz", "Austria",  LocalDate.of(1999, 11, 30));
        log.info("Loaded movie recommender with users.");

        RatingCreateDTO rating1 = rating(false, "Aenean tortor mi, dictum vel eros ac, auctor feugiat augue.", LocalDate.of(2022, 2, 3), 5, personService.findByLogin(eva.getLogin()), movieService.findAll().get(0));
        RatingCreateDTO rating2 = rating(false, "Maecenas ultricies, nisl vel placerat tempor, purus magna tempus tortor, ac aliquet dolor mi at quam.", LocalDate.of(2019, 2, 3), 5, personService.findByLogin(jack.getLogin()), movieService.findAll().get(1));
        RatingCreateDTO rating3 = rating(false, "Morbi nibh odio, consectetur ut enim ut, lacinia efficitur lectus.", LocalDate.of(2018, 5, 13), 3, personService.findByLogin(donau.getLogin()), movieService.findAll().get(2));
        RatingCreateDTO rating4 = rating(false, "Suspendisse tellus lacus, commodo sit amet ex eget, consequat hendrerit arcu.", LocalDate.of(2022, 4, 21), 2, personService.findByLogin(abida.getLogin()), movieService.findAll().get(3));
        RatingCreateDTO rating5 = rating(false, "Pellentesque metus ligula, molestie eget eleifend vel, interdum vel est. Aliquam ipsum nibh, blandit quis augue tristique, bibendum imperdiet justo.", LocalDate.of(2022, 2, 3), 5, personService.findByLogin(signy.getLogin()), movieService.findAll().get(3));
        RatingCreateDTO rating6 = rating(false, "Nunc at aliquet nulla. Sed placerat posuere nunc, id faucibus risus hendrerit eu.", LocalDate.of(2022, 3, 31), 4, personService.findByLogin(ernie.getLogin()), movieService.findAll().get(4));
        RatingCreateDTO rating7 = rating(false, "Vivamus tristique mauris ut sapien efficitur, ac feugiat nulla tempus. Sed dignissim non ligula eget dapibus.", LocalDate.of(2022, 2, 2), 5, personService.findByLogin(nikola.getLogin()), movieService.findAll().get(5));
        RatingCreateDTO rating8 = rating(false, "Maecenas ut nunc venenatis, gravida magna eget, aliquam erat.", LocalDate.of(2022, 2, 3), 5, personService.findByLogin(berkley.getLogin()), movieService.findAll().get(5));
        RatingCreateDTO rating9 = rating(false, "Proin faucibus auctor nisi ac pharetra.", LocalDate.of(2022, 2, 23), 5, personService.findByLogin(adam.getLogin()), movieService.findAll().get(6));
        RatingCreateDTO rating10 = rating(false, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2020, 12, 24), 5, personService.findByLogin(ernie.getLogin()), movieService.findAll().get(7));
        RatingCreateDTO rating11 = rating(false, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2020, 7, 13), 2, personService.findByLogin(abida.getLogin()), movieService.findAll().get(7));
        RatingCreateDTO rating12 = rating(false, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2020, 8, 18), 1, personService.findByLogin(donau.getLogin()), movieService.findAll().get(6));
        RatingCreateDTO rating13 = rating(false, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2020, 9, 10), 3, personService.findByLogin(jack.getLogin()), movieService.findAll().get(8));
        RatingCreateDTO rating14 = rating(false, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2020, 10, 4), 5, personService.findByLogin(ilean.getLogin()), movieService.findAll().get(8));
        RatingCreateDTO rating15 = rating(false, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2020, 4, 3), 5, personService.findByLogin(eva.getLogin()), movieService.findAll().get(9));
        RatingCreateDTO rating16 = rating(true, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2021, 4, 13), 5, personService.findByLogin(eva.getLogin()), movieService.findAll().get(9));
        RatingCreateDTO rating17 = rating(true, "Suspendisse condimentum elementum dolor, eu iaculis dui consequat nec.", LocalDate.of(2021, 4, 30), 5, personService.findByLogin(jack.getLogin()), movieService.findAll().get(1));
        log.info("Loaded movie recommender with ratings.");
    }

    private RatingCreateDTO rating(Boolean reportedRating, String ratingDescription, LocalDate timeAdded, Integer totalStars, PersonDTO person, MovieDTO movie) {
        RatingCreateDTO r = new RatingCreateDTO();
        r.setReportedRating(reportedRating);
        r.setRatingDescription(ratingDescription);
        r.setTimeAdded(timeAdded);
        r.setTotalStars(totalStars);
        r.setPerson(person);
        r.setMovie(movie);
        ratingService.create(r);
        return r;
    }

    private PersonCreateDTO person(String login, String password, String firstName, String lastName, String country, LocalDate birthDate) {
        PersonCreateDTO p = new PersonCreateDTO();
        p.setLogin(login);
        p.setPassword(password);
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setCountry(country);
        p.setBirthDate(birthDate);
        personService.register(p);
        return p;
    }

    private AdminCreateDTO admin(String login, String password) {
        AdminCreateDTO a = new AdminCreateDTO();
        a.setLogin(login);
        a.setPassword(password);
        adminService.register(a);
        return a;
    }

    private byte[] readImage(String file) throws IOException {
        try (InputStream is = this.getClass().getResourceAsStream("/" + file)) {
            int nRead;
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] data = new byte[1024];
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            return buffer.toByteArray();
        }
    }

    private void genre(String name, String description) {
        GenreCreateDTO g = new GenreCreateDTO();
        g.setName(name);
        g.setDescription(description);
        genreService.create(g);
    }

    private MovieCreateDTO movie(String title, String description, String director, Set<GenreDTO> genres, List<String> actors, byte[] image) {
        MovieCreateDTO m = new MovieCreateDTO();
        m.setTitle(title);
        m.setDescription(description);
        m.setDirector(director);
        m.setGenres(genres);
        m.setActors(actors);
        m.setImage(image);
        movieService.create(m);
        return m;
    }

}
