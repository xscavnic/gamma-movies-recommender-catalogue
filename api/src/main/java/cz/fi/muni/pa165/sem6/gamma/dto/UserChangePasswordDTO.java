package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class UserChangePasswordDTO {
	@NotNull
	private String login;
	@NotNull
	private String oldPassword;
	@NotNull
	private String newPassword;

	public UserChangePasswordDTO() {
	}

	public UserChangePasswordDTO(String login, String oldPassword, String newPassword) {
		this.login = login;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserChangePasswordDTO entity = (UserChangePasswordDTO) o;
		return Objects.equals(this.login, entity.login) &&
				Objects.equals(this.oldPassword, entity.oldPassword) &&
				Objects.equals(this.newPassword, entity.newPassword);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login, oldPassword, newPassword);
	}
}
