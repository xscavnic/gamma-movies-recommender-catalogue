package cz.fi.muni.pa165.sem6.gamma.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Data transfer object for creating Person entity
 *
 * @author Kristián Malák
 */
public class PersonCreateDTO {
	@NotNull
	private String login;
	@NotNull
	private String password;
	@NotNull
	private String firstName;
	@NotNull
	private String lastName;
	@NotNull
	private String country;
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthDate;

	public PersonCreateDTO() {
	}

	public PersonCreateDTO(String login, String password, String firstName, String lastName, String country, LocalDate birthDate) {
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
		this.birthDate = birthDate;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PersonCreateDTO entity = (PersonCreateDTO) o;
		return Objects.equals(this.login, entity.login) &&
				Objects.equals(this.password, entity.password) &&
				Objects.equals(this.firstName, entity.firstName) &&
				Objects.equals(this.lastName, entity.lastName) &&
				Objects.equals(this.country, entity.country) &&
				Objects.equals(this.birthDate, entity.birthDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login, password, firstName, lastName, country, birthDate);
	}
}
