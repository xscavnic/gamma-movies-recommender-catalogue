package cz.fi.muni.pa165.sem6.gamma.dto;

public abstract class UserDTO {
	public abstract Long getId();

	public abstract String getPassword();

	public abstract void setPassword(String password);
}
