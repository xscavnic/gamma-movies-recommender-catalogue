package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Data transfer object for creating Admin entity
 *
 * @author Kristián Malák
 */
public class AdminCreateDTO {
	@NotNull
	private String login;
	@NotNull
	private String password;

	public AdminCreateDTO() {
	}

	public AdminCreateDTO(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AdminCreateDTO entity = (AdminCreateDTO) o;
		return Objects.equals(this.login, entity.login) &&
				Objects.equals(this.password, entity.password);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login, password);
	}
}
