package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Data transfer object - genreCreate
 *
 * @author Jakub Kuchár
 */
public class GenreCreateDTO {
    @NotNull
    @Size(min = 2, max = 50)
    private String name;

    @NotNull
    @Size(min = 2, max = 500)
    private String description;

    private Set<MovieDTO> movieSet = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MovieDTO> getMovieSet() {
        return movieSet;
    }

    public void setMovieSet(Set<MovieDTO> movieSet) {
        this.movieSet = movieSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GenreCreateDTO)) return false;

        GenreCreateDTO that = (GenreCreateDTO) o;

        return getName() != null ? getName().equals(that.getName()) : that.getName() == null;
    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "GenreCreateDTO{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", movieSet size=" + movieSet.size() +
                '}';
    }
}
