package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * Data transfer object for Admin entity
 *
 * @author Kristián Malák
 */
public class AdminDTO extends UserDTO {
	private Long id;
	@NotNull
	private String login;
	@NotNull
	private String password;
	@NotNull
	private List<GenreDTO> genres;

	public AdminDTO() {
	}

	public AdminDTO(Long id, String login, String password, List<GenreDTO> genres) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.genres = genres;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	public List<GenreDTO> getGenres() {
		return genres;
	}

	public void setGenres(List<GenreDTO> genres) {
		this.genres = genres;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AdminDTO entity = (AdminDTO) o;
		return Objects.equals(this.login, entity.login);
	}

	@Override
	public int hashCode() {
		return 31 + (getLogin() != null ? getLogin().hashCode() : 0);
	}
}
