package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Data transfer object - ratingCreate
 *
 * @author Šárka Ščavnická
 */
public class RatingCreateDTO {

    private Boolean reportedRating;
    @Size(max = 500)
    private String ratingDescription;

    @NotNull
    private Long id;

    @NotNull
    private LocalDate timeAdded;

    @NotNull
    @Min(0)
    @Max(5)
    private Integer totalStars;

    @NotNull
    private PersonDTO person;

    @NotNull
    private MovieDTO movie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRatingDescription() {
        return ratingDescription;
    }

    public void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription;
    }

    public Integer getTotalStars() {
        return totalStars;
    }

    public void setTotalStars(Integer totalStars) {
        this.totalStars = totalStars;
    }

    public LocalDate getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(LocalDate timeAdded) {
        this.timeAdded = timeAdded;
    }

    public Boolean getReportedRating() {
        return reportedRating;
    }

    public void setReportedRating(Boolean reportedRating) {
        this.reportedRating = reportedRating;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO m) {
        this.movie = m;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO p) {
        this.person = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RatingDTO)) return false;

        RatingDTO r = (RatingDTO) o;

        if (getTotalStars() != null ? !getTotalStars().equals(r.getTotalStars()) : r.getTotalStars() != null)
            return false;
        if (getTimeAdded() != null ? !getTimeAdded().equals(r.getTimeAdded()) : r.getTimeAdded() != null)
            return false;
//        if (getMovie() != null ? !getMovie().equals(r.getMovie()) : r.getMovie() != null) return false;
        return getPerson() != null ? getPerson().equals(r.getPerson()) : r.getPerson() == null;
    }

    @Override
    public int hashCode() {
        int result = getTotalStars() != null ? getTotalStars().hashCode() : 0;
        result = 31 * result + (getTimeAdded() != null ? getTimeAdded().hashCode() : 0);
//        result = 31 * result + (getMovie() != null ? getMovie().hashCode() : 0);
        result = 31 * result + (getPerson() != null ? getPerson().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "description='" + ratingDescription + '\'' +
                ", movie=" + movie +
                ", stars=" + totalStars +
                ", person=" + person +
                ", time=" + timeAdded +
                ", reported=" + reportedRating +
                '}';
    }

}
