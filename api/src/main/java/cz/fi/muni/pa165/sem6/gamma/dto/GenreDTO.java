package cz.fi.muni.pa165.sem6.gamma.dto;

/**
 * Data transfer object - genre
 *
 * @author Jakub Kuchár
 */
public class GenreDTO {
    private Long id;
    private String name;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GenreDTO)) return false;

        GenreDTO genreDTO = (GenreDTO) o;

        return getName() != null ? getName().equals(genreDTO.getName()) : genreDTO.getName() == null;
    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }
}
