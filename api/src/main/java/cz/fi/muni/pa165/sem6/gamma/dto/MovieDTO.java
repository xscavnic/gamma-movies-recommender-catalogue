package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Data transfer object - movie
 *
 * @author Pavol Bock, 506490
 */
public class MovieDTO extends MovieCreateDTO{

    @NotNull
    private Long id;

    private Set<RatingDTO> rating;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<RatingDTO> getReviews() {
        return rating;
    }

    public void setReviews(Set<RatingDTO> Ratings) {
        this.rating = Ratings;
    }

}
