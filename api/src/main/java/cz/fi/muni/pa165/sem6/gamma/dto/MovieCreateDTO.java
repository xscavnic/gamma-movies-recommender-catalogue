package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;


/**
 * Data transfer object - movieCreate
 *
 * @author Pavol Bock, 506490
 */
public class MovieCreateDTO {

    @NotNull
    @Size(max = 300)
    private String title;

    @Size(max = 2500)
    private String description;

    @NotNull
    @Size(max = 50)
    private String director;

    @NotNull
    private Set<GenreDTO> genres;

    private List<String> actors;

    private byte[] image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String Title) {
        this.title = Title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String Director) {
        this.director = Director;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String Description) {
        this.description = Description;
    }

    public byte[] getImage(int position) {
        return image;
    }

    public void setImage(byte[] Image) {
        this.image = Image;
    }

    public void setGenres(Set<GenreDTO> Genres) { this.genres = Genres; }

    public Set<GenreDTO> getGenres(){return genres;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieCreateDTO)) return false;

        MovieCreateDTO that = (MovieCreateDTO) o;

        if (!getTitle().equals(that.getTitle())) return false;
        if (!getDirector().equals(that.getDirector())) return false;
        return getGenres().equals(that.getGenres());
    }

    @Override
    public int hashCode() {
        int result = getTitle().hashCode();
        result = 31 * result + getDirector().hashCode();
        result = 31 * result + getGenres().hashCode();
        return result;
    }
}
