package cz.fi.muni.pa165.sem6.gamma.dto;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class UserAuthenticateDTO {
	@NotNull
	private String login;
	@NotNull
	private String password;
	@NotNull
	private Long id;

	public UserAuthenticateDTO() {
	}

	public UserAuthenticateDTO(String login, String password) {
		this.id = id;
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserAuthenticateDTO entity = (UserAuthenticateDTO) o;
		return  Objects.equals(this.id, entity.id) &&
				Objects.equals(this.login, entity.login) &&
				Objects.equals(this.password, entity.password);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, login, password);
	}
}
