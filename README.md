# Gamma - Movies Recommender Catalogue

## About the project
* Name: *Movies Recommender Catalogue*
* Technologies: Java 11, Hibernate, Maven, Spring
* Developers:
  * Jakub Kuchár, [*@xkuchar2*](https://gitlab.fi.muni.cz/xkuchar2)
  * Pavol Bock, [*@xbock*](https://gitlab.fi.muni.cz/xbock)
  * Kristián Malák, [*@xmalak*](https://gitlab.fi.muni.cz/xmalak)
  * Šárka Ščavnická, [*@xscavnic*](https://gitlab.fi.muni.cz/xscavnic)
* Assignment:
  * The web application is a catalogue of movies of different genres.
  Each movie has a title, description, film director, and list of 
  actors, and image(s) from the main playbill. Each movie is categorized
  in one or more genres. Only the administrator can create, remove and 
  update the list of movies. Users can rate the movies according to
  different criteria (e.g how novel are the ideas of the movie, their 
  final score, etc…). The main feature of the system is that users can
  pick one movie and get the list of similar movies and / or movies that
  were liked the most by other users watching the same movie (no need 
  of complex algorithms, some simple recommendation is enough!).

## Project description
Movies recommender is a simple catalogue with movies where users can create ratings. 

### Roles
System has two authorization roles - **Admin** and **Person**.

* **Admin** is a role for a system administrator. This user has the ability to add, edit and delete movies into the catalogue, or create and edit different genres.
* **Person** is a role for general user. Persons can view the movie catalogue, create ratings for movies and ask the system to recommend similar movies to a certain (selected) movie.

### Entities
* **User** - This is an abstract class representing every user interacting with the system. Implemented by two roles:.
  * **Person** - Entity representing normal user.
  * **Admin** - Entity representing a system administrator.
* **Movie** - Entity representing a movie.
* **Rating** - Entity representing ratings that correspond to certain movie.
* **Genre** - Entity representing different genres, corresponding to appropriate movies.

## Use case diagram
![Use case diagram](documentation/Use case diagram.jpg)

## Class diagram
![Class diagram](documentation/Class diagram.jpg)

## Instructions
To run the the *DAO* layer tests, type `mvn clean install` into the terminal.




[//]: # (## Getting started)

[//]: # ()
[//]: # (To make it easy for you to get started with GitLab, here's a list of recommended next steps.)

[//]: # ()
[//]: # (Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom]&#40;#editing-this-readme&#41;!)

[//]: # ()
[//]: # (## Add your files)

[//]: # ()
[//]: # (- [ ] [Create]&#40;https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file&#41; or [upload]&#40;https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file&#41; files)

[//]: # (- [ ] [Add files using the command line]&#40;https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line&#41; or push an existing Git repository with the following command:)

[//]: # ()
[//]: # (```)

[//]: # (cd existing_repo)

[//]: # (git remote add origin https://gitlab.fi.muni.cz/xscavnic/gamma-movies-recommender-catalogue.git)

[//]: # (git branch -M main)

[//]: # (git push -uf origin main)

[//]: # (```)

[//]: # ()
[//]: # (## Integrate with your tools)

[//]: # ()
[//]: # (- [ ] [Set up project integrations]&#40;https://gitlab.fi.muni.cz/xscavnic/gamma-movies-recommender-catalogue/-/settings/integrations&#41;)

[//]: # ()
[//]: # (## Collaborate with your team)

[//]: # ()
[//]: # (- [ ] [Invite team members and collaborators]&#40;https://docs.gitlab.com/ee/user/project/members/&#41;)

[//]: # (- [ ] [Create a new merge request]&#40;https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html&#41;)

[//]: # (- [ ] [Automatically close issues from merge requests]&#40;https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically&#41;)

[//]: # (- [ ] [Enable merge request approvals]&#40;https://docs.gitlab.com/ee/user/project/merge_requests/approvals/&#41;)

[//]: # (- [ ] [Automatically merge when pipeline succeeds]&#40;https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html&#41;)

[//]: # ()
[//]: # (## Test and Deploy)

[//]: # ()
[//]: # (Use the built-in continuous integration in GitLab.)

[//]: # ()
[//]: # (- [ ] [Get started with GitLab CI/CD]&#40;https://docs.gitlab.com/ee/ci/quick_start/index.html&#41;)

[//]: # (- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing&#40;SAST&#41;]&#40;https://docs.gitlab.com/ee/user/application_security/sast/&#41;)

[//]: # (- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy]&#40;https://docs.gitlab.com/ee/topics/autodevops/requirements.html&#41;)

[//]: # (- [ ] [Use pull-based deployments for improved Kubernetes management]&#40;https://docs.gitlab.com/ee/user/clusters/agent/&#41;)

[//]: # (- [ ] [Set up protected environments]&#40;https://docs.gitlab.com/ee/ci/environments/protected_environments.html&#41;)

[//]: # ()
[//]: # (***)

[//]: # ()
[//]: # (# Editing this README)

[//]: # ()
[//]: # (When you're ready to make this README your own, just edit this file and use the handy template below &#40;or feel free to structure it however you want - this is just a starting point!&#41;.  Thank you to [makeareadme.com]&#40;https://www.makeareadme.com/&#41; for this template.)

[//]: # ()
[//]: # (## Suggestions for a good README)

[//]: # (Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.)

[//]: # ()
[//]: # (## Name)

[//]: # (Choose a self-explaining name for your project.)

[//]: # ()
[//]: # (## Description)

[//]: # (Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.)

[//]: # ()
[//]: # (## Badges)

[//]: # (On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.)

[//]: # ()
[//]: # (## Visuals)

[//]: # (Depending on what you are making, it can be a good idea to include screenshots or even a video &#40;you'll frequently see GIFs rather than actual videos&#41;. Tools like ttygif can help, but check out Asciinema for a more sophisticated method.)

[//]: # ()
[//]: # (## Installation)

[//]: # (Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.)

[//]: # ()
[//]: # (## Usage)

[//]: # (Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.)

[//]: # ()
[//]: # (## Support)

[//]: # (Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.)

[//]: # ()
[//]: # (## Roadmap)

[//]: # (If you have ideas for releases in the future, it is a good idea to list them in the README.)

[//]: # ()
[//]: # (## Contributing)

[//]: # (State if you are open to contributions and what your requirements are for accepting them.)

[//]: # ()
[//]: # (For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.)

[//]: # ()
[//]: # (You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.)

[//]: # ()
[//]: # (## Authors and acknowledgment)

[//]: # (Show your appreciation to those who have contributed to the project.)

[//]: # ()
[//]: # (## License)

[//]: # (For open source projects, say how it is licensed.)

[//]: # ()
[//]: # (## Project status)

[//]: # (If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.)
