package cz.fi.muni.pa165.sem6.gamma.service.config;

import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.service.GenreServiceImpl;
import org.springframework.context.annotation.Configuration;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import cz.fi.muni.pa165.sem6.gamma.PersistenceSampleApplicationContext;

import java.util.Collections;

/**
 * Inspired by https://github.com/fi-muni/PA165/blob/master/eshop-service/src/main/java/cz/fi/muni/pa165/service/config/ServiceConfiguration.java
 * from the example project.
 *
 * @author Šárka Ščavnická
 */
@Configuration
@Import(PersistenceSampleApplicationContext.class)
@ComponentScan(basePackageClasses={GenreServiceImpl.class})
public class ServiceConfiguration {

    @Bean
    public Mapper dozer(){
        DozerBeanMapper dozer = new DozerBeanMapper();
        dozer.setMappingFiles(Collections.singletonList("dozerJdk8Converters.xml"));
        return dozer;
    }
}
