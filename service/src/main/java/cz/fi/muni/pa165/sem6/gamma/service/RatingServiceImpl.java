package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.RatingDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Implementation of Service for Rating entity
 *
 * @author Šárka Ščavnická
 */
@Service
public class RatingServiceImpl implements RatingService {

	@Autowired
	private RatingDAO ratingDAO;

	@Autowired
	private BeanMappingService beanMappingService;

	@Override
	public void create(@NotNull RatingCreateDTO r) {
		ratingDAO.create(beanMappingService.mapTo(r, Rating.class));
	}

	@Override
	public void remove(@NotNull RatingDTO r) {
		ratingDAO.remove(beanMappingService.mapTo(r, Rating.class));
	}

	@Override
	public List<RatingDTO> findAll() {
		return beanMappingService.mapTo(ratingDAO.findAll(), RatingDTO.class);
	}

	@Override
	public RatingDTO findById(@NotNull Long id) {
		return beanMappingService.mapTo(ratingDAO.findById(id), RatingDTO.class);
	}

	@Override
	public List<RatingDTO> getRatingByUser(@NotNull Long userId) {
		return beanMappingService.mapTo(ratingDAO.getRatingByUser(userId), RatingDTO.class);
	}

	@Override
	public List<RatingDTO> findAllByMovie(@NotNull Long movieId) {
		return beanMappingService.mapTo(ratingDAO.findAllByMovie(movieId), RatingDTO.class);
	}

	@Override
	public void update(RatingDTO r) {
		ratingDAO.update(beanMappingService.mapTo(r, Rating.class));
	}

	@Override
	public void reportRating(RatingDTO r) {
		r.setReportedRating(true);
		update(r);
	}

	@Override
	public void unReportRating(RatingDTO r) {
		r.setReportedRating(false);
		update(r);
	}
}
