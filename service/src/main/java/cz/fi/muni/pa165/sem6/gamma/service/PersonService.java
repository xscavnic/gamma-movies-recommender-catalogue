package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dto.PersonCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonDTO;

import java.util.List;

/**
 * Service interface specific to Person (entity and role) extending the template UserService
 */
public interface PersonService extends UserService<PersonDTO> {
	/**
	 * @param personCreateDTO containing all necessary fields
	 */
	void register(PersonCreateDTO personCreateDTO);

	/**
	 * Ban or unban (change the blacklisted attribute) Person
	 * @param p PersonDTO of Person to ban or unban
	 * @param ban should Person be banned
	 */
	void ban(PersonDTO p, boolean ban);

	/**
	 * Find all People which are either banned or not
	 * @param banned find banned People or not
	 * @return all banned or all unbanned people mapped to PersonDTOs
	 */
	List<PersonDTO> findAll(boolean banned);
}
