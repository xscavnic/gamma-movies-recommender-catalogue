package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.UserDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserChangePasswordDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.User;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public abstract class UserServiceImpl<E extends User, D extends UserDTO> implements UserService<D> {
	final PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
	private final Class<D> dtotype;
	private final Class<E> entitytype;

	protected UserServiceImpl(Class<E> entitytype, Class<D> dtotype) {
		this.dtotype = dtotype;
		this.entitytype = entitytype;
	}

	abstract UserDAO<E> getDAO();

	abstract BeanMappingService getBeanMappingService();

	@Override
	public void update(D d) {
		if (d == null) throw new IllegalArgumentException("Provided user dto is null");
		User user = getDAO().findById(d.getId());
		if (user == null)
			throw new EntityNotFoundException("Provided user is not present in the database (no user was found with matching id)");
		if (!Objects.equals(user.getPassword(), d.getPassword()))
			throw new SecurityException("Password cannot be changed this way");
		getDAO().update(getBeanMappingService().mapTo(d, entitytype));
	}

	@Override
	public void remove(D d) {
		if (d == null) throw new IllegalArgumentException("Provided user is null");
		if (getDAO().findById(d.getId()) == null)
			throw new EntityNotFoundException("Provided user is not present in the database (no user was found with matching id)");
		getDAO().remove(getBeanMappingService().mapTo(d, entitytype));
	}

	@Override
	public D findById(Long id) {
		if (id == null) throw new IllegalArgumentException("The id provided is null");
		User user;
		user = getDAO().findById(id);
		if (user == null) throw new EntityNotFoundException("There is no user with the provided id");
		return getBeanMappingService().mapTo(user, dtotype);
	}

	@Override
	public D findByLogin(String login) {
		if (login == null) throw new IllegalArgumentException("Provided login is null");
		E user;
		user = getDAO().findByLogin(login);
		if (user == null) throw new EntityNotFoundException("No user found with the given login");
		return getBeanMappingService().mapTo(user, dtotype);
	}

	@Override
	public List<D> findAll() {
		return getBeanMappingService().mapTo(getDAO().findAll(), dtotype);
	}

	@Override
	public Optional<D> authenticate(UserAuthenticateDTO userAuthenticateDTO) {
		if (userAuthenticateDTO == null) throw new IllegalArgumentException("UserAuthenticateDTO is null");
		if (userAuthenticateDTO.getLogin() == null) throw new IllegalArgumentException("Login is null");
		if (userAuthenticateDTO.getPassword() == null) throw new IllegalArgumentException("Password is null");
		D user;
		try {
			user = findByLogin(userAuthenticateDTO.getLogin());
		} catch (EntityNotFoundException | DaoException e) {
			return Optional.empty();
		}
		if (user == null || !passwordEncoder.matches(userAuthenticateDTO.getPassword(), user.getPassword()))
			return Optional.empty();
		return Optional.of(user);
	}

	@Override
	public void changePassword(UserChangePasswordDTO userChangePasswordDTO) {
		if (userChangePasswordDTO == null) throw new IllegalArgumentException("UserChangePasswordDTO is null");
		if (userChangePasswordDTO.getLogin() == null) throw new IllegalArgumentException("Login is null");
		if (userChangePasswordDTO.getOldPassword() == null) throw new IllegalArgumentException("Old password is null");
		if (userChangePasswordDTO.getNewPassword() == null) throw new IllegalArgumentException("New Password is null");
		E user = getDAO().findByLogin(userChangePasswordDTO.getLogin());
		if (!passwordEncoder.matches(userChangePasswordDTO.getOldPassword(), user.getPassword()))
			throw new SecurityException("Old password does not match");
		user.setPassword(passwordEncoder.encode(userChangePasswordDTO.getNewPassword()));
		getDAO().update(user);
	}
}
