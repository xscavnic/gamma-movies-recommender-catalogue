package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dto.RatingCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Rating;

import java.util.List;

/**
 * RatingService class
 *
 * @author Šárka Ščavnická
 */
public interface RatingService {
    void create(RatingCreateDTO r);

    void remove(RatingDTO r);

    List<RatingDTO> findAll();

    RatingDTO findById(Long id);

    List<RatingDTO> getRatingByUser(Long userId);

    List<RatingDTO> findAllByMovie(Long movieId);

    void reportRating(RatingDTO r);

    void update(RatingDTO r);

    void unReportRating(RatingDTO r);
}
