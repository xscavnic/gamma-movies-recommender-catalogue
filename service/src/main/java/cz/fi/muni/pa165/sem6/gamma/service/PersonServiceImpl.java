package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.PersonDAO;
import cz.fi.muni.pa165.sem6.gamma.dao.UserDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl extends UserServiceImpl<Person, PersonDTO> implements PersonService {
	@Autowired
	private PersonDAO personDAO;

	@Autowired
	private BeanMappingService beanMappingService;

	public PersonServiceImpl() {
		super(Person.class, PersonDTO.class);
	}

	@Override
	public void register(PersonCreateDTO personCreateDTO) {
		if (personCreateDTO == null) throw new IllegalArgumentException("PersonCreateDTO is null");
		if (personCreateDTO.getLogin() == null) throw new IllegalArgumentException("Login is null");
		if (personCreateDTO.getPassword() == null) throw new IllegalArgumentException("Password is null");
		if (personCreateDTO.getFirstName() == null) throw new IllegalArgumentException("First name is null");
		if (personCreateDTO.getLastName() == null) throw new IllegalArgumentException("Last name is null");
		if (personCreateDTO.getCountry() == null) throw new IllegalArgumentException("Country is null");
		if (personCreateDTO.getBirthDate() == null) throw new IllegalArgumentException("Birth date is null");
		personDAO.create(new Person(personCreateDTO.getLogin(), passwordEncoder.encode(personCreateDTO.getPassword()), personCreateDTO.getFirstName(), personCreateDTO.getLastName(), personCreateDTO.getCountry(), personCreateDTO.getBirthDate()));
	}

	@Override
	UserDAO<Person> getDAO() {
		return personDAO;
	}

	@Override
	BeanMappingService getBeanMappingService() {
		return beanMappingService;
	}

	@Override
	public void ban(PersonDTO p, boolean ban) {
		if (p == null) throw new IllegalArgumentException("Person is null");
		p.setBlacklisted(ban);
		update(p);
	}

	@Override
	public List<PersonDTO> findAll(boolean banned) {
		return findAll().stream().filter(person -> person.getBlacklisted() == banned).collect(Collectors.toList());
	}
}
