package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dto.UserChangePasswordDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.UserAuthenticateDTO;

import java.util.List;
import java.util.Optional;

/**
 * Template service interface for all users
 * @param <U> PersonDTO or AdminDTO
 */
public interface UserService<U> {
	/**
	 * @param u PersonDTO/AdminDTO
	 */
	void update(U u);

	/**
	 * @param u PersonDTO/AdminDTO
	 */
	void remove(U u);

	/**
	 * @param id of User
	 * @return User found with given id mapped to PersonDTO/AdminDTO
	 */
	U findById(Long id);

	/**
	 * @param login of User
	 * @return User found with given login mapped to PersonDTO/AdminDTO
	 */
	U findByLogin(String login);

	/**
	 * @return List of all Users (of type Person or Admin) each mapped to PersonDTO/AdminDTO
	 */
	List<U> findAll();

	/**
	 * @param userAuthenticateDTO containing login and password
	 * @return User (mapped to PersonDTO/AdminDTO) found with given login if hash of given password matches stored hashed password of found User, Optional.empty() otherwise
	 */
	Optional<U> authenticate(UserAuthenticateDTO userAuthenticateDTO);

	/**
	 * Change password of given User if hash of given oldPassword matches stored hashed password of given User to hash of given newPassword
	 * @param userChangePasswordDTO containing the User, old password and new password
	 */
	void changePassword(UserChangePasswordDTO userChangePasswordDTO);
}
