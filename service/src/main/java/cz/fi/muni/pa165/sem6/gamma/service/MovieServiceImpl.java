package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.MovieDAO;
import cz.fi.muni.pa165.sem6.gamma.dao.RatingDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Movie service
 *
 * @author Pavol Bock, 506490
 */
@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieDAO movieDAO;

	@Autowired
	private RatingDAO ratingDAO;

	@Autowired
	private BeanMappingService beanMappingService;

	@Override
	public void create(@NotNull MovieCreateDTO movie) {
		movieDAO.create(beanMappingService.mapTo(movie, Movie.class));
	}

	@Override
	public MovieDTO findById(Long id) {
		return beanMappingService.mapTo(movieDAO.findById(id), MovieDTO.class);
	}

	@Override
	public List<MovieDTO> findAll() {
		return beanMappingService.mapTo(movieDAO.findAll(), MovieDTO.class);
	}

	@Override
	public void remove(MovieDTO movie) {
		movieDAO.remove(beanMappingService.mapTo(movie, Movie.class));
	}

	@Override
	public void update(MovieDTO movie) {
		movieDAO.update(beanMappingService.mapTo(movie, Movie.class));
	}

	@Override
	public Set<MovieDTO> getByGenres(Set<Long> genreIds) {
		return new HashSet<>(beanMappingService.mapTo(movieDAO.getMoviesByGenres(genreIds), MovieDTO.class));
	}

	@Override
	public List<MovieDTO> sortByRating() {
		Map<Long, Double> result = movieDAO.getMoviesRatings();
		List<MovieDTO> movies = new ArrayList<>(beanMappingService.mapTo(movieDAO.findAll(), MovieDTO.class));
		return movies.stream().filter(x -> result.containsKey(x.getId()))
				.sorted((x, y) -> (int) ((result.get(y.getId()) - result.get(x.getId())) * 10))
				.collect(Collectors.toList());
	}

	@Override
	public Double getMovieRatings(MovieDTO movie) {
		return movieDAO.getRating(beanMappingService.mapTo(movie, Movie.class));
	}

	@Override
	public List<MovieDTO> similarByGenre(MovieDTO movie, int topN) {
		List<MovieDTO> result = beanMappingService.mapTo(movieDAO.getSimilarByGenre(beanMappingService.mapTo(movie, Movie.class)), MovieDTO.class);
		return result.stream().limit(topN).collect(Collectors.toList());
	}


	@Override
	public List<MovieDTO> similarByUserRatings(MovieDTO movie, int topN) {
		List<MovieDTO> result = beanMappingService.mapTo(movieDAO.getSimilarByRatings(beanMappingService.mapTo(movie, Movie.class)), MovieDTO.class);
		return result.stream().limit(topN).collect(Collectors.toList());
	}

	@Override
	public List<MovieDTO> topMoviesByGenre(MovieDTO movie, int topN) {
		List<MovieDTO> sortedMovies = sortByRating();
		List<MovieDTO> similarGenres = similarByGenre(movie, topN);
		return sortedMovies.stream().filter(similarGenres::contains).collect(Collectors.toList());
	}

}
