package cz.fi.muni.pa165.sem6.gamma.service;

import org.dozer.Mapper;
import org.springframework.context.annotation.Configuration;


import java.util.Collection;
import java.util.List;

/**
 * Inspired by https://github.com/fi-muni/PA165/blob/master/eshop-service/src/main/java/cz/fi/muni/pa165/service/BeanMappingService.java
 * from the example project.
 * @author Šárka Ščavnická
 */
public interface BeanMappingService {
    public  <T> List<T> mapTo(Collection<?> objects, Class<T> mapToClass);
    public  <T> T mapTo(Object u, Class<T> mapToClass);
    public Mapper getMapper();
}
