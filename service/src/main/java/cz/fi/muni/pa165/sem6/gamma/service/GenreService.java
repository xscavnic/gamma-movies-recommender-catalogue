package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;

import java.util.List;

/**
 * Genre service interface
 * @author Jakub Kuchár
 */
public interface GenreService {
    void create(GenreCreateDTO g);
    GenreDTO findById(Long id);
    GenreDTO findByName(String name);
    List<GenreDTO> findAll();
    void remove(GenreDTO g);
    void update(GenreDTO g);
}
