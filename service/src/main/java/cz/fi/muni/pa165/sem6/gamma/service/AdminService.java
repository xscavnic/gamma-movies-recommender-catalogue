package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dto.AdminCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.AdminDTO;

public interface AdminService extends UserService<AdminDTO> {
	/**
	 * New admin should only be registered by another admin (should be implemented on higher layer)
	 * @param adminCreateDTO containing login and password
	 */
	void register(AdminCreateDTO adminCreateDTO);
}
