package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.GenreDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Implementation of the genre service interface
 *
 * @author Jakub Kuchár, 484954
 */
@Service
public class GenreServiceImpl implements GenreService {

	@Autowired
	private GenreDAO genreDAO;

	@Autowired
	private BeanMappingService beanMappingService;

	@Override
	public void create(@NotNull GenreCreateDTO g) {
		genreDAO.create(beanMappingService.mapTo(g, Genre.class));
	}

	@Override
	public GenreDTO findById(@NotNull Long id) {
		GenreDTO genreDTO = beanMappingService.mapTo(genreDAO.findById(id), GenreDTO.class);
		genreDTO.setId(id);
		return genreDTO;
	}

	@Override
	public GenreDTO findByName(String name) {
		Genre genre = genreDAO.findByName(name);
		GenreDTO genreDTO = beanMappingService.mapTo(genre, GenreDTO.class);
		genreDTO.setId(genre.getID());
		return genreDTO;
	}

	@Override
	public List<GenreDTO> findAll() {
		List<Genre> genres = genreDAO.findAll();
		List<GenreDTO> genreDTOS = beanMappingService.mapTo(genres, GenreDTO.class);
		for (int i = 0; i < genres.size(); i++) {
			genreDTOS.get(i).setId(genres.get(i).getID());
		}
		return genreDTOS;
	}

	@Override
	public void remove(@NotNull GenreDTO g) {
		genreDAO.remove(beanMappingService.mapTo(g, Genre.class));
	}

	@Override
	public void update(@NotNull GenreDTO g) {
		genreDAO.update(beanMappingService.mapTo(g, Genre.class));
	}
}
