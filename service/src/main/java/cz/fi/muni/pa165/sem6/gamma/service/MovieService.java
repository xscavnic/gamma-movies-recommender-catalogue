package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dto.MovieCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;

import java.util.List;
import java.util.Set;

/**
 * Movie service interface
 *
 * @author Pavol Bock, 506490
 */
public interface MovieService {

	/**
	 * Creates new movie.
	 *
	 * @param movieCreateDTO of Movie
	 */
	void create(MovieCreateDTO movieCreateDTO);

	/**
	 * Finds movie by id.
	 *
	 * @param id of Movie
	 * @return MovieDTO of Movie found with given id
	 */
	MovieDTO findById(Long id);

	/**
	 * Finds all movies.
	 *
	 * @return List of all movies as MovieDTOs
	 */
	List<MovieDTO> findAll();

	/**
	 * Remove movie.
	 *
	 * @param movieDTO of Movie
	 */
	void remove(MovieDTO movieDTO);

	/**
	 * Update movie.
	 *
	 * @param movieDTO of Movie
	 */
	void update(MovieDTO movieDTO);

	/**
	 * Return movies which has at least one genre same as was input.
	 *
	 * @param genreIds of Movie
	 * @return set of movies (mapped to MovieDTOs)
	 */
	Set<MovieDTO> getByGenres(Set<Long> genreIds);

	/**
	 * Return movie rating.
	 *
	 * @param movieDTO of Movie
	 * @return rating of movie
	 */
	Double getMovieRatings(MovieDTO movieDTO);

	/**
	 * Return sorted list of movies.
	 *
	 * @return list of movies (mapped to MovieDTOs)
	 */
	List<MovieDTO> sortByRating();

	/**
	 * Return movies which has at least one genre same as was input, maximally topN.
	 *
	 * @param movieDTO of Movie
	 * @param topN     number(int)
	 * @return set of movies (mapped to MovieDTOs)
	 */
	List<MovieDTO> similarByGenre(MovieDTO movieDTO, int topN);

	/**
	 * Return movies which has similar or higher rating, maximally topN.
	 *
	 * @param movieDTO of Movie
	 * @param topN     number(int)
	 * @return set of movies (mapped to MovieDTOs)
	 */
	List<MovieDTO> similarByUserRatings(MovieDTO movieDTO, int topN);

	/**
	 * Return movies which has at least one genre same as was input and biggest rating,maximally topN.
	 *
	 * @param movieDTO of Movie
	 * @param topN     number(int)
	 * @return set of movies (mapped to MovieDTOs)
	 */
	List<MovieDTO> topMoviesByGenre(MovieDTO movieDTO, int topN);

}
