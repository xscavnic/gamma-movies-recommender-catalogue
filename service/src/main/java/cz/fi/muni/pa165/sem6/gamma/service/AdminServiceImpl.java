package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.AdminDAO;
import cz.fi.muni.pa165.sem6.gamma.dao.UserDAO;
import cz.fi.muni.pa165.sem6.gamma.entity.Admin;
import cz.fi.muni.pa165.sem6.gamma.dto.AdminCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.AdminDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends UserServiceImpl<Admin, AdminDTO> implements AdminService {
	@Autowired
	private AdminDAO adminDAO;

	@Autowired
	private BeanMappingService beanMappingService;

	public AdminServiceImpl() {
		super(Admin.class, AdminDTO.class);
	}

	@Override
	public void register(AdminCreateDTO adminCreateDTO) {
		if (adminCreateDTO.getLogin() == null) throw new IllegalArgumentException("Login is null");
		if (adminCreateDTO.getPassword() == null) throw new IllegalArgumentException("Password is null");
		adminDAO.create(new Admin(adminCreateDTO.getLogin(), passwordEncoder.encode(adminCreateDTO.getPassword())));
	}

	@Override
	UserDAO<Admin> getDAO() {
		return adminDAO;
	}

	@Override
	BeanMappingService getBeanMappingService() {
		return beanMappingService;
	}
}
