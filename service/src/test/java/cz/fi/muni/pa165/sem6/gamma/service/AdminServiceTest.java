package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.AdminDAO;
import cz.fi.muni.pa165.sem6.gamma.entity.Admin;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import cz.fi.muni.pa165.sem6.gamma.dto.AdminDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.AdminCreateDTO;
import org.hibernate.service.spi.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Admin service tests
 *
 * @author Pavol Bock, 506490
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class AdminServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private AdminDAO adminDAO;

    @Autowired
    @InjectMocks
    private AdminService adminService;

    @Autowired
    private BeanMappingService beanMappingService;

    private Admin admin;
    private Admin newAdmin;
    private Admin errorAdmin;

    @BeforeClass
    public void setup() throws ServiceException {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void prepareAdmin(){
        admin = new Admin();
        admin.setLogin("admin");
        admin.setPassword("password");
        admin.setId(1L);
        adminDAO.create(admin);

        errorAdmin = new Admin();
        errorAdmin.setPassword("error");
        errorAdmin.setLogin("error");
    }

    @Test
    public void findById(){
        when(adminDAO.findById(admin.getId())).thenReturn(admin);
        assertThat(adminService.findById(admin.getId())).isEqualTo(beanMappingService.mapTo(admin, AdminDTO.class));
    }

    @Test
    public void findByIdNonExisting(){
        Assert.assertThrows(EntityNotFoundException.class,()->adminService.findById(55L));
    }

    @Test
    public void findByIdNullException(){
        Assert.assertThrows(IllegalArgumentException.class,()->adminService.findById(null));
    }

    @Test
    public void register(){
        newAdmin = new Admin();
        newAdmin.setLogin("newAdmin");
        newAdmin.setPassword("newPassword");
        newAdmin.setId(2L);

        doNothing().when(adminDAO).create(newAdmin);
        adminService.register(new AdminCreateDTO("newAdmin","newPassword"));
        verify(adminDAO).create(newAdmin);
    }

    @Test
    public void registerLoginException(){
        Assert.assertThrows(IllegalArgumentException.class,()->adminService.register(new AdminCreateDTO(null,"password")));
    }

    @Test
    public void registerPasswordException(){
        Assert.assertThrows(IllegalArgumentException.class,()->adminService.register(new AdminCreateDTO("admin",null)));
    }

}
