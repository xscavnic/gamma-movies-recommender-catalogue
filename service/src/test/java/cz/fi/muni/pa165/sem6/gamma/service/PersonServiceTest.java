package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.PersonDAO;
import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.PersonCreateDTO;
import org.hibernate.service.spi.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Movie service
 *
 * @author Pavol Bock, 506490
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class PersonServiceTest extends AbstractTestNGSpringContextTests {

    @Mock
    private PersonDAO personDAO;

    @Autowired
    @InjectMocks
    private PersonService personService;

    @Autowired
    private BeanMappingService beanMappingService;

    private Person person;
    private Person person1;
    private Person newPerson;
    private Person errorPerson;
    private LocalDate date;

    @BeforeClass
    public void setup() throws ServiceException {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void preparePerson(){
        person = new Person();
        person.setLogin("JohnyKing");
        person.setPassword("password");
        person.setId(1L);
        person.setFirstName("Janko");
        person.setLastName("Hrasko");
        person.setCountry("Slovakia");

        date = LocalDate.now();
        person.setBirthDate(date);
        personDAO.create(person);

        person1 = new Person();
        person1.setLogin("JohnyKing1");
        person1.setPassword("password1");
        person1.setId(1L);
        person1.setBlacklisted(true);
        person1.setFirstName("Janko1");
        person1.setLastName("Hrasko1");
        person1.setCountry("Slovakia");

        person1.setBirthDate(date);
        personDAO.create(person1);

        errorPerson = new Person();
        errorPerson.setPassword("error");
        errorPerson.setLogin("error");
    }


    @Test
    public void findById(){
        when(personDAO.findById(person.getId())).thenReturn(person);
        assertThat(personService.findById(person.getId())).isEqualTo(beanMappingService.mapTo(person, PersonDTO.class));
    }

    @Test
    public void findByIdNonExisting(){
        Assert.assertThrows(EntityNotFoundException.class,()->personService.findById(55L));
    }

    @Test
    public void findByIdNullException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.findById(null));
    }

    @Test
    public void register(){
        newPerson = new Person();
        newPerson.setLogin("newPerson");
        newPerson.setPassword("newPassword");

        doNothing().when(personDAO).create(newPerson);
        personService.register(new PersonCreateDTO("newPerson","newPassword","new","person","Slovakia",date));
        verify(personDAO).create(newPerson);
    }

    @Test
    public void registerLoginException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.register(new PersonCreateDTO(null,"newPassword","new","person","Slovakia",date)));
    }

    @Test
    public void registerPasswordException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.register(new PersonCreateDTO("newPerson",null,"new","person","Slovakia",date)));
    }

    @Test
    public void registerFirstNameException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.register(new PersonCreateDTO("newPerson","newPassword",null,"person","Slovakia",date)));
    }

    @Test
    public void registerLastNameException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.register(new PersonCreateDTO("newPerson","newPassword","new",null,"Slovakia",date)));
    }

    @Test
    public void registerCountryException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.register(new PersonCreateDTO("newPerson","newPassword","new","person",null,date)));
    }

    @Test
    public void registerDateException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.register(new PersonCreateDTO("newPerson","newPassword","new","person","Slovakia",null)));
    }

    @Test
    public void banTrue(){
        when(personDAO.findById(person1.getId())).thenReturn(person1);
        doNothing().when(personDAO).update(person1);
        personService.ban(beanMappingService.mapTo(person1, PersonDTO.class),true);
        verify(personDAO).update(person1);
        assertThat(person1.isBlacklisted()).isTrue();
    }

    @Test
    public void banFalse(){
        when(personDAO.findById(person.getId())).thenReturn(person);
        doNothing().when(personDAO).update(person);
        personService.ban(beanMappingService.mapTo(person, PersonDTO.class),false);
        verify(personDAO).update(person);
        assertThat(person.isBlacklisted()).isFalse();
    }

    @Test
    public void banException(){
        Assert.assertThrows(IllegalArgumentException.class,()->personService.ban(null,true));
    }

    @Test
    public void findAll(){
        List<Person> listOfPersons = List.of(person);
        when(personDAO.findAll()).thenReturn(listOfPersons);
        List<PersonDTO> result = personService.findAll();

        assertThat(result).hasSize(1);
        assertThat(result.get(0)).isEqualTo(beanMappingService.mapTo(person, PersonDTO.class));
    }
}
