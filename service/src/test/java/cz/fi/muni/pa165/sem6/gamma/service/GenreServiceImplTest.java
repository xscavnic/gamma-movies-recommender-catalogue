package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.GenreDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.GenreDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import org.hibernate.service.spi.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for GenreServiceImpl class
 *
 * @author Šárka Ščavnická
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class GenreServiceImplTest extends AbstractTestNGSpringContextTests {

	@Mock
	private GenreDAO genreDAO;

	@Autowired
	@InjectMocks
	private GenreService genreService;

	@Autowired
	private BeanMappingService beanMappingService;

	private Set<Genre> genres1;
	private Set<Genre> genres2;
	private Genre g1;
	private Genre g2;
	private Genre g3;

	@BeforeMethod
	public void setup() throws ServiceException {
		MockitoAnnotations.openMocks(this);
		prepare();
	}

	@Test
	public void createGenre() {
		doNothing().when(genreDAO).create(g3);
		genreService.create(beanMappingService.mapTo(g3, GenreCreateDTO.class));
		verify(genreDAO).create(g3);
	}


	@Test
	public void findGenreById() {
		when(genreDAO.findById(g1.getID())).thenReturn(g1);
		GenreDTO genreReturned = genreService.findById(g1.getID());
		Assert.assertEquals(genreReturned, beanMappingService.mapTo(g1, GenreDTO.class));
	}

	@Test
	public void findGenreByName() {
		when(genreDAO.findByName(g1.getName())).thenReturn(g1);
		GenreDTO genreReturned = genreService.findByName(g1.getName());
		Assert.assertEquals(genreReturned, beanMappingService.mapTo(g1, GenreDTO.class));
	}

	@Test
	public void findAllGenres() {
		List<Genre> allGenres = List.of(g1, g2);
		when(genreDAO.findAll()).thenReturn(allGenres);
		List<GenreDTO> returnedGenres = genreService.findAll();

		Assert.assertEquals(returnedGenres.size(), 2);
		Assert.assertEquals(allGenres.get(0), beanMappingService.mapTo(returnedGenres.get(0), Genre.class));
		Assert.assertEquals(allGenres.get(1), beanMappingService.mapTo(returnedGenres.get(1), Genre.class));
	}

	@Test
	public void findAllWithNoGenres() {
		List<Genre> noGenres = List.of();
		when(genreDAO.findAll()).thenReturn(noGenres);
		List<GenreDTO> returnedGenres = genreService.findAll();

		Assert.assertEquals(returnedGenres.size(), 0);
	}

	@Test
	public void remove() {
		doNothing().when(genreDAO).remove(g2);
		genreService.remove(beanMappingService.mapTo(g2, GenreDTO.class));
		Assert.assertNull(genreDAO.findById(g2.getID()));
		verify(genreDAO).remove(g2);
	}

	@Test
	public void update() {
		g1.setDescription("New g1 description");
		doNothing().when(genreDAO).update(g1);
		genreService.update(beanMappingService.mapTo(g1, GenreDTO.class));
		verify(genreDAO).update(g1);
	}

	private void prepare() {
		g1 = new Genre();
		g1.setName("Movie genre 1");
		g1.setDescription("Description 1");
		genreDAO.create(g1);

		g2 = new Genre();
		g2.setName("Movie genre 2");
		g2.setDescription("Description 2");
		genreDAO.create(g2);

		g3 = new Genre();
		g3.setName("Movie genre 3");
		g3.setDescription("Description 3");

		genres1 = new HashSet<>();
		genres1.add(g1);
		genres2 = new HashSet<>();
		genres2.add(g1);
		genres2.add(g2);

		Movie movie1 = new Movie();
		movie1.setTitle("Movie 1 Title");
		movie1.setDirector("Director 1");
		movie1.setGenres(genres1);

		Movie movie2 = new Movie();
		movie2.setTitle("Movie 2 Title");
		movie2.setDirector("Director 2");
		movie2.setGenres(genres2);
	}
}
