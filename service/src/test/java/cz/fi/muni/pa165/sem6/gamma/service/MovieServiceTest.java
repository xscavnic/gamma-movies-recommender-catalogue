package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.GenreDAO;
import cz.fi.muni.pa165.sem6.gamma.dao.MovieDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.MovieDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;
import cz.fi.muni.pa165.sem6.gamma.exception.DaoException;
import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import org.hibernate.exception.DataException;
import org.hibernate.service.spi.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Tests for MovieService
 * Tests ending with "WithException" simulate an exception on the Dao layer
 *
 * @author Kristián Malák
 */
@ContextConfiguration(classes = ServiceConfiguration.class)
public class MovieServiceTest extends AbstractTestNGSpringContextTests {
	@Mock
	private MovieDAO movieDAO;

	@Autowired
	@InjectMocks
	private MovieService movieService;

	@Autowired
	private BeanMappingService beanMappingService;

	private Movie movie1;
	private Movie movie2;
	private Genre genre;

	private Movie createMovie(String title, String director, Set<Genre> genres, long id) {
		Movie movie = new Movie();
		movie.setTitle(title);
		movie.setDirector(director);
		movie.setGenres(genres);
		Mockito.doAnswer(invocation -> {
			ReflectionTestUtils.setField((Movie) invocation.getArgument(0), "id", id);
			return null;
		}).when(movieDAO).create(movie);
		movieDAO.create(movie);
		Mockito.doNothing().when(movieDAO).create(movie);
		return movie;
	}

	@BeforeClass
	public void setup() throws ServiceException {
		MockitoAnnotations.openMocks(this);
		GenreDAO genreDAO = Mockito.mock(GenreDAO.class);
		genre = new Genre();
		genre.setName("Action");
		genre.setDescription("");
		Mockito.doAnswer(invocation -> {
			ReflectionTestUtils.setField((Genre) invocation.getArgument(0), "id", 1L);
			return null;
		}).when(genreDAO).create(genre);
		genreDAO.create(genre);
		movie1 = createMovie("awesome movie1", "Steven Spielberg", Set.of(genre), 1L);
		movie2 = createMovie("awesome movie2", "Michael Bay", Set.of(genre), 2L);
		Mockito.clearInvocations(movieDAO);
		Mockito.when(movieDAO.findById(movie1.getId())).thenReturn(movie1);
		Mockito.when(movieDAO.findById(movie2.getId())).thenReturn(movie2);
		Mockito.when(movieDAO.findAll()).thenReturn(List.of(movie1, movie2));
		Mockito.when(movieDAO.getMoviesByGenres(Set.of(genre.getID()))).thenReturn(Set.of(movie1));
		Mockito.when(movieDAO.getRating(movie1)).thenReturn(5.);
		Mockito.when(movieDAO.getSimilarByGenre(movie1)).thenReturn(List.of(movie2));
		Mockito.when(movieDAO.getSimilarByRatings(movie2)).thenReturn(List.of(movie1));
		Mockito.when(movieDAO.getMoviesRatings()).thenReturn(Map.of(movie2.getId(), 2., movie1.getId(), 5.));
	}

	@Test
	public void create() {
		movieService.create(beanMappingService.mapTo(movie1, MovieCreateDTO.class));
		Mockito.verify(movieDAO, Mockito.times(1)).create(movie1);
		movieService.create(beanMappingService.mapTo(movie2, MovieCreateDTO.class));
		Mockito.verify(movieDAO, Mockito.times(1)).create(movie2);
	}

	@Test
	public void findById() {
		assertThat(movieService.findById(movie1.getId())).isEqualTo(beanMappingService.mapTo(movie1, MovieDTO.class));
		assertThat(movieService.findById(movie2.getId())).isEqualTo(beanMappingService.mapTo(movie2, MovieDTO.class));
	}

	@Test
	public void findAll() {
		assertThat(movieService.findAll()).containsExactlyInAnyOrder(beanMappingService.mapTo(movie1, MovieDTO.class), beanMappingService.mapTo(movie2, MovieDTO.class));
	}

	@Test
	public void update() {
		movieService.update(beanMappingService.mapTo(movie1, MovieDTO.class));
		Mockito.verify(movieDAO, Mockito.times(1)).update(movie1);
	}

	@Test
	public void remove() {
		movieService.remove(beanMappingService.mapTo(movie1, MovieDTO.class));
		Mockito.verify(movieDAO, Mockito.times(1)).remove(movie1);
	}

	@Test
	public void getByGenres() {
		assertThat(movieService.getByGenres(Set.of(genre.getID()))).containsExactlyInAnyOrder(beanMappingService.mapTo(movie1, MovieDTO.class));
	}

	@Test
	public void getMovieRatings() {
		assertThat(movieService.getMovieRatings(beanMappingService.mapTo(movie1, MovieDTO.class))).isEqualTo(5.);
	}

	@Test
	public void sortByRating() {
		assertThat(movieService.sortByRating()).containsExactly(beanMappingService.mapTo(movie1, MovieDTO.class), beanMappingService.mapTo(movie2, MovieDTO.class));
	}

	@Test
	public void similarByGenre() {
		assertThat(movieService.similarByGenre(beanMappingService.mapTo(movie1, MovieDTO.class), 1)).containsExactly(beanMappingService.mapTo(movie2, MovieDTO.class));
	}

	@Test
	public void similarByUserRatings() {
		assertThat(movieService.similarByUserRatings(beanMappingService.mapTo(movie2, MovieDTO.class), 1)).containsExactly(beanMappingService.mapTo(movie1, MovieDTO.class));
	}

	@Test
	public void topMoviesByGenre() {
		assertThat(movieService.topMoviesByGenre(beanMappingService.mapTo(movie1, MovieDTO.class), 1)).containsExactly(beanMappingService.mapTo(movie2, MovieDTO.class));
	}
}
