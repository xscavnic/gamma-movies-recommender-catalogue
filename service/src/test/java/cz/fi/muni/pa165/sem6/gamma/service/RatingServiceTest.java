package cz.fi.muni.pa165.sem6.gamma.service;

import cz.fi.muni.pa165.sem6.gamma.dao.RatingDAO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingCreateDTO;
import cz.fi.muni.pa165.sem6.gamma.dto.RatingDTO;
import cz.fi.muni.pa165.sem6.gamma.entity.Genre;
import cz.fi.muni.pa165.sem6.gamma.entity.Movie;
import cz.fi.muni.pa165.sem6.gamma.entity.Person;
import cz.fi.muni.pa165.sem6.gamma.entity.Rating;
import cz.fi.muni.pa165.sem6.gamma.service.config.ServiceConfiguration;
import org.hibernate.service.spi.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ServiceConfiguration.class)
public class RatingServiceTest extends AbstractTestNGSpringContextTests {
	private final LocalDate now = LocalDate.now();
	private final LocalDate fiveYrsAgo = LocalDate.now().minusYears(5);
	private final LocalDate tenYrsAgo = LocalDate.now().minusYears(10);
	private final LocalDate twentyYrsAgo = LocalDate.now().minusYears(20);
	@Mock
	private RatingDAO ratingDAO;
	@Autowired
	@InjectMocks
	private RatingService ratingService;
	@Autowired
	private BeanMappingService beanMappingService;
	private Movie m0ratings;
	private Movie m3ratings;
	private Movie m1rating;
	private Person author1;
	private Person author2;
	private Person author3;
	private Rating r1;
	private Rating r2;
	private Rating r3;
	private Rating r4;

	private Movie createMovie(String title, String director, Set<Genre> genres, Long id) {
		Movie res = new Movie();
		res.setId(id);
		res.setDirector(director);
		res.setTitle(title);
		res.setGenres(genres);
		return res;
	}

	private Rating createRating(String desc, Integer stars, Movie m, Person p, LocalDate timeAdded, Long id) {
		Rating res = new Rating();
		res.setId(id);
		res.setMovie(m);
		res.setPerson(p);
		res.setRatingDescription(desc);
		res.setTotalStars(stars);
		res.setTimeAdded(timeAdded);
		res.setReportedRating(false);
		return res;
	}

	private Genre createGenre(String name, String desc, Long id) {
		Genre res = new Genre();
		res.setId(id);
		res.setName(name);
		res.setDescription(desc);
		return res;
	}


	private void prepare() {
		Genre g1 = createGenre("genre1", "testing genre 1", 1L);
		Genre g2 = createGenre("genre2", "testing genre 2", 2L);
		Set<Genre> genres1 = new HashSet<>();
		genres1.add(g1);
		Set<Genre> genres2 = new HashSet<>();
		genres1.add(g2);

		author1 = new Person("slimshady35", "jahoda", "Jakub", "Kuchar", "Slovakia", twentyYrsAgo);
		author2 = new Person("tymian1", "popol", "Pavol", "Bock", "Czech Republic", tenYrsAgo);
		author3 = new Person("bazalka2", "iglu", "Kristian", "Malak", "Hungary", fiveYrsAgo);
		author1.setId(1L);
		author2.setId(2L);
		author3.setId(3L);

		m0ratings = createMovie("Film1", "Roger Federer", genres1, 1L);
		m1rating = createMovie("Film 2", "Arnold Schwarzeneger", genres2, 2L);
		m3ratings = createMovie("Film 3", "Amanda Strong", genres1, 3L);

		r1 = createRating("Good", 3, m1rating, author1, now, 1L);
		r2 = createRating("Very good", 4, m3ratings, author1, tenYrsAgo, 2L);
		r3 = createRating("Excellent", 5, m3ratings, author3, fiveYrsAgo, 3L);
		r4 = createRating("Poor", 2, m3ratings, author2, twentyYrsAgo, 4L);
	}

	@BeforeClass
	public void setup() throws ServiceException {
		MockitoAnnotations.openMocks(this);
		prepare();
	}

	@Test
	public void create() {
		doNothing().when(ratingDAO).create(r1);
		ratingService.create(beanMappingService.mapTo(r1, RatingCreateDTO.class));
		verify(ratingDAO).create(r1);
	}

	@Test
	public void findAllReturnsAll() {
		List<Rating> expected = new ArrayList<>();
		expected.add(r1);
		expected.add(r2);
		expected.add(r3);
		expected.add(r4);

		when(ratingDAO.findAll()).thenReturn(expected);

		List<RatingDTO> actuallyReturned = ratingService.findAll();

		assertThat(expected).isEqualTo(beanMappingService.mapTo(actuallyReturned, Rating.class));
	}

	@Test
	public void findAllReturnsNone() {
		List<Rating> expected = new ArrayList<>();

		when(ratingDAO.findAll()).thenReturn(new ArrayList<>());

		List<RatingDTO> actuallyReturned = ratingService.findAll();

		assertThat(expected).isEqualTo(beanMappingService.mapTo(actuallyReturned, Rating.class));
	}

	@Test
	public void findById() {
		when(ratingDAO.findById(1L)).thenReturn(r1);
		when(ratingDAO.findById(2L)).thenReturn(r2);
		when(ratingDAO.findById(3L)).thenReturn(r3);
		when(ratingDAO.findById(4L)).thenReturn(r4);

		RatingDTO returned1 = ratingService.findById(1L);
		RatingDTO returned2 = ratingService.findById(2L);
		Rating returned3 = beanMappingService.mapTo(ratingService.findById(3L), Rating.class);
		Rating returned4 = beanMappingService.mapTo(ratingService.findById(4L), Rating.class);

		assertThat(beanMappingService.mapTo(r1, RatingDTO.class)).isEqualTo(returned1);
		assertThat(beanMappingService.mapTo(r2, RatingDTO.class)).isEqualTo(returned2);
		assertThat(r3).isEqualTo(returned3);
		assertThat(r4).isEqualTo(returned4);
	}

	@Test
	public void remove() {
		doNothing().when(ratingDAO).remove(r1);
		ratingService.remove(beanMappingService.mapTo(r1, RatingDTO.class));

		verify(ratingDAO).remove(r1);
	}

	@Test
	public void update() {
		r4.setTotalStars(5);
		doNothing().when(ratingDAO).update(r4);
		ratingService.update(beanMappingService.mapTo(r4, RatingDTO.class));

		verify(ratingDAO).update(r4);
	}

	@Test
	public void findAllByMovie() {
		List<Rating> expected1 = new ArrayList<>();
		expected1.add(r1);
		List<Rating> expected2 = new ArrayList<>();
		expected1.add(r2);
		expected1.add(r3);
		expected1.add(r4);
		List<Rating> expected3 = new ArrayList<>();

		when(ratingDAO.findAllByMovie(m1rating.getId())).thenReturn(expected1);
		when(ratingDAO.findAllByMovie(m3ratings.getId())).thenReturn(expected2);
		when(ratingDAO.findAllByMovie(m0ratings.getId())).thenReturn(new ArrayList<>());

		List<RatingDTO> actual1 = ratingService.findAllByMovie(m1rating.getId());
		List<RatingDTO> actual2 = ratingService.findAllByMovie(m3ratings.getId());
		List<Rating> actual3 = beanMappingService.mapTo(ratingService.findAllByMovie(m0ratings.getId()), Rating.class);

		assertThat(beanMappingService.mapTo(expected1, RatingDTO.class)).containsAll(actual1);
		assertThat(beanMappingService.mapTo(expected2, RatingDTO.class)).containsAll(actual2);
		assertThat(expected3).containsAll(actual3);
	}

	@Test
	public void getRatingByUser() {
		List<Rating> expected1 = new ArrayList<>();
		expected1.add(r1);
		expected1.add(r2);
		List<Rating> expected2 = new ArrayList<>();
		expected1.add(r4);
		List<Rating> expected3 = new ArrayList<>();
		expected1.add(r3);

		when(ratingDAO.getRatingByUser(author1.getId())).thenReturn(expected1);  //r1,r2
		when(ratingDAO.getRatingByUser(author2.getId())).thenReturn(expected2);  //r4
		when(ratingDAO.getRatingByUser(author3.getId())).thenReturn(expected3);  //r3

		List<RatingDTO> actual1 = ratingService.getRatingByUser(author1.getId());
		List<RatingDTO> actual2 = ratingService.getRatingByUser(author2.getId());
		List<Rating> actual3 = beanMappingService.mapTo(ratingService.getRatingByUser(author3.getId()), Rating.class);

		assertThat(beanMappingService.mapTo(expected1, RatingDTO.class)).containsAll(actual1);
		assertThat(beanMappingService.mapTo(expected2, RatingDTO.class)).containsAll(actual2);
		assertThat(expected3).containsAll(actual3);
	}

	@Test
	public void reportRating() {
		doAnswer(invocation -> {
			r3 = invocation.getArgument(0);
			return null;
		}).when(ratingDAO).update(any());
		ratingService.reportRating(beanMappingService.mapTo(r3, RatingDTO.class));
		when(ratingDAO.findById(r3.getId())).thenReturn(r3);
		assertThat(ratingService.findById(r3.getId()).getReportedRating()).isTrue();
	}

	@Test
	public void unreportRating() {
		doAnswer(invocation -> {
			r2 = invocation.getArgument(0);
			return null;
		}).when(ratingDAO).update(any());
		ratingService.reportRating(beanMappingService.mapTo(r2, RatingDTO.class));
		ratingService.unReportRating(beanMappingService.mapTo(r2, RatingDTO.class));
		when(ratingDAO.findById(r2.getId())).thenReturn(r2);
		assertThat(ratingService.findById(r2.getId()).getReportedRating()).isFalse();
	}
}
